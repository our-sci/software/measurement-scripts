/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */

//  [{ 'environmental_array': [['co2']], pulses: [24], 'pulse_distance': [5000000], 'pulsed_lights': [[1]] }];

export default () => {
  const co2_burst_12 = [{
    environmental_array: [
      ['co2']
    ],
    pulses: [12],
    //    pulses: [240], // 800 * 120 = 100,000 seconds -->  
    pulse_distance: [5000000],
    //    pulse_distance: [800000000], // ~ every 800 seconds () (13m)
    pulsed_lights: [
      [1]
    ],
  }, 
];

  return {
    co2_burst_12,
  };
};