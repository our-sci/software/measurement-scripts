export default {
  field_id: {},
  crop: {
    other: {
      other_crop: {
        required: true,
      },
    },
  },

  general_management: {
    required: true,
  },
  irrigation_source: {
    required: true,
  },
  land_prep_method: {
    required: true,
    tillage: {
      'tillage/type_1': {
        required: true,
      },
      'tillage/date_1': {
        required: true,
      },
      'tillage/depth_1': {
        required: true,
      },
      'tillage/tillage_2': {
        required: true,
        Yes: {
          'tillage/type_2': {
            required: true,
          },
          'tillage/date_2': {
            required: true,
          },
          'tillage/depth_2': {
            required: true,
          },
        },
      },
      'tillage/tillage_3': {
        Yes: {
          'tillage/type_3': {
            required: true,
          },
          'tillage/date_3': {
            required: true,
          },
          'tillage/depth_3': {
            required: true,
          },
        },
      },
    },
    notill_chem: {
      herbicide_name: {},
      herbicide_rate: {},
    },
    other: {
      land_prep_other: {
        required: true,
      },
    },
  },

  mulch_question: {
    Yes: {
      'mulch/type': {
        other: {
          'mulch/mulch_other': {
            required: true,
          },
        },
      },
      'mulch/depth': {
        required: true,
      },
      'mulch/date': {
        required: true,
      },
    },
  },
  transplanting_question: {
    Yes: {
      'transplanting/potting_soil_days': {
        required: true,
      },
      'transplanting/potting_soil_brand': {},
      'transplanting/seedling_tray_type': {
        cell: {
          'transplanting/cell_number': {},
        },
      },
      'transplanting/lighting': {},
      'transplanting/climate': {},
      'transplanting/seedling_treatment': {
        '!none': {
          'transplanting/seedling_treatment_date': {},
          'transplanting/seedling_treatment_name': {},
        },
        other: {
          'transplanting/seedling_treatment_other': {
            required: true,
          },
        },
      },
    },
  },

  planting_date: {
    required: true,
  },

  lime_question: {
    Yes: {
      'lime/rate': {},
      'lime/lime_date': {},
    },
  },

  amendments_question: {
    Yes: {
      'amendments/amendment_type_1': {},
      'amendments/name_1': {},
      'amendments/amendment_date_1': {},
      'amendments/method_1': {},
      'amendments/nutrients_1': {
        N: { 'amendments/n_1': {} },
        P: { 'amendments/p_1': {} },
        K: { 'amendments/k_1': {} },
        other: {
          'amendments/nutrients_trace_1': {},
        },
      },
      'amendments/amendment_2': {
        Yes: {
          'amendments/amendment_type_2': {},
          'amendments/name_2': {},
          'amendments/amendment_date_2': {},
          'amendments/method_2': {},
          'amendments/nutrients_2': {
            N: { 'amendments/n_2': {} },
            P: { 'amendments/p_2': {} },
            K: { 'amendments/k_2': {} },
            other: {
              'amendments/nutrients_trace_2': {},
            },
          },
        },
      },
    },
  },
};
