/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

import app from './lib/app';
import serial from './lib/serial';
import sendDevice from './lib/sendDevice';

export default serial; // expose this to android for calling onDataAvailable

(async () => {
  await serial.init('/dev/ttyUSB0', {
    //  await serial.init('/dev/rfcomm0', {
    baudRate: 9600,
    dataBits: 8,
    stopBits: 1,
    parity: 'none',
  });

  // Then send the protocol to the deviceB
  //  serial.write("p");
  //  console.log(JSON.stringify(protocol));

  // Mark the progress of pulling in the data
  const count = 0;
  try {
    // try just makes sure that if it doesn't work, you gracefully handle the error
    const result = await serial.readLine(); // go get one line from the serial port
    app.result(result); // save the result in result.json (I know, it's not a json...)
  } catch (err) {
    console.error(`error reading: ${err}`);
  }
})();
