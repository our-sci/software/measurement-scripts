module.exports = {
  extends: 'airbnb-base',
  rules: {
    'global-require': 0,
    'no-console': 0,
    'no-mixed-operators': 0,
    'no-await-in-loop': 0,
    'no-plusplus': 0,
  },
};
