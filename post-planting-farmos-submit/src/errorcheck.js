import { err, survey, regex } from './sensor';

function seqCheck(seq) {
  seq.forEach((s) => {
    if (survey[s] === null || survey[s] === undefined || survey[s] === '') {
      err(`Missing answer for: ${s}`);
    }
  });
}

function sel(name, eq = 'yes') {
  if (survey[name] && survey[name].toLowerCase() === eq.toLowerCase()) {
    return true;
  }
  return false;
}

function yes(input) {
  if (input && (input === 'yes' || input === 'Yes')) {
    return true;
  }
  return false;
}

function checkTillage() {
  seqCheck(['tillage/type_1', 'tillage/date_1', 'tillage/depth_1']);

  if (yes(survey['tillage/tillage_2'])) {
    seqCheck(['tillage/type_2', 'tillage/date_2', 'tillage/depth_2']);
  }

  if (yes(survey['tillage/tillage_3'])) {
    seqCheck(['tillage/type_3', 'tillage/date_3', 'tillage/depth_3']);
  }
}

function checkMulch() {
  seqCheck(['mulch/mulch_type_1', 'mulch/mulch_date_1']);
  if (survey['mulch/mulch_type_1'] === 'other') {
    seqCheck(['mulch/mulch_other_1']);
  }

  if (yes(survey['mulch/mulch_2'])) {
    seqCheck(['mulch/mulch_type_2', 'mulch/mulch_date_2']);
    if (survey['mulch/mulch_type_2'] === 'other') {
      seqCheck(['mulch/mulch_other_2']);
    }
  }
}

function checkTransplanting() {
  seqCheck([
    'transplanting/potting_soil_days',
    'transplanting/potting_soil_brand',
    'transplanting/seedling_tray_type',
    'transplanting/lighting',
    'transplanting/climate',
    'transplanting/seedling_treatment',
  ]);

  if (sel('transplanting/seedling_tray_type', 'cell')) {
    seqCheck(['transplanting/cell_number']);
  }

  if (survey['transplanting/seedling_treatment'].split(' ').includes('other')) {
    seqCheck(['transplanting/seedling_treatment_other']);
  }

  if (!survey['transplanting/seedling_treatment'].split(' ').includes('none')) {
    seqCheck(['transplanting/seedling_treatment_date', 'transplanting/seedling_treatment_name']);
  }
}

function checkLime() {
  seqCheck(['lime/rate', 'lime/lime_date']);
}

function checkAmendments() {
  for (let i = 1; i < 10; i++) {
    if (i !== 1) {
      if (!sel(`amendments/amendment_${i}`)) {
        break;
      }
    }

    seqCheck([
      `amendments/amendment_type_${i}`,
      `amendments/name_${i}`,
      `amendments/amendment_date_${i}`,
      `amendments/method_${i}`,
      `amendments/nutrients_${i}`,
    ]);

    if (survey[`amendments/nutrients_${i}`]) {
      const selected = survey[`amendments/nutrients_${i}`].split(' ');
      /*
    ['N', 'P', 'K'].forEach((n) => {
      if (selected.includes(n)) {
        seqCheck([`amendments/${n.toLowerCase()}_${i}`]);
      }
    });
    */

      if (selected.includes('other')) {
        seqCheck([`amendments/nutrients_trace_${i}`]);
      }
    }
  }
}

export default function errorCheck() {
  if (!survey.field_id) {
    err('Field ID missing');
  }

  if (!survey.field_id.match(regex.field_id)) {
    err(`Unable to parse Field ID<br>must match ${regex.field_id}`);
  }

  if (!survey.crop) {
    err('Crop not set');
  }

  if (survey.crop === 'other') {
    if (!survey.other_crop) {
      err('crop other selected but not supplied');
    }
  }

  if (survey.crop === 'grape' && !survey.planting_date_grape) {
    err('selected grape but no date for grape planting supplied');
  }

  if (survey.crop !== 'grape') {
    if (!survey.planting_date) {
      err('no planting / transplanting date selected');
    }
  }

  if (!survey.submit_rfc) {
    err('Will you submit samples from this planting to the RFC lab as part of the Farm Partners or NOFA programs?');
  }

  if (!survey.general_management) {
    err('general management not answered');
  }

  if (!survey.irrigation_source) {
    err('irrigation source not answered');
  }

  if (!survey.land_prep_method) {
    err('land prep method not answered');
  }

  if (survey.land_prep_method === 'notill_chem') {
    if (!survey.herbicide_name) {
      err('herbicide name not supplied');
    }

    if (!survey.herbicide_rate) {
      err('herbicide rate not supplied');
    }
  }

  if (survey.land_prep_method === 'other') {
    if (!survey.land_prep_other) {
      err('selected land prep other, but no name for land prep method supplied');
    }
  }

  if (survey.land_prep_method === 'tillage') {
    checkTillage();
  }

  if (sel('mulch_question')) {
    checkMulch();
  }

  if (sel('transplanting_question')) {
    checkTransplanting();
  }

  if (sel('lime_question')) {
    checkLime();
  }

  if (sel('amendments_question')) {
    checkAmendments();
  }
}
