const regex = {
  anyOptional: /^.*$/,
  any: /^.+$/,
  number: /^\d+$/,
  numberOptional: /^\d*$/,
  date: /^\d{4}-\d{2}-\d{2}$/,
  optionalDate: /^(\d{4}-\d{2}-\d{2})|()$/,
  month_year: /^\d{4}-\d{2}$/,
  field_id: /^.*\(server: (\d+), tid: (\d+)\)$/,
};

export { regex };
