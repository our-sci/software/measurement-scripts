/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/* global processor */

import {
  sprintf
} from 'sprintf-js';
import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';

const Chartist = require('chartist');

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();
  if (result.error) {
    ui.error('Error', result.error);
    app.save();
    return;
  }

  let standardSolutions = [];

  // NOTE for standard solutions, location 0 on the array is intentionally left undefined so that i=1 i<standard.length works for everything in the loop.
  if (result.test === 'Antioxidants') {
    standardSolutions = ['undefined', 25, 50, 100, 200, 300]; // uM iron equivalents
  } else if (result.test === 'Protein') {
    standardSolutions = ['undefined', 50, 100, 150, 200, 250]; // uL protein per ml
  } else if (result.test === 'Polyphenols') {
    standardSolutions = ['undefined', 12.5, 25, 50, 75, 125, 150, 200]; // ug gallic acid per ml
  } else {
    standardSolutions = [];
    ui.warning(
      'Standard not identified',
      'The model was unable to identify measurements to use within this survey.  Make sure you have run all measurements in the survey.  If you are not using a standard survey, you need to include some measurements to run the model on!',
    );
  }

  ui.warning(result.test + ' standard', '');
  //  ui.info('standardSolutions', standardSolutions);
  const numberStandards = standardSolutions.length;

  // STANDARDS CALIBRATION
  // ///////////////////////////////////////////////////////
  // Using a linear model
  // create fit data for the temperature and median response, display the results, save them to CSV, and prepare them to be saved back to the device

  //  const regData = [];
  const regFit = [];
  const xData = [];
  const yData = [];
  const wavelengths = [370, 395, 420, 530, 605, 650, 730, 850, 880, 940];
  let toDevice_yint = 'set_user_defined+';
  let toDevice_slope = 'set_user_defined+';
  const missingFlag = [];
  // loop through all of the wavelengths to run regressions, display, and save to csv
  for (let z = 0; z < wavelengths.length; z++) {
    //    regData.push([]);
    xData.push([]);
    yData.push([]);
    // loop through the number of measurements collected to put into the heat correction model
    for (let i = 1; i < standardSolutions.length; i++) {
      // if that data point is not there, remove it from the model and display... allows users to remove bad data points.  Notify them that this happened, however.
      if (typeof result['standard' + i].data === 'undefined') {
        // mark bad points with -999999, remove later after loop
        //        regData[z].push(-999999, -999999);
        xData[z].push(-999999);
        yData[z].push(-999999);
        // notify just once (when first identified).
        if (missingFlag.includes(i) !== true) {
          ui.warning(
            'Standard ' + standardSolutions[i] + ' not found',
            'This data point in the standard set was not collected or removed.  If this was done in error, re-measure that standard.  If intentionally removed, you may continue.',
          );
        }
        // mark flag to keep track of missing points
        missingFlag.push(i);
      } else {
        const meas = result['standard' + i].data;
        const median = meas['median_' + wavelengths[z]];
        //        regData[z].push([standardSolutions[i], Number(median)]);
        xData[z].push(Number(median));
        yData[z].push(standardSolutions[i]);
      }
    }
    // remove missing points from all arrays
    //    regData[z] = regData[z].filter(val => val !== -999999);
    xData[z] = xData[z].filter(val => val !== -999999);
    yData[z] = yData[z].filter(val => val !== -999999);
    // run the model (linear regression)
    ui.info('Data', 'standard: ' + yData[z] + '<br>reflectance: ' + xData[z]);
    const regResults = MathMore.MathLINREG(xData[z], yData[z]);
    const slopesNormalized = [];
    slopesNormalized[0] = regResults.b; // / average_value;
    slopesNormalized[1] = regResults.m; // / average_value;
    // save results to csv and display
    regFit.push(regResults.r2);
    app.csvExport('raw_slope_' + wavelengths[z], regResults.m);
    app.csvExport('raw_yint_' + wavelengths[z], regResults.b);
    app.csvExport('norm_slope_' + wavelengths[z], slopesNormalized[1]);
    app.csvExport('norm_yint_' + wavelengths[z], slopesNormalized[0]);
    app.csvExport('r2_' + wavelengths[z], regFit[z]);
    // Error check if r2 is too low
    let poorFit = 0;
    if (regFit[z] < 0.9) {
      ui.warning(
        'Poor fit! r2 = ' + MathMore.MathROUND(regFit[z], 3) + ' at ' + wavelengths[z] + 'nm',
        'Evaluate data and consider repeating the calibration',
      );
      poorFit += 1;
    }
    ui.info(
      'Calibration at ' + wavelengths[z],
      'r2: ' +
      regFit[z] +
      '<br>' +
      'y intercept: ' +
      slopesNormalized[0] +
      '<br>' +
      'slope: ' +
      slopesNormalized[1] +
      '<br>' +
      'y intercept raw: ' +
      regResults.b +
      '<br>' +
      'slope raw: ' +
      regResults.m,
      //    '<br>' +
      //    slopesNormalized[2]
      //      '<br>' +
      //      slopesNormalized[3] +
      //      '<br>' +
      //      slopesNormalized[4],
    );
    // Note the number of poor fits.
    app.csvExport('poorFit', poorFit);
    // prepare results to save to device, to be assembled and pushed in another measurement script.
    toDevice_yint += z + 1 + '+' + MathMore.MathROUND(slopesNormalized[0], 10) + '+';
    toDevice_slope += z + 11 + '+' + MathMore.MathROUND(slopesNormalized[1], 10) + '+';
    //  toDevice += z + 21 + '+' + slopesNormalized[2] * 1000 + '+';
    //    toDevice += z + 31 + '+' + slopesNormalized[3] * 1000000 + '+';
    //    toDevice += z + 41 + '+' + slopesNormalized[4] * 1000000000 + '+';
    // output individual plots for each wavelength
    ui.plotxy(xData[z], yData[z], 'Fit for ' + wavelengths[z]);
  }

  // assemble the plots so they show all in one graph
  ui.multixy(xData, yData, 'Fits for Wavelengths', wavelengths.map(v => 'Fit for ' + v));
  // close out the text to be sent to device, export to CSV (important, also gets picked up by next measurement script), and display for santify check
  toDevice_yint += '-1+';
  toDevice_slope += '-1+';
  app.csvExport('toDevice', toDevice_yint + toDevice_slope); // toDevice saved to csv to check if needed
  app.setEnv('toDevice', toDevice_yint + toDevice_slope, 'data to send to device'); // toDevice saved to environment variable, which is then saved to device by 'toDevice' measurement
  //  toDevice += '-1+';
  //  toDevice += '-1+';
  //  toDevice += '-1+';
  ui.warning(
    'Notes on saving to device',
    "Y Intercept values are saved in userdef[1] - userdef[10], and slope values are saved to userdef[11] - userdef[20].  They can be accessed by using the 'recall' function.",
  );
  ui.info('Save yint to Device', toDevice_yint);
  ui.info('Save slopes to Device', toDevice_slope);
  //  ui.info('Save to Device coefficient 3', toDevice);
  //  ui.info('Save to Device coefficient 4', toDevice);
  //  ui.info('Save to Device coefficient 5', toDevice);
})();
// make sure this is at the very end!
app.save();