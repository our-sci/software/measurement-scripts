/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

import app from './lib/app';
import serial from './lib/serial';
import wavelengthProcessor from './lib/process-wavelengths';
import sendDevice from './lib/sendDevice';
import * as MathMore from './lib/math';
import sleep from './lib/utils'; // use: await sleep(1000);
/*
import {
  xhr
} from 'winjs';
*/

export default serial; // expose this to android for calling onDataAvailable

(async () => {
  await serial.init('/dev/ttyACM0', {
    //  await serial.init('/dev/rfcomm0', {
    baudRate: 115200,
    dataBits: 8,
    stopBits: 1,
    parity: 'none',
  });


  // ///////////////////////////////////////////////////////
  // NAMING CONVENTIONS FOR Reflectometer VERSIONING
  // We are using the x1.x2.x3 versioning system, where:
  // x3 = immediate patch (data from device will be backwards compatible)
  // x2 = minor version (data from device will be backwards compatible, but there are feature improvements, UI changes, new outputs etc.)
  // x1 = major version (data from device will not be backwards compatible... LED intensity change, detector response change, changed calculation of important output, built for new device, etc.)
  // when collecting data or running experiments, it's important to not switch major versions if you want to ensure comparable data.

  // ///////////////////////////////////////////////////////
  // OPTIONAL instead of reading from the serial port mock some data
  // serial.feed('./mock/mock_sensor_output.json');

  // ///////////////////////////////////////////////////////
  // Multiple measurements are saved from this single measurement script
  // Select a serial.write command below based on what you are updating to the Our Sci database.
  // Then copy paste the associated manifest (listed at the very bottom) and save it to the manifest.json file in src folder
  // Finally hit ctrl-b "compile and upload" and check the website app.our-sci.net to confirm.

  /*
  // Calibration measurements (device calibration to card, blank to EEPROM, blank to local environment variable)
  // the lights are in numerical order, rather than wavelength order...
  calibrateAllWhite, // .....
  calibrateAllBlack, // .....
  calibrateAllBlank, // .....
  calibrateBlankLocal, // .......
  // NOT USED calibrateAllWhiteMaster,
  // NOT USED calibrateAllBlackMaster,

  // standard measurement for user device on sample showing relative values (0 - 100)
  standard, // .......
  standardBlank, // .......
  standardRaw, // ......
  standardRawNoHeat, // ......
  standardRawHeatCal, // ......
  standardHeatTest, // ......
  standardApplyCalibration,
  environmental_only
  testIntensity // .....
  testIntensityIr // ....

  // protocols used during manufacturing to reset device memory and set bluetooth
  resetAll,
  configure_bluetooth,
  configure_bluetooth_115200
  set_device_info

  //  other legacy protocols
  oldReflectometer2_flat,
  */

  let protocol = sendDevice().standardBlank; // <-- copy paste the protocol you want to run in here
  //  console.log(sendDevice().testIntensity);

  // If it's a protocol to set device ID, ten send the protocol to the device.
  if (app.isAnswered('device_id') && protocol === sendDevice().set_device_info) {
    console.log(`${app.getAnswer('device_id')}+`);
    protocol += `${app.getAnswer('device_id')}+`;
  }
  // If setting bluetooth, then add the device ID as hex to the bluetooth name
  else if (app.isAnswered('device_id') && (protocol === sendDevice().configure_bluetooth || protocol === sendDevice().configure_bluetooth_115200)) {
    const parsed = parseInt(app.getAnswer('device_id'), 10);
    const hex = parsed.toString(16);
    if (protocol === sendDevice().configure_bluetooth) {
      protocol += `${hex}+9600+`;
    } else if (protocol === sendDevice().configure_bluetooth_115200) {
      protocol += `+${hex}+115200+`;
    }
    console.log(protocol);
  }
  if (typeof protocol === 'object') {
    // if you want to adjust on the fly based on local environmental variables, add if statements here!
    if (
      app.getEnv('rfclab') === '1' &&
      JSON.stringify(protocol) === JSON.stringify(sendDevice().standard)
    ) {
      protocol = sendDevice().standardApplyCalibration;
    }
    // ... more if statements as needed

    console.log(JSON.stringify(protocol));
    serial.write(JSON.stringify(protocol));
  } else {
    console.log(protocol);
    serial.write(protocol);
  }

  // set of scripts to produce (send info to serial, manifest, a few flags that I can use to adjust visualization...) -->
  // see and get environment vars and questions from survey (using VS intellisense, typescript)
  // mock for environment vars.
  // eliminate mock for surveys (not having to \" all the time) access surveys via phone or access the web.

  // Mark the progress of pulling in the data
  let count = 0;
  try {
    app.progress(0.1);
    const result = await serial.readStreamingJson('data_raw[*]', () => {
      count += 1;
      app.progress(0.1 + (count / 617.0) * 100.0 * 0.9);
    });
    app.progress(100);

    if (result.error) { // if there is an error (like battery too low) tell the user
      app.result(result);
      return;
    }
    // if this is setting the device ID, then also put in result the non-hex version of the ID
    if (app.isAnswered('device_id')) {
      result.device_id_number = `${app.getAnswer('device_id')}`;
    }
    if (typeof protocol === 'object') {
      // ///////////////////////////////////////////////////////
      // if this is just a single measurement, then define meas as the measurement.  If there are multiple, note that they also need to be pulled out here.
      for (let j = 0; j < result.sample.length; j++) {
        const meas = result.sample[j];
        meas.passed = {};
        // default values to zero, save over if they exist (otherwise we're dealing with typeof 'undefined' all the time which is annoying)
        meas.passed.is_chlorophyll = '';
        meas.passed.test = '';
        meas.passed.cardQr = '';
        meas.passed.blank = '';
        // Go get any information from previous questions and pass it along for the processor script to use.
        // May also save things using environment variable (env.get, env.set)
        if (typeof meas.calibration === 'undefined') {
          // default calibration to zero.
          meas.calibration = 0;
        } // if you can't find it, set it to zero
        if (app.isAnswered('is_chlorophyll')) {
          meas.passed.is_chlorophyll = app.getAnswer('is_chlorophyll');
          console.log('has chlorophyll' + meas.passed.is_chlorophyll);
        }
        if (app.isAnswered('sample_type')) {
          meas.passed.sample_type = app.getAnswer('sample_type');
          console.log('this is lab test ' + meas.passed.sample_type);
        }
        // if (app.isAnswered('dilution_super_anti_poly')) {
        //   meas.passed.dilution_super_anti_poly = app.getAnswer('dilution_super_anti_poly');
        //   console.log('dilution level is ' + meas.passed.dilution_super_anti_poly);
        // }
        if (app.isAnswered('cardQr')) {
          meas.passed.cardQr = app.getAnswer('cardQr');
          console.log('this is a calibration, passed QR code to processor... it is ... ');
          console.log(meas.passed.cardQr);
        }
        // if 'blank' environmental variable exists and is not empty
        if (
          typeof app.getEnv('blank') !== 'undefined' &&
          app.getEnv('blank') !== '' &&
          app.getEnv('blank') !== null
        ) {
          // and cal = 0
          if (meas.calibration === 0) {
            meas.passed.blank = JSON.parse(app.getEnv('blank'));
            console.log(
              'this measurement will use a blank saved in the local environment variables.  The saved blank is... ',
            );
            console.log(JSON.stringify(meas.passed.blank));
            console.log(
              'saved blank has been passed to processor.js and local environment blank has been reset {}.  Confirm reset: ',
            );
            app.setEnv(
              'blank',
              '',
              "Blank, and is applied to the next 'standard' measurement.  Reset after each measurement when it is applied.",
            );
            console.log(app.getEnv('blank'));
          } else {
            console.log(
              'A blank was found, but is not supposed to be applied in this measurement.  It was not used, and was removed as a local variable.  Confirm reset: ',
            );
            app.setEnv(
              'blank',
              '',
              "Blank, and is applied to the next 'standard' measurement.  Reset after each measurement when it is applied.",
            );
            console.log(app.getEnv('blank'));
          }
        }
      }
      /* for testing above
      "toDevice": "chickenRunIs+A+Great-movie+",
      "is_chlorophyll": "1",
      "test": "1",
      */
    } else { // create a flag so we know it's just a text input and we just need to print whatever comes out.
      result.isTextInput = 1;
    }

    app.result(result);
  } catch (err) {
    console.error(`error reading json: ${err}`);
  }
})();


// {
//   "sample_type": "polyphenols",
//   "cardQr": "12051",
//   "is_chlorophyll": "1"
// }

// list of all scripts saved to app.our-sci.net based on this script (only changing the protocol sent to device in sensor.js)

/*
"id": "calibrateAllWhite-5",
"name": "Reflectometer calibration teflon standard 5",
"description": "Calibrate a device to the device's teflon standard.",
"version": "5.2.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllBlack-5",
"name": "Reflectometer calibration open blank 5",
"description": "Calibrate a device to an open blank by point open window to empty space.",
"version": "5.2.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllBlank-5",
"name": "Reflectometer calibration blank 5",
"description": "Create and save a blank value, to be applied as an offset when using the standard_blank measurement on samples.",
"version": "5.1.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateBlankLocal-5",
"name": "Reflectometer blank saved as local variable",
"description": "Standard reflectomter blank offset which saves result to the local Environment variable. standard measurement uses if found, then resets.",
"version": "5.3.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "standard-5",
"name": "Reflectometer standard 5",
"description": "Standard reflectometer measurement for field and lab use",
"version": "5.7.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "standardBlank-5",
"name": "Reflectometer blank 5",
"description": "Standard reflectomter measurement with blank offset, used on samples in cuvettes or similar situations in which blanks are useful.  Use the calibrateAllBlank measurement on the blank itself to save the blank values to the device",
"version": "5.3.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "standardRaw-5",
"name": "Reflectometer raw 5",
"description": "outputs raw data only, use standard reflectometer measurement for normal use",
"version": "5.3.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "standardRawNoHeat-5",
"name": "Reflectometer raw no heat calibration 5",
"description": "outputs raw data only, use standard reflectometer measurement for normal use.  Heat calibration not applied.",
"version": "5.2.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "standardRawHeatCal-5",
"name": "Reflectometer raw used for heat calibration 5",
"description": "outputs raw data only without heat (voltage) adjustement, repeats 12 times over 24 minutes.  Use standard reflectometer measurement for normal use.  Heat calibration not applied.",
"version": "5.2.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "standardHeatTest-5",
"name": "Reflectometer test of 12 measurements over a range of measurements",
"description": "Used to test the heat calibration StandardRawHeatCal.",
"version": "5.3.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "testIntensity-5",
"name": "Test a set of light intensities",
"description": "Test a set of intensities near the normal standard-5 intensity set, for device manufacture only",
"version": "5.2.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "testIntensityIr-5",
"name": "Test a set of light intensities - IR lights only",
"description": "Test a set of intensities near the normal standard-5 intensity set for IR lights only, for device manufacture only",
"version": "5.2.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "environmental_only",
"name": "Environmental measurements only",
"description": "Measure temperature, pressure, humidity, and VOCs only",
"version": "3.0.2",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "resetAll",
"name": "Reset all memory from print_memory",
"description": "Sets all values back to pre-calibration factory settings (0 or 100)",
"version": "3.0.2",
"action": "Begin reset - are you sure?",
"requireDevice": "true"
/*

/*
"id": "configure_bluetooth",
"name": "Assign correct bluetooth settings",
"description": "Sets to 115200 baud (originally 9600), device name to device ID as hex",
"version": "3.2.4",
"action": "Configure Bluetooth",
"requireDevice": "true"
/*

/*
"id": "configure_bluetooth_115200",
"name": "Assign correct bluetooth settings (115200)",
"description": "Sets to 115200 baud (originally 115200), device name to device ID as hex.  Used only on devices which have already been set correctly once.",
"version": "3.2.4",
"action": "Configure Bluetooth",
"requireDevice": "true"
/*

/*
"id": "set_device_info",
"name": "Assign device ID",
"description": "Sets device ID",
"version": "3.0.2",
"action": "Are you sure, set device ID ?!",
"requireDevice": "true"
/*



// old stuff


/* // DEPRECIATED
"id": "calibrateAllWhiteMaster-4",
"name": "Reflectometer calibration white master 4",
"description": "Calibrate the master device to master calibration card, using the white square.",
"version": "4.0.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/* // DEPRECIATED
"id": "calibrateAllBlackMaster-4",
"name": "Reflectometer calibration black master 4",
"description": "Calibrate the master device to master calibration card, using the black square.",
"version": "4.0.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllBlackMasterMaster-3",
"name": "Reflectometer Master device - Master card calibration 3",
"description": "ONLY used to calibrate the master card.  Force saves 0 / 100 values to the device.",
"version": "3.0.2",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "standard-firmware-1-18",
"name": "Reflectometer measurement firmware 1.18",
"description": "only for beta devices on old firmware 1.18",
"version": "2.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "heat-cal",
"name": "Heat Calibration",
"description": "Calibrate devices for different temperatures (3 hour test)",
"version": "1.0",
"action": "Run Calibration",
"requireDevice": "true"
*/

/*
"id": "standard-chlorophyll-3",
"name": "Chlorophyll standard 3",
"description": "Chlorophyll content using reflectometer for field and lab use",
"version": "3.0.2",
"action": "Run Measurement",
"requireDevice": "true"
  */