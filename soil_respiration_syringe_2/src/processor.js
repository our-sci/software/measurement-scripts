/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

/* global processor */

import { sprintf } from 'sprintf-js';
import _ from 'lodash';

import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  if (result.error) {
    ui.error('Something was not right', result.error);
    return;
  }
  // add 5g soil to 0.7 ml water

  //  1000ml Syringe setup 5 g only
  // ///////////////////////////////////////////////////////
  // Define the experimental parameters for the CO2 method
  const sample_time = 5; // time between samples in seconds
  const Headspace = 85;
  const Temp = 298;
  const Rconstant = 82.05;
  const conversionfactor = 5000; // conversion factor is assuming 5 g soil (soil g x 1000)
  const conversionfactor2 = 12000; //  converts to weight of C only and not of CO2
  // ///////////////////////////////////////////////////////
  //  */
  const hoursdiff = MathMore.MathROUND((result.t2 - result.t1) / 1000 / 60 / 60);
  const co2final = result.respiration_24.data.max_co2;
  const co2baseline = result.respiration_baseline.data.max_co2;
  const co2increase = MathMore.MathROUND((co2final - co2baseline) * (24 / hoursdiff));
  const ugCgsoil = MathMore.MathROUND(
    ((co2increase * Headspace) / conversionfactor / (Temp * Rconstant)) * conversionfactor2,
  );

  // ui.info('Hours difference', (result.t2 - result.t1) / 1000 / 60 / 60);
  /*
  if (co2max === 10000) {
    // if the device is working correctly, it'll max out at exactly 10,000.  If there is a noisy spike that needs to be removed, it's often > 10,000
    const warningText = 'CO2 too high';
    ui.warning(
      warningText,
      'CO2 reading on the device maxed sensors range at 10,000 ppm.  Check the experimental setup for other sources of CO2, or if the soil is extremely active, consider changing the protocol to account for high CO2 activity.',
    );

    warnings.push(warningText);
    ui.info('CO2 increase', null);
    ui.info('ugC per g soil', null);
    app.csvExport('co2_increase', null);
    ui.csvExport('ugc_gsoil', null);
*/
  // } else {
  ui.info('ugC per g soil', ugCgsoil);
  ui.info('CO2 increase', co2increase);
  ui.info('Max CO2', co2final);
  ui.info('Min CO2', co2baseline);
  ui.info('Hours difference', hoursdiff);
  app.csvExport('ugc_gsoil', ugCgsoil);
  app.csvExport('co2_increase', co2increase);
  app.csvExport('Max_co2', co2final);
  app.csvExport('Min_co2', co2baseline);
  app.csvExport('Hours difference', hoursdiff);
  //  }
  /*
  // ///////////////////////////////////////////////////////
  // Print all errors and info statements's up to this point and save them to the database
  if (typeof warnings[0] !== 'undefined') {
    for (let i = 0; i < warnings.length; i++) {
      app.csvExport('warnings', warnings.toString());
    }
  }
  if (typeof info[0] !== 'undefined') {
    for (let i = 0; i < info.length; i++) {
      app.csvExport('info', info.toString());
    }
  }
  */
  // ///////////////////////////////////////////////////////
  /*
  ui.plot(
    {
      series: [co2],
    },
    'CO2 over time',
    {
      min: co2min,
      max: co2max,
    },
  );
*/

  app.save();
})();
