/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-unreachable */

/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

/** Greg's notes
 * added //DEBUG to quiet console log statements here and in libraries app, process-wavelengths, and serial.
 */

import moment from 'moment';
import _ from 'lodash';
import mathjs from 'mathjs';

import serial from './lib/serial';
import app from './lib/app';
import sendDevice from './lib/sendDevice';
import * as MathMore from './lib/math';
import * as utils from './lib/utils'; // use: await sleep(1000);
import sheets from './lib/sheets';

export default serial; // expose this to android for calling onDataAvailable

const result = {};

function errorExit(error) {
  app.result({
    error,
  });
}

(async () => {
  try {
    /*
    if (!app.hasQuestion('respiration_baseline') || !app.hasQuestion('respiration_24')) {
      throw new Error('respiration_baseline and respiration_24 are missing');
    }
*/
    if (!app.isAnswered('respiration_baseline') || !app.isAnswered('respiration_24')) {
      throw new Error('respiration_baseline and respiration_24 not answered');
    }

    const respiration_baseline = JSON.parse(app.getAnswer('respiration_baseline'));
    const respiration_24 = JSON.parse(app.getAnswer('respiration_24'));
    result.respiration_baseline = respiration_baseline;
    result.respiration_24 = respiration_24;
    const t1 = Date.parse(respiration_baseline.meta.date);
    const t2 = Date.parse(respiration_24.meta.date);
    console.log(`t1 is: ${t1}\nt2 is: ${t2}`);
    result.t1 = t1;
    result.t2 = t2;
  } catch (e) {
    result.error = e;
    console.log(e);
  }

  app.result(result);
})();
