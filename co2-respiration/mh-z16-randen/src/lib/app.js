/* global android */

const DesktopApp = {
  result: (res) => {
    const tmp = JSON.stringify(res, null, 2);
    console.log(`saving result to ./data/result.json:\n${tmp}`);
    const fs = require('fs');
    if (!fs.existsSync('./data')) {
      fs.mkdirSync('./data');
    }
    fs.writeFileSync('./data/result.json', tmp);
    process.exit(0);
  },
  progress: (pct) => {
    console.log(`progress is ${pct} %`);
  },
  onSerialDataIntercepted: (data) => {
    console.log(`serial data intercepted: ${data}`);
  },
  csvExport: (key, value) => {
    console.log(`exporting key value pair to csv: ${key} => ${value}`);
  },
  csvExportValue: (value) => {
    console.log(`exporting value to CSV ${value}`);
  },
  getAnswer: (dataName) => {
    console.log(`get data name ${dataName}`);
    return 'example value';
  },
  save: () => {
    console.log('saving env');
    return null;
  },
  isAndroid: false,
};

const AndroidApp = {
  result: (res) => {
    const tmp = JSON.stringify(res, null, 2);
    android.result(tmp);
  },
  progress: (pct) => {
    android.progress(pct);
  },
  onSerialDataIntercepted: (data) => {
    android.onSerialDataIntercepted(data);
  },
  csvExport: (key, value) => {
    android.csvExport(key, value);
  },
  csvExportValue: (value) => {
    android.csvExport(value);
  },
  getAnswer: dataName => android.getAnswer(dataName),
  save: () => android.save(),
  isAndroid: true,
};

export default (() => {
  if (typeof _ANDROID === 'undefined') {
    return DesktopApp;
  }
  return AndroidApp;
})();
