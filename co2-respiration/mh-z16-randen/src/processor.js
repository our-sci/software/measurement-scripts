/* global processor */

import { sprintf } from 'sprintf-js';
import regression from 'regression';
import app from './lib/app';
import * as ui from './lib/ui';

const result = (() => {
  if (typeof processor === 'undefined') {
    return require('../data/result.json');
  }

  return JSON.parse(processor.getResult());
})();

const co2lres = result.low_res.map(v => v[0]);

const tvoc = result.high_res.map(v => v[2]);
const eco2 = result.high_res.map(v => v[3]);
const hum = result.high_res.map(v => v[4]);
const press = result.high_res.map(v => v[5]);
const co2vals = result.high_res.map(v => v[0]);

const co2 = {
  series: [co2vals],
};

const startIdx = result.started_index;
console.log(`start index is: ${result.start_index}`);

const co2avg = co2vals.reduce((a, b) => a + b, 0) / co2vals.length;

const temp = {
  series: [result.high_res.map(v => v[1])],
};

/*
const lres = {
  series: [result.low_res],
};
*/

if (startIdx != null && startIdx >= 0 && startIdx < co2vals.length) {
  const vals = co2vals.slice(startIdx);

  // const fit = regression.exponential(vals);

  /*
  const fitted = [...vals.keys()].map(idx => {
    Math.exp();
  });
  */

  const duration = vals.length * 2;

  ui.plot(
    {
      series: [vals],
    },
    `CO2 concentration in [ppm] over ${duration} [s]`,
    { min: 0, max: 3000 },
  );
}

ui.plot(co2, 'CO2 concentration in [ppm] over 5 min', { min: 0, max: 4000 });

ui.plot({ series: [co2lres] }, 'CO2 concentration in [ppm] over 5 h', { min: 0, max: 4000 });

ui.plot({ series: [eco2] }, 'ECO2 5 min', { min: 0, max: 50000 });

ui.plot(temp, 'Temp in [°C] over 5 min', { min: -10, max: 50 });

ui.info(sprintf('%.0f', co2avg), 'CO2 avg [ppm]');
ui.info(sprintf('%.0f', Math.max(...co2vals)), 'CO2 max [ppm]');
// ui.plot(lres, 'CO2 concentration in ppm, Low Resolution over 60h', { min: 0, max: 5000 });

ui.plot({ series: [hum] }, 'Air humidity in %', { min: 0, max: 100 });
ui.plot({ series: [press] }, 'Air pressure in [kPa]', { min: 30000, max: 110000 });

app.save();
