#define TIMEOUT 500
#define HIGH_RES_INTERVAL 5
#define LOW_RES_INTERVAL_FACT 24

#define HIGH_RES_COUNT 200
#define LOW_RES_COUNT 1800

uint8_t CMD[] = {0XFF , 0x01 , 0x86 , 0x00 , 0x00 , 0x00, 0x00 , 0x00 , 0x79};

int16_t high_res_values[HIGH_RES_COUNT] = {0};
int16_t low_res_values[LOW_RES_COUNT] = {0};

uint16_t h_head = 0;
uint16_t l_head = 0;

uint16_t high_res_counter = LOW_RES_INTERVAL_FACT; // start off to trigger
uint16_t seconds_counter = 0;

volatile uint8_t next = 0;

IntervalTimer measurementTimer;

void setup() {
  Serial.begin(115200);
  Serial1.begin(9600);
  Serial2.begin(9600);

  delay(1000);
  Serial.print("C02 Sensor MH-Z16\n");
  Serial2.print("C02 Sensor MH-Z16\n");

  measurementTimer.begin(timer, 1000000);

}

void loop() {

  char c = 0;
  while (Serial.available()) {
    c = Serial.read();
  }

  while (Serial2.available()) {
    c = Serial2.read();
  }

  if (c == 'a') {
    print_values();
  }

  process();

  delay(30);
}

int32_t measure() {
  Serial1.write(CMD, sizeof(CMD));
  uint8_t expected = 9, count = 0;
  uint16_t concentration = 0;

  unsigned long s = millis();

  for (;;) {
    if (millis() - s > TIMEOUT) {
      return -1;
    }
    while (Serial1.available()) {
      uint8_t c = Serial1.read();
      if (count == 2) {
        concentration = c << 8;
      } else if (count == 3) {
        concentration |= c;
      }

      count++;
    }

    if (count >= expected) {
      break;
    }

    delay(50);
  }

  return concentration;

}

void timer() {
  seconds_counter++;
  if (seconds_counter % HIGH_RES_INTERVAL == 0) {
    next = 1;
    seconds_counter = 0;
  }
}

void process() {

  if (next == 0) {
    return;
  }
  next = 0;

  high_res_counter++;

  int32_t value = measure();
  if (value < 0) {
    push_high_res(-1);
  } else {
    push_high_res((int16_t)value);
  }


  if (high_res_counter >= LOW_RES_INTERVAL_FACT) {
    high_res_counter = 0;

    if (value < 0) {
      push_low_res(-1);
    } else {
      push_low_res((int16_t)value);
    }
  }


}

void push_high_res(int16_t value) {
  high_res_values[h_head] = value;

  h_head++;
  if (h_head >= HIGH_RES_COUNT) {
    h_head = 0;
  }
}

void push_low_res(int16_t value) {
  low_res_values[l_head] = value;

  l_head++;
  if (l_head >= LOW_RES_COUNT) {
    l_head = 0;
  }
}


void print_high_res() {
  uint32_t i = 0;
  uint32_t len = HIGH_RES_COUNT;
  print("\"high_res\":[");

  uint32_t tail = h_head + 1;

  if (tail < len) {
    for (i = tail; i < len; i++) {
      print(high_res_values[i]);

      if (tail == 1) {
        if (i + 1 < len) {
          print(",\n");
        }
      } else {
        print(",\n");
      }
    }
  }

  for (i = 0 ; i < h_head; i++) {
    print(high_res_values[i]);

    if (i + 1 < h_head) {
      print(",\n");
    }
  }
  print("]");
}

void print_low_res() {
  uint32_t i = 0;
  uint32_t len = LOW_RES_COUNT;
  print("\"low_res\":[");

  uint32_t tail = l_head + 1;

  if (tail < len) {
    for (i = tail; i < len; i++) {
      print(low_res_values[i]);

      if (tail == 1) {
        if (i + 1 < len) {
          print(",\n");
        }
      } else {
        print(",\n");
      }
    }
  }

  for (i = 0 ; i < l_head; i++) {
    print(low_res_values[i]);

    if (i + 1 < l_head) {
      print(",\n");
    }
  }
  print("]");
}

void print(const char *data) {
  Serial.print(data);
  Serial2.print(data);
}

void print(long v) {
  Serial.print(v);
  Serial2.print(v);
}

void print_values() {
  print("{\n");
  print_high_res();
  print(",\n");
  print_low_res();
  print("}\n");
}


