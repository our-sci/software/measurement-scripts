/* global processor */

import { sprintf } from 'sprintf-js';
import app from './lib/app';
import * as ui from './lib/ui';

const result = (() => {
  if (typeof processor === 'undefined') {
    return require('../data/result.json');
  }

  return JSON.parse(processor.getResult());
})();

// plot first 50 measured voltage levels
const sensor1 = {
  series: [result.sensor1],
};

const sensor2 = {
  series: [result.sensor2],
};

ui.plot(sensor1, 'Sensor 1', { min: 0, max: 1500 });
ui.plot(sensor2, 'Sensor 2', { min: 0, max: 5000 });

app.save();
