/**
 * Based on the more simple sensor.js, this script
 * allows a json to be intercepted while it is not finished
 * using the readStreamingJson function.
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

import app from './lib/app';
import serial from './lib/serial';
// import { sleep } from './lib/utils';

export default serial; // expose this to android for calling onDataAvailable

(async () => {
  await serial.init('/dev/ttyACM0', {
    baudRate: 115200,
    dataBits: 8,
    stopBits: 1,
    parity: 'none',
  });

  // instead of reading from the serial port mock some data
  // serial.feed('./mock/mock_sensor_output.json');

  console.log('sending a');
  serial.write('a');

  const expected = 512;
  let count = 0;

  try {
    const res = await serial.readStreamingJson('data[*]', (item) => {
      console.log(`item ${count} / ${expected}  = ${item}`);
      const frac = count / expected;
      app.progress(frac * 100);
      count += 1;
    });

    console.log(`res ${JSON.stringify(res)}`);
    app.result(res);
  } catch (err) {
    console.error(`error reading json: ${err}`);
  }
})();
