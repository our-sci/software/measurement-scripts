/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */

import * as MathMore from './math';

/*
import {
  warning
} from './ui';
*/

export default (meas, wavelengths) => {
  // DEBUG  console.log(`hello ${result}`);

  // ///////////////////////////////////////////////////////
  // Set the conditions of the measurement - which lights, # pulses, # of pulses to ignore due to heating. etc.
  //  const wavelengths = [370, 395, 420, 530, 605, 650, 730, 850, 880, 940];  // this is now pulled from sensor.js
  const pulses = 60;
  const pulse_distance = 1.5;
  const partPulses = 2 / 3;
  // ///////////////////////////////////////////////////////

  // ///////////////////////////////////////////////////////
  // determine if this is a calibration script or not, and pass that back to sensor.js so it can return data back to device to save
  let calibration = 0;
  if (typeof meas.calibration === 'undefined') {
    calibration = 0;
  } else {
    calibration = meas.calibration;
  }

  // ///////////////////////////////////////////////////////
  /* need to add once converting to calibration script
  if (calibration === 0) {
      // get calibration information (min and max reflectance) from device

      let ir_high = meas.shiney_ir;
      let ir_low = meas.black_ir;

      let vis_high = meas.shiney_vis;
      let vis_low = meas.black_vis;

      let ir_blank = meas.blank_ir;
      let vis_blank = meas.blank_vis;
  }
  */

  // ///////////////////////////////////////////////////////
  // get the sample values from the raw trace, put them into an array
  const sample_values_raw = [];
  const sample_voltage_raw = [];
  for (let j = 0; j < wavelengths.length; j++) {
    sample_values_raw[j] = meas.data_raw.slice(pulses * j, pulses * (j + 1));
    sample_voltage_raw[j] = meas.voltage_raw.slice(pulses * j, pulses * (j + 1));
  }
  // TEST
  // info("sample_values_raw: " + sample_values_raw[0].toString())
  // ///////////////////////////////////////////////////////

  // ///////////////////////////////////////////////////////
  // Then, we choose the last few pulses to use to avoid the heating effect
  for (let j = 0; j < wavelengths.length; j++) {
    sample_values_raw[j] = sample_values_raw[j].slice(pulses * partPulses, pulses);
    sample_voltage_raw[j] = sample_voltage_raw[j].slice(pulses * partPulses, pulses);
  }
  // TEST
  // info("sample_values_raw: " + JSON.stringify(sample_values_raw));
  // ///////////////////////////////////////////////////////

  // ///////////////////////////////////////////////////////
  // And finally, we're going to straighten out the pulses to reduce our standard deviation using a linear regression and correction
  // first we need a time array to plug into the regression formula (the x of y = mx + b)
  const timeArray = [];
  for (let z = 0; z < pulses - pulses * partPulses; z++) {
    timeArray[z] = z * pulse_distance;
  }
  // Then we need to create the new variable to store the flattened values
  const sample_voltage_flat = [];
  const sample_values_flat = [];
  for (let j = 0; j < wavelengths.length; j++) {
    const tmp_array = [];
    sample_values_flat[j] = tmp_array;
    sample_voltage_flat[j] = tmp_array;
  }

  const sample_values_adjustments = [];
  const sample_values_slope = [];
  const sample_values_yint = [];
  const sample_values_center = [];
  const sample_values_time = [];

  // now we can do the straightening via regression + correction
  for (let j = 0; j < wavelengths.length; j++) {
    sample_values_adjustments[j] = [];
    sample_values_slope[j] = [];
    sample_values_yint[j] = [];
    sample_values_center[j] = [];
    sample_values_time[j] = [];
    const reg = MathMore.MathLINREG(timeArray, sample_values_raw[j]);
    // what is the center point of rotation for the line (halfway through the array) - that's the value from which we will adjust other values
    const centerPoint = reg.m * (pulses - pulses * partPulses) / 2 + reg.b;
    for (let i = 0; i < sample_values_raw[j].length; i++) {
      //      const adjustment = centerPoint - (reg.m * timeArray[i] + reg.b);
      const adjustment = 0;
      //      sample_values_flat[j][i] = sample_values_raw[j][i]; // + adjustment;
      sample_values_flat[j].push(sample_values_raw[j][i]); // + adjustment;
      sample_values_adjustments[j][i] = adjustment;
      sample_values_slope[j][i] = reg.m;
      sample_values_yint[j][i] = reg.b;
      sample_values_center[j][i] = centerPoint;
      sample_values_time[j][i] = timeArray[i];
      // TEST //    output["adjustment_"+wavelengths[j]+"_"+i] = adjustment;
    }
  }

  // ///////////////////////////////////////////////////////

  // ///////////////////////////////////////////////////////
  // now convert the data into a percentage (0 - 100) based on the min and max reflectance saved in the device.  (if it's a calibration, don't do that!)
  const sample_values_perc = sample_values_flat;
  /* not necessary when outputting the percentage (0 - 100) directly from device, for ease of use set sample_value_raw == sample_values_perc
  let sample_values_perc = [];
  for (let j = 0; j < wavelengths.length; j++) {
      let tmp_array = [];
      sample_values_perc[j] = tmp_array;
  }
  for (let j = 0; j < wavelengths.length; j++) {
      for (let i = 0; i < sample_values_flat[j].length; i++) {
          if (calibration > 0) {
              sample_values_perc[j][i] = sample_values_flat[j][i];
          }
          else {
              sample_values_perc[j][i] = 100 * (sample_values_flat[j][i] - max_reflectance_Low[j]) / (max_reflectance_High[j] - max_reflectance_Low[j]);
          }
      }
  }
  */
  // ///////////////////////////////////////////////////////

  // ///////////////////////////////////////////////////////
  // TEST // Compare flattened, unflattened, and percentage values
  // ui.info('sample values flat', JSON.stringify(sample_values_flat[0]));
  // ui.info("sample_values_raw: ", JSON.stringify(sample_values_raw[0]));
  // ui.info("sample_values_perc: ", JSON.stringify(sample_values_perc[0]));
  // ///////////////////////////////////////////////////////

  // ///////////////////////////////////////////////////////
  // Now we can pull the median, standard deviation, spad, and bits from these adjusted + corrected values
  // Note - to calculate bits, we need to convert the median value back up to a 16 bit value to calculate bits, so there's some math to convert it back to a raw value there
  const median = [];
  //  const median_raw = [];
  //  const absorbance = [];
  const stdev = [];
  //  const stdev_raw = [];
  const three_stdev = [];
  //  const three_stdev_raw = [];
  //  const bits = [];
  //  const bits_actual = [];
  const spad = [];
  const warnings = [];
  const info = [];
  // ///////////////////////////////////////////////////////

  for (let j = 0; j < wavelengths.length; j++) {
    median[j] = MathMore.MathMEDIAN(sample_values_perc[j]);
    if (median[j] === 'NaN') {
      warnings.push('The ' + wavelengths[j] + 'nm LED is not a valid number.  It is possible the device is missing calibration information, or there are some zero values which are making calculations impossible (divide by zero errors).');
    }
    // median_raw[j] = MathMore.MathMEDIAN(sample_values_flat[j]);
    // absorbance[j] = -1 * Math.log(median[j] / 100);
    stdev[j] = MathMore.MathSTDEV(sample_values_perc[j]);
    if (stdev[j] >= 35) {
      warnings.push('The ' + wavelengths[j] + 'nm LED appears noisier than normal.  Look at the "Raw Spectral Response"  graph and consider re-measuring the sample.  Pay attention to sources of vibration like wind, hand shaking, etc.');
    }

    //    stdev_raw[j] = MathMore.MathSTDEV(sample_values_flat[j]);
    three_stdev[j] = 3 * stdev[j];
    //    three_stdev_raw[j] = 3 * stdev_raw[j];
    //    bits[j] = 15 - MathMore.MathLOG(stdev_raw[j] * 2) / MathMore.MathLOG(2);
    //    if (calibration === 0) {
    //        bits_actual[j] = (15 - MathMore.MathLOG((65536 / max_reflectance[j]) * stdev_raw[j] * 2) / MathMore.MathLOG(2));
    //    }
  }
  for (let j = 0; j < wavelengths.length; j++) {
    // ok this is super wonky, but just going to shift the range up by 10 counts for everything.  Since they are all 0 - 100, this shouldn't have an impact except to 
    // keep us from getting NaNs for SPAD values
    spad[j] = 100 * Math.log((median[9] + 8) / (median[j] + 8)); // because we've already normalized values from 0 (black) to 100 (shiney), the max value is always 100 so just divide by 100
  }

  return {
    //    calibration,
    wavelengths,
    median,
    //    median_raw,
    //    absorbance,
    stdev,
    //    stdev_raw,
    three_stdev,
    spad,
    warnings,
    info,
    sample_values_perc,
    sample_voltage_raw,
    sample_values_flat,
    sample_values_raw,
    sample_values_adjustments,
    sample_values_slope,
    sample_values_yint,
    sample_values_center,
    sample_values_time,
    //    three_stdev_raw,
    //    bits,
    //    bits_actual,
  };
};