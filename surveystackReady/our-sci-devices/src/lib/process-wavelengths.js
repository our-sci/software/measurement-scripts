/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */

import * as MathMore from './math'

/*
import {
  warning
} from './ui';
*/

export default (meas, wavelengths, detectors) => {
  // This should be generalized to pass a specific set of arrays, rather than an entire measurement...
  // That way, it could be run on voltage and the detector measurements separately, avoiding the production of nan's in the output (processed).

  // ///////////////////////////////////////////////////////
  // Set the conditions of the measurement - which lights, # pulses, # of pulses to ignore due to heating. etc.
  //  const wavelengths = [370, 395, 420, 530, 605, 650, 730, 850, 880, 940];  // this is now pulled from sensor.js
  const pulses = 60
  const pulse_distance = 1.5
  const partPulses = 2 / 3
  // ///////////////////////////////////////////////////////

  // ///////////////////////////////////////////////////////
  // determine if this is a calibration script or not, and pass that back to sensor.js so it can return data back to device to save
  let calibration = 0
  if (typeof meas.calibration === 'undefined') {
    calibration = 0
  } else {
    calibration = meas.calibration
  }

  // ///////////////////////////////////////////////////////
  // get the sample values from the raw trace, convert them to numbers from strings, and put them into an array organized by wavelength
  const sample_values_raw = []
  const sample_voltage_raw = []
  for (let j = 0; j < wavelengths.length; j++) {
    sample_values_raw[j] = []
    sample_voltage_raw[j] = []
    for (let i = pulses * j; i < pulses * (j + 1); i++) {
      sample_values_raw[j].push(Number(meas.data_raw[i]))
      if (typeof meas.voltage_raw !== 'undefined') {
        sample_voltage_raw[j].push(Number(meas.voltage_raw[i]))
      }
    }
  }
  //  console.log(sample_values_raw);
  //  console.log(sample_voltage_raw);

  // ///////////////////////////////////////////////////////
  // Then, we choose the last few pulses to use to avoid the heating effect
  for (let j = 0; j < wavelengths.length; j++) {
    sample_values_raw[j] = sample_values_raw[j].slice(pulses * partPulses, pulses)
    sample_voltage_raw[j] = sample_voltage_raw[j].slice(pulses * partPulses, pulses)
  }

  // ///////////////////////////////////////////////////////
  // And finally, we're going to straighten out the pulses to reduce our standard deviation using a linear regression and correction
  // first we need a time array to plug into the regression formula (the x of y = mx + b)
  const timeArray = []
  for (let z = 0; z < pulses - pulses * partPulses; z++) {
    timeArray[z] = z * pulse_distance
  }
  // now we can do the straightening via regression + correction
  const sample_values_adjustments = []
  for (let j = 0; j < wavelengths.length; j++) {
    sample_values_adjustments[j] = []
    const regArray = sample_values_raw.slice(j, j + 1)
    const reg = MathMore.MathLINREG(timeArray, regArray[0])
    //    console.log(reg.m, reg.b);
    // what is the center point of rotation for the line (halfway through the array) - that's the value from which we will adjust other values
    const centerPoint = reg.m * (pulses - pulses * partPulses) / 2 + reg.b
    for (let i = 0; i < regArray[0].length; i++) {
      const adjustment = centerPoint - (reg.m * timeArray[i] + reg.b)
      sample_values_adjustments[j].push(adjustment)
    }
  }

  // VOLTAGE - now we can do the straightening via regression + correction
  const voltage_values_adjustments = []
  for (let j = 0; j < wavelengths.length; j++) {
    voltage_values_adjustments[j] = []
    const regArray = sample_voltage_raw.slice(j, j + 1)
    const reg = MathMore.MathLINREG(timeArray, regArray[0])
    //    console.log(reg.m, reg.b);
    // what is the center point of rotation for the line (halfway through the array) - that's the value from which we will adjust other values
    const centerPoint = reg.m * (pulses - pulses * partPulses) / 2 + reg.b
    for (let i = 0; i < regArray[0].length; i++) {
      const adjustment = centerPoint - (reg.m * timeArray[i] + reg.b)
      voltage_values_adjustments[j].push(adjustment)
    }
  }

  // --> this is completely crazy
  // in chrome --> console, look at lines console log for raw versus flat... somehow, when printing in chrome as array, it is sorted.  Also when pushed to processor later it is sorted... why why why?
  // save the adjusted values to a new array called sample_values_flat
  const sample_voltage_flat = []
  const sample_values_flat = []
  for (let j = 0; j < wavelengths.length; j++) {
    sample_values_flat[j] = []
    sample_voltage_flat[j] = []
    for (let i = 0; i < sample_values_raw[j].length; i++) {
      //      console.log(sample_values_raw[j][i]);
      //      console.log(sample_values_adjustments[j][i]);
      const value = sample_values_raw[j][i] + sample_values_adjustments[j][i]
      const value2 = sample_voltage_raw[j][i] + voltage_values_adjustments[j][i]
      //      console.log(value);
      sample_values_flat[j][i] = value
      sample_voltage_flat[j][i] = value2
      //      console.log(sample_values_flat[j][i]);
    }
  }
  /*
  for (let i = 0; i < sample_values_raw[0].length; i++) {
    console.log(sample_values_raw[0][i]);
    console.log(sample_values_flat[0][i]);
  }
  console.log(sample_values_raw[0]);
  console.log(sample_values_flat[0]);
*/

  // save and output this... change to 16 repeats, 2 minutes each.
  // test 02 device, make sure output works... or update 16 device
  // then.../' fridge for 16m, start then hot for 15m, then back to fridge for 15 min.

  // ///////////////////////////////////////////////////////
  // now convert the data into a percentage (0 - 100) based on the min and max reflectance saved in the device.  (if it's a calibration, don't do that!)
  // OPTIONAL
  const sample_values_perc = sample_values_flat

  // ///////////////////////////////////////////////////////
  // TEST // Compare flattened, unflattened, and percentage values
  // ui.info('sample values flat', JSON.stringify(sample_values_flat[0]));
  // ui.info("sample_values_raw: ", JSON.stringify(sample_values_raw[0]));
  // ///////////////////////////////////////////////////////

  // ///////////////////////////////////////////////////////
  // Now we can pull the median, standard deviation, spad, and bits from these adjusted + corrected values
  // Note - to calculate bits, we need to convert the median value back up to a 16 bit value to calculate bits, so there's some math to convert it back to a raw value there
  const median = []
  //  const median_raw = [];
  //  const absorbance = [];
  const stdev = []
  //  const stdev_raw = [];
  const three_stdev = []
  //  const three_stdev_raw = [];
  //  const bits = [];
  //  const bits_actual = [];
  const spad = []
  const warnings = []
  const info = []
  // ///////////////////////////////////////////////////////

  for (let j = 0; j < wavelengths.length; j++) {
    median[j] = MathMore.MathMEDIAN(sample_values_perc[j])
    if (median[j] === 'NaN') {
      warnings.push(`The ${wavelengths[j]}nm LED is not a valid number.  It is possible the device is missing calibration information or there are some zero values which are making calculations impossible (divide by zero errors).`)
    }
    // median_raw[j] = MathMore.MathMEDIAN(sample_values_flat[j]);
    // absorbance[j] = -1 * Math.log(median[j] / 100);
    stdev[j] = MathMore.MathSTDEV(sample_values_perc[j])
    if ((calibration === 0 || calibration === 3 || calibration === 5 || calibration === 8) && stdev[j] >= 50 && median[j] <= 65535) { // catch noisy or spiky response (stdev > 0.5% of full range)
      warnings.push(`The ${wavelengths[j]}nm LED appears noisier than normal (standard deviation of ${Math.round(stdev[j])} which is ${Math.round(10000 * stdev[j] / median[j]) / 100}% of your measurement ${median[j]}).  Look at the Raw Spectral Response  graph and consider re-measuring the sample.  Pay attention to sources of vibration like wind hand shaking etc.`)
    }
    if ((calibration === 1 || calibration === 2 || calibration === 4 | median[j] > 65535) && stdev[j] >= 500000) { // catch noisy or spiky response (stdev > 0.5% of full range), include anything which is spiking over the 16 bit range and therefore a raw measurement.  This catches standardHeatTest
      warnings.push(`The ${wavelengths[j]}nm LED appears noisier than normal (standard deviation of ${Math.round(stdev[j])} which is ${Math.round(10000 * stdev[j] / median[j]) / 100}% of your measurement ${median[j]}).  Look at the Raw Spectral Response  graph and consider re-measuring the sample.  Pay attention to sources of vibration like wind hand shaking etc.`)
    }

    //    stdev_raw[j] = MathMore.MathSTDEV(sample_values_flat[j]);
    three_stdev[j] = 3 * stdev[j]
    //    three_stdev_raw[j] = 3 * stdev_raw[j];
    //    bits[j] = 15 - MathMore.MathLOG(stdev_raw[j] * 2) / MathMore.MathLOG(2);
    //    if (calibration === 0) {
    //        bits_actual[j] = (15 - MathMore.MathLOG((65536 / max_reflectance[j]) * stdev_raw[j] * 2) / MathMore.MathLOG(2));
    //    }
  }
  for (let j = 0; j < wavelengths.length; j++) {
    // ok this is super wonky, but just going to shift the range up by 10 counts for everything.  Since they are all 0 - 100, this shouldn't have an impact.
    // keep us from getting NaNs for SPAD values
    spad[j] = 100 * Math.log((median[9] + 8) / (median[j] + 8)) // because we've already normalized values from 0 (black) to 100 (shiney), the max value is always 100 so just divide by 100
  }

  // VOLTAGE -
  const voltage_median = []
  const voltage_stdev = []
  const voltage_three_stdev = []

  if (typeof meas.voltage_raw !== 'undefined') {
    for (let j = 0; j < wavelengths.length; j++) {
      voltage_median[j] = MathMore.MathMEDIAN(sample_voltage_flat[j])
      if (voltage_median[j] === 'NaN') {
        warnings.push('The ' + wavelengths[j] + 'nm voltage is not a valid number.  It is possible the device is missing calibration information or there are some zero values which are making calculations impossible (divide by zero errors).')
      }
      voltage_stdev[j] = MathMore.MathSTDEV(sample_voltage_flat[j])
      if (voltage_stdev[j] >= 100) {
        warnings.push('The ' + wavelengths[j] + 'nm voltage appears noisier than normal.  Look at the Raw Spectral Response  graph and consider re-measuring the sample.  Pay attention to sources of vibration like wind hand shaking etc.')
      }
      voltage_three_stdev[j] = 3 * voltage_stdev[j]
    }
  }

  return {
    wavelengths,
    detectors,
    median,
    stdev,
    //    calibration,
    //    median_raw,
    //    absorbance,
    //    stdev_raw,
    three_stdev,
    spad,
    warnings,
    info,
    //    sample_values_raw,
    //    sample_voltage_flat,
    //    sample_values_perc,
    //    sample_values_adjustments,
    //    sample_voltage_raw,
    voltage_median,
    voltage_stdev,
    voltage_three_stdev
    //    three_stdev_raw,
    //    bits,
    //    bits_actual,
  }
}
