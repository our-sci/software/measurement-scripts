/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */

export default () => {
  const calibrateTeflon = [{ // Calibrate a device to the device's teflon standard
    calibration: 1,
    averages: 3,
    //      pulses: [10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10],
    pulses: [60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60],
    data_type: [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
    pulse_length: [
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7]
    ],
    pulse_distance: [
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500,
      1500
    ],
    pulsed_lights: [
      [1],
      [2],
      [3],
      [4],
      [5],
      [6],
      [7],
      [8],
      [9],
      [10],
      [1],
      [2],
      [3],
      [4],
      [5],
      [6],
      [7],
      [8],
      [9],
      [10]
    ],
    pulsed_lights_brightness: [
      [35],
      [50],
      [550],
      [680],
      [500],
      [300],
      [875],
      [925],
      [250],
      [65],
      [35],
      [50],
      [550],
      [680],
      [500],
      [300],
      [875],
      [925],
      [250],
      [65]
    ],
    detectors: [
      [0],
      [0],
      [0],
      [0],
      [0],
      [0],
      [0],
      [0],
      [0],
      [0],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1]
    ]
  },
  {
    environmental: [
      ['temperature_humidity_pressure_voc']
    ]
  }
  ]

  // When making a copy of another protocol, JSON.parse(JSON.stringify()) is needed to ensure that you are creating an entirely separate copy (not referenced to the original)
  // Calibrate a device to an open blank by point open window to empty space.
  const calibrateOpen = JSON.parse(JSON.stringify(calibrateTeflon))
  calibrateOpen[0]['calibration'] = 2

  // Create and save a blank value, to be applied as an offset when using the standard_blank measurement on samples.
  const calibrateBlank = JSON.parse(JSON.stringify(calibrateTeflon))
  calibrateBlank[0]['calibration'] = 3
  calibrateBlank[0]['data_type'] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  // calibrateBlank[0]['data_type'] = [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2];

  // no longer used!!!
  const calibrateTeflonMaster = JSON.parse(JSON.stringify(calibrateTeflon))
  calibrateTeflonMaster[0]['calibration'] = 4
  calibrateTeflonMaster[0]['data_type'] = [
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0
  ]

  const calibrateOpenMaster = JSON.parse(JSON.stringify(calibrateTeflon))
  calibrateOpenMaster[0]['calibration'] = 5
  calibrateOpenMaster[0]['data_type'] = [
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0
  ]

  // Standard reflectometer measurement for field and lab use
  const standard = [{
    averages: 3,
    pulses: [60, 60, 60, 60, 60, 60, 60, 60, 60, 60],
    pulse_length: [
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7]
    ],
    pulse_distance: [1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500],
    pulsed_lights: [
      [3],
      [4],
      [5],
      [9],
      [6],
      [7],
      [8],
      [1],
      [2],
      [10]
    ],
    pulsed_lights_brightness: [
      [550],
      [680],
      [500],
      [250],
      [300],
      [875],
      [925],
      [35],
      [50],
      [65]
    ],
    detectors: [
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [0],
      [0],
      [0]
    ]
  },
  {
    environmental: [
      ['temperature_humidity_pressure_voc']
    ]
  }
  ]

  // Test a set of intensities near the normal standard intensity set, for device manufacture only
  const testIntensity = [{
    calibration: 7,
    data_type: [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
    show_voltage: 1,
    //      averages: 3,
    pulses: [60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60],
    pulse_length: [
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7]
    ],
    pulse_distance: [1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500],
    pulsed_lights: [
      [3],
      [3],
      [3],
      [3],
      [4],
      [4],
      [4],
      [4],
      [5],
      [5],
      [5],
      [5],
      [9],
      [9],
      [9],
      [9],
      [6],
      [6],
      [6],
      [6],
      [7],
      [7],
      [7],
      [7],
      [8],
      [8],
      [8],
      [8],
      [1],
      [1],
      [1],
      [1],
      [2],
      [2],
      [2],
      [2],
      [10],
      [10],
      [10],
      [10]
    ],
    // after evaluation of actual range, calculated range is this...
    //       360,475,591,707
    // 579,637,695,753
    // 513,540,566,593
    // 208,234,261,287
    // 281,292,303,314
    // 847,862,876,891
    // 827,875,923,971
    // 29,32,35,38
    // 38,45,51,58
    // 56,61,66,71

    pulsed_lights_brightness: [
      [360],
      [475],
      [591],
      [707],
      [579],
      [637],
      [695],
      [753],
      [513],
      [540],
      [566],
      [593],
      [208],
      [234],
      [261],
      [287],
      [260],
      [280],
      [300],
      [320],
      [847],
      [862],
      [876],
      [891],
      [827],
      [875],
      [923],
      [971],
      [29],
      [32],
      [35],
      [38],
      [45],
      [48],
      [51],
      [58],
      [56],
      [61],
      [66],
      [71]
      // [510],
      // [530],
      // [550],
      // [570],
      // [640],
      // [660],
      // [680],
      // [700],
      // [460],
      // [480],
      // [500],
      // [520],
      // [230],
      // [240],
      // [250],
      // [260],
      // [260],
      // [280],
      // [300],
      // [320],
      // [825],
      // [850],
      // [875],
      // [900],
      // [875],
      // [900],
      // [925],
      // [950],
      // [33],
      // [34],
      // [35],
      // [36],
      // [46],
      // [48],
      // [50],
      // [52],
      // [55],
      // [60],
      // [65],
      // [70],
    ],
    detectors: [
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [0],
      [0],
      [0],
      [0],
      [0],
      [0],
      [0],
      [0],
      [0],
      [0],
      [0],
      [0]
    ]
  },
  {
    environmental: [
      ['temperature_humidity_pressure_voc']
    ]
  }
  ]

  // FIX THIS.. NEEDS TO BE UPDATED
  const standardSample = JSON.parse(JSON.stringify(standard))
  standardSample[0]['recall'] = [];
  ['6', '7', '8'].forEach((num) => {
    standardSample[0]['recall'].push(`userdef[${num}]`)
    standardSample[0]['recall'].push(`userdef[1${num}]`)
    standardSample[0]['recall'].push(`userdef[2${num}]`)
  })

  // Get results w/ nomalization applied, and receieve normalized results back as well.
  const standardSoil = JSON.parse(JSON.stringify(standard))
  standardSoil[0]['normalized'] = 'soil'
  standardSoil[0]['recall'] = [];
  ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'].forEach((num) => {
    standardSoil[0]['recall'].push(`normalized_soil[${num}]`)
  })
  const standard5mlcuvette = JSON.parse(JSON.stringify(standard))
  standard5mlcuvette[0]['normalized'] = '5mlcuvette'
  standard5mlcuvette[0]['recall'] = [];
  ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'].forEach((num) => {
    standard5mlcuvette[0]['recall'].push(`normalized_5mlcuvette[${num}]`)
  })
  const standard1mlcuvette = JSON.parse(JSON.stringify(standard))
  standard1mlcuvette[0]['normalized'] = '1mlcuvette'
  standard1mlcuvette[0]['recall'] = [];
  ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'].forEach((num) => {
    standard1mlcuvette[0]['recall'].push(`normalized_1mlcuvette[${num}]`)
  })
  const standardCustom = JSON.parse(JSON.stringify(standard))
  standardCustom[0]['normalized'] = 'custom'
  standardCustom[0]['recall'] = [];
  ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'].forEach((num) => {
    standardCustom[0]['recall'].push(`normalized_custom[${num}]`)
  })

  // Test a set of intensities near the normal standard intensity set for IR lights only, for device manufacture only
  const testIntensityIr = JSON.parse(JSON.stringify(testIntensity))
  testIntensityIr[0].data_type.splice(0, 28)
  testIntensityIr[0].pulses.splice(0, 28)
  testIntensityIr[0].pulse_length.splice(0, 28)
  testIntensityIr[0].pulse_distance.splice(0, 28)
  testIntensityIr[0].pulsed_lights.splice(0, 28)
  testIntensityIr[0].pulsed_lights_brightness.splice(0, 28)
  testIntensityIr[0].detectors.splice(0, 28)

  // When making a copy of another protocol, JSON.parse(JSON.stringify()) is needed to ensure that you are creating an entirely separate copy (not referenced to the original)

  // Standard reflectomter measurement with blank offset, used on samples in cuvettes or similar situations in which blanks are useful.  Use the calibrateAllBlank measurement on the blank itself to save the blank values to the device
  const standardWarmup = JSON.parse(JSON.stringify(standard))
  standardWarmup[0]['averages'] = 9

  const standardStress = JSON.parse(JSON.stringify(standard))
  //  standardStress[0]['averages'] = 4000;
  standardStress[0]['averages'] = 4000
  standardStress[0]['averages_delay'] = 5000

  // Standard reflectomter measurement with blank offset, used on samples in cuvettes or similar situations in which blanks are useful.  Use the calibrateAllBlank measurement on the blank itself to save the blank values to the device
  const standardBlank = JSON.parse(JSON.stringify(standard))
  standardBlank[0]['data_type'] = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

  // Standard reflectomter blank offset which saves result to the local Environment variable. standard measurement uses if found, then resets.
  const calibrateBlankLocal = JSON.parse(JSON.stringify(standard))
  calibrateBlankLocal[0]['calibration'] = 6

  // FIX THIS.. NEEDS TO BE UPDATED
  const standardApplyCalibration = JSON.parse(JSON.stringify(standard))
  standardApplyCalibration[0]['recall'] = []
  for (let i = 0; i < 31; i++) {
    standardApplyCalibration[0]['recall'].push('userdef[' + i + ']')
  }

  // outputs raw data only, use standard reflectometer measurement for normal use
  const standardRaw = JSON.parse(JSON.stringify(standard))
  standardRaw[0]['data_type'] = [2, 2, 2, 2, 2, 2, 2, 2, 2, 2]
  standardRaw[0]['show_voltage'] = 1

  // outputs raw data only, use standard reflectometer measurement for normal use.  Heat calibration not applied.
  const standardRawNoHeat = JSON.parse(JSON.stringify(standard))
  standardRawNoHeat[0]['data_type'] = [3, 3, 3, 3, 3, 3, 3, 3, 3, 3]
  standardRawNoHeat[0]['show_voltage'] = 1 // make sure that voltage is outputted

  // calibration check against reference values (ir/vis_high_master), using the standard run of LEDs
  const standardCheck = JSON.parse(JSON.stringify(standard))
  standardCheck[0]['calibration'] = 8
  standardCheck[0]['recall'] = []
  for (let i = 1; i < 11; i++) {
    standardCheck[0]['recall'].push(`ir_high_master[${i}]`)
    standardCheck[0]['recall'].push(`vis_high_master[${i}]`)
  }

  // confirms teflon calibration, using the extended run of all leds, all detectors
  const calibrateTeflonCheck = JSON.parse(JSON.stringify(calibrateTeflon))
  calibrateTeflonCheck[0]['calibration'] = 5
  delete calibrateTeflonCheck[0]['data_type']

  // outputs raw data only, use standard reflectometer measurement for normal use.  Heat calibration not applied.
  const calibrateTeflonNoHeat = JSON.parse(JSON.stringify(calibrateTeflon))
  //  calibrateTeflonNoHeat[0]['calibration'] = 0;
  calibrateTeflonNoHeat[0]['data_type'] = [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3]
  calibrateTeflonNoHeat[0]['show_voltage'] = 1 // make sure that voltage is outputted

  // Used to test the heat calibration StandardRawHeatCal.
  const standardHeatTest = JSON.parse(JSON.stringify(standardRaw))
  standardHeatTest[0]['show_voltage'] = 1 // make sure that voltage is outputted
  standardHeatTest[0]['protocols'] = 12
  standardHeatTest[0]['protocols_delay'] = 240000
  standardHeatTest[0]['environmental'] = [ // take temp measurements every time (not just at the end)
    ['temperature_humidity_pressure_voc']
  ]
  standardHeatTest.splice(1, 1) // get rid of temp measurement at the end since now we're taking it every time.

  // outputs raw data only without heat (voltage) adjustement, repeats 12 times over 24 minutes.  Use standard reflectometer measurement for normal use.  Heat calibration not applied.
  const standardRawHeatCal = JSON.parse(JSON.stringify(standardRawNoHeat))
  standardRawHeatCal[0]['protocols'] = 12
  standardRawHeatCal[0]['protocols_delay'] = 240000
  standardRawHeatCal[0]['environmental'] = [ // take temp measurements every time (not just at the end)
    ['temperature_humidity_pressure_voc']
  ]
  standardRawHeatCal.splice(1, 1) // get rid of temp measurement at the end since now we're taking it every time.

  // outputs raw data only without heat (voltage) adjustement, repeats 12 times over 2.4 minutes.  Use standard reflectometer measurement for normal use.  Heat calibration not applied.
  const standardRawHeatCalShort = JSON.parse(JSON.stringify(standardRawHeatCal))
  standardRawHeatCalShort[0]['protocols_delay'] = 2400

  // Measure temperature, pressure, humidity, and VOCs only
  const environmental_only = [{
    environmental: [
      ['temperature_humidity_pressure_voc']
    ]
  }]

  // Sets all values back to pre-calibration factory settings (0 or 100)
  // Sets just colorcal_blanks... which seem to be a problem sometime
  // reset vis/ir high/low normal/master
  // const resetRange = 'set_colorcal_blanks+0+0+0+0+1+0+0+0+2+0+0+0+3+0+0+0+4+0+0+0+5+0+0+0+6+0+0+0+7+0+0+0+8+0+0+0+9+0+0+0+10+0+0+0+-1+set_ir_high+0+655350000+1+655350000+2+655350000+3+655350000+4+655350000+5+655350000+6+655350000+7+655350000+8+655350000+9+655350000+10+655350000+-1+set_ir_low+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_vis_high+0+655350000+1+655350000+2+655350000+3+655350000+4+655350000+5+655350000+6+655350000+7+655350000+8+655350000+9+655350000+10+655350000+-1+set_vis_low+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_ir_high_master+0+10000+1+10000+2+10000+3+10000+4+10000+5+10000+6+10000+7+10000+8+10000+9+10000+10+10000+-1+set_ir_low_master+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_vis_high_master+0+10000+1+10000+2+10000+3+10000+4+10000+5+10000+6+10000+7+10000+8+10000+9+10000+10+10000+-1+set_vis_low_master+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+print_memory+'
  // reset vis/ir high/low
  const resetRange = 'set_ir_high+0+655350000+1+655350000+2+655350000+3+655350000+4+655350000+5+655350000+6+655350000+7+655350000+8+655350000+9+655350000+10+655350000+-1+set_ir_low+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_vis_high+0+655350000+1+655350000+2+655350000+3+655350000+4+655350000+5+655350000+6+655350000+7+655350000+8+655350000+9+655350000+10+655350000+-1+set_vis_low+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+print_memory+'
  // reset vis/ir high/low
  const resetRangeManufacturer = 'set_ir_high+0+655350000+1+655350000+2+655350000+3+655350000+4+655350000+5+655350000+6+655350000+7+655350000+8+655350000+9+655350000+10+655350000+-1+set_ir_low+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_vis_high+0+655350000+1+655350000+2+655350000+3+655350000+4+655350000+5+655350000+6+655350000+7+655350000+8+655350000+9+655350000+10+655350000+-1+set_vis_low+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_ir_high_master+0+10000+1+10000+2+10000+3+10000+4+10000+5+10000+6+10000+7+10000+8+10000+9+10000+10+10000+-1+set_ir_low_master+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_vis_high_master+0+10000+1+10000+2+10000+3+10000+4+10000+5+10000+6+10000+7+10000+8+10000+9+10000+10+10000+-1+set_vis_low_master+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+print_memory+'
  // reset all eeprom values
  const resetAll = 'set_normalized_soil+0+1+1+1+2+1+3+1+4+1+5+1+6+1+7+1+8+1+9+1+10+1+-1+set_normalized_5mlcuvette+0+1+1+1+2+1+3+1+4+1+5+1+6+1+7+1+8+1+9+1+10+1+-1+set_normalized_1mlcuvette+0+1+1+1+2+1+3+1+4+1+5+1+6+1+7+1+8+1+9+1+10+1+-1+set_normalized_custom+0+1+1+1+2+1+3+1+4+1+5+1+6+1+7+1+8+1+9+1+10+1+-1+set_colorcal_blanks+0+0+0+0+1+0+0+0+2+0+0+0+3+0+0+0+4+0+0+0+5+0+0+0+6+0+0+0+7+0+0+0+8+0+0+0+9+0+0+0+10+0+0+0+-1+set_ir_high+0+655350000+1+655350000+2+655350000+3+655350000+4+655350000+5+655350000+6+655350000+7+655350000+8+655350000+9+655350000+10+655350000+-1+set_ir_low+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_vis_high+0+655350000+1+655350000+2+655350000+3+655350000+4+655350000+5+655350000+6+655350000+7+655350000+8+655350000+9+655350000+10+655350000+-1+set_vis_low+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_user_defined+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+11+0+12+0+13+0+14+0+15+0+16+0+17+0+18+0+19+0+20+0+21+0+22+0+23+0+24+0+25+0+26+0+27+0+28+0+29+0+30+0+31+0+32+0+33+0+34+0+35+0+36+0+37+0+38+0+39+0+40+0+41+0+42+0+43+0+44+0+45+0+46+0+47+0+48+0+49+0+50+0+51+0+52+0+53+0+54+0+55+0+56+0+57+0+58+0+59+0+60+0+61+0+62+0+63+0+64+0+65+0+66+0+67+0+68+0+69+0+70+0+-1+set_ir_high_master+0+10000+1+10000+2+10000+3+10000+4+10000+5+10000+6+10000+7+10000+8+10000+9+10000+10+10000+-1+set_ir_low_master+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_vis_high_master+0+10000+1+10000+2+10000+3+10000+4+10000+5+10000+6+10000+7+10000+8+10000+9+10000+10+10000+-1+set_vis_low_master+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_heatcal1+0+1+1+1+2+1+3+1+4+1+5+1+6+1+7+1+8+1+9+1+10+1+-1+set_heatcal2+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_heatcal3+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_ir_blank+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_vis_blank+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+print_memory+'
  // reset all eeprom values (no normalized values)
  const resetAllSoil = 'set_colorcal_blanks+0+0+0+0+1+0+0+0+2+0+0+0+3+0+0+0+4+0+0+0+5+0+0+0+6+0+0+0+7+0+0+0+8+0+0+0+9+0+0+0+10+0+0+0+-1+set_ir_high+0+655350000+1+655350000+2+655350000+3+655350000+4+655350000+5+655350000+6+655350000+7+655350000+8+655350000+9+655350000+10+655350000+-1+set_ir_low+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_vis_high+0+655350000+1+655350000+2+655350000+3+655350000+4+655350000+5+655350000+6+655350000+7+655350000+8+655350000+9+655350000+10+655350000+-1+set_vis_low+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_user_defined+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+11+0+12+0+13+0+14+0+15+0+16+0+17+0+18+0+19+0+20+0+21+0+22+0+23+0+24+0+25+0+26+0+27+0+28+0+29+0+30+0+31+0+32+0+33+0+34+0+35+0+36+0+37+0+38+0+39+0+40+0+41+0+42+0+43+0+44+0+45+0+46+0+47+0+48+0+49+0+50+0+51+0+52+0+53+0+54+0+55+0+56+0+57+0+58+0+59+0+60+0+61+0+62+0+63+0+64+0+65+0+66+0+67+0+68+0+69+0+70+0+-1+set_ir_high_master+0+10000+1+10000+2+10000+3+10000+4+10000+5+10000+6+10000+7+10000+8+10000+9+10000+10+10000+-1+set_ir_low_master+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_vis_high_master+0+10000+1+10000+2+10000+3+10000+4+10000+5+10000+6+10000+7+10000+8+10000+9+10000+10+10000+-1+set_vis_low_master+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_heatcal1+0+1+1+1+2+1+3+1+4+1+5+1+6+1+7+1+8+1+9+1+10+1+-1+set_heatcal2+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_heatcal3+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_ir_blank+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_vis_blank+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+print_memory+'
  // reset just colorcal
  const resetColorCal = 'set_colorcal_blanks+0+0+0+0+1+0+0+0+2+0+0+0+3+0+0+0+4+0+0+0+5+0+0+0+6+0+0+0+7+0+0+0+8+0+0+0+9+0+0+0+10+0+0+0+-1+'
  // reset just the non-relevant LEDs
  const resetNormalized = 'set_normalized_soil+0+1+1+1+2+1+3+1+4+1+5+1+6+1+7+1+8+1+9+1+10+1+-1+set_normalized_5mlcuvette+0+1+1+1+2+1+3+1+4+1+5+1+6+1+7+1+8+1+9+1+10+1+-1+set_normalized_1mlcuvette+0+1+1+1+2+1+3+1+4+1+5+1+6+1+7+1+8+1+9+1+10+1+-1+set_normalized_custom+0+1+1+1+2+1+3+1+4+1+5+1+6+1+7+1+8+1+9+1+10+1+-1+'
  // reset just the non-relevant LEDs
  const resetOtherLeds = 'set_vis_high+0+655350000+1+655350000+2+655350000+10+655350000+-1+set_vis_low+0+0+1+0+2+0+10+0+-1+set_ir_high+0+655350000+3+655350000+4+655350000+5+655350000+6+655350000+7+655350000+8+655350000+9+655350000+-1+set_ir_low+0+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+-1+set_vis_high_master+0+10000+1+10000+2+10000+10+10000+-1+set_vis_low_master+0+0+1+0+2+0+10+0+-1+set_ir_high_master+0+10000+3+10000+4+10000+5+10000+6+10000+7+10000+8+10000+9+10000+-1+set_ir_low_master+0+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+-1+set_vis_blank+0+0+1+0+2+0+10+0+-1+set_ir_blank+0+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+-1+print_memory+'
  // Sets to 115200 baud (originally 9600), device name to device ID as hex.  Example use configure_bluetooth+152+ .  Saves name as hex of 152.
  const configure_bluetooth = 'configure_bluetooth+NAME+9600+'
  // Sets to 115200 baud (originally 115200), device name to device ID as hex.  Used only on devices which have already been set correctly once.
  const configure_bluetooth_115200 = 'configure_bluetooth+NAME+115200+'
  // Sets device ID
  const set_device_info = 'set_device_info+'
  // This gets populated by something referenced in the survey, or something the user set in params
  const print_memory = 'print_memory+'

  /// //////////////////////FOR SOIL RESPIRATION SENSOR/////////////////////////////////////////////
  // see https://gitlab.com/our-sci/hardware-development/soil-respiration/-/wikis/Firmware-Commands

  // Measure raw weight value, directly from Qwic scale
  // Used for calibrating scale
  const weight_raw = [{
    environmental_array: [
      ['weight_raw']
    ],
    pulses: [1],
    pulse_distance: [15000],
    pulsed_lights: [
      [1]
    ]
  }]

  // Measure weight value with saved calibration applied
  // Standard weight measurement in (g) assuming calibration in (g)
  const weight = [{
    environmental_array: [
      ['weight']
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }]

  // Example command to save scale calibration values to EEPROM (device long-term memory)
  // EXAMPLE ONLY.  You need to save the actual measured values when recalibrating the scale
  // saves to userdef[0] / [1] / [2] for calibration of the scale
  // userdef[0] is the weight_raw of nothing on the scale
  // userdef[1] is the weight_raw of the standard
  // userdef[2] is the actual weight of the standard (eg 30g)
  // if these are stored, then running the weight command outputs the correct calibrated weight
  const save_weights_example = [{
    save: [
      [0, 1072853.000000],
      [1, 1408491.000000],
      [2, 30]
    ]
  }]

  // Measure weight while pumping water
  // Stop pumping when target or timeout is achieved and deliver final weight
  const pump_until = [{
    environmental_array: [
      ['weight']
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['digital_write', 5, 1],
      ['analog_write', 20, 3600, 12, 11718, -1, -1]
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['weight_until', 2.3, 15000] // this gets updated w/ the actual weight at run time
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['digital_write', 5, 0],
      ['analog_write', 20, 0, 12, 11718, -1, -1]
    ],
    pulses: [1],
    pulse_distance: [10000],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['weight']
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }]

  // Run the pumps for a fixed amount of time
  // Weigh before and after pumping
  const pump_timed = [{
    environmental_array: [
      ['weight']
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['digital_write', 5, 1],
      ['analog_write', 20, 3600, 12, 11718, -1, -1]
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }, {
    pulses: [10],
    pulse_distance: [800000],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['digital_write', 5, 0],
      ['analog_write', 20, 0, 12, 11718, -1, -1]
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['weight']
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }]

  const fan_on = [{
    environmental_array: [
      ['digital_write', 5, 1],
      ['analog_write', 22, 4096, 12, 10000, -1, -1]
    ],
    pulses: [5],
    pulse_distance: [1000000],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['digital_write', 5, 1],
      ['analog_write', 22, 0, 12, 10000, -1, -1]
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }]

  // start in top position, move down quickly to syringe limit
  // begin co2 measurement and move down slowly
  // on completion of co2, turn on exhaust fan + quickly move up until hit top limit switch
  // run exhaust fan for fixed time
  // using the 300ml syringe, at a speed of 750 it goes about 56ml in 9 co2 measurements
  // 1) set direction (digital write, pin 3 --> 0) and move (analog_write pin 4) fast (13000), wait for pin 15 to read low (0) to stop
  // 2) on hit pin 15 from previous, slow movement to 750 (set to 750 on analog_write pin 4).  Do this until I give an explicit command to stop (that's what -1 -1 means)
  // 3) measure co2 9 times, 6 seconds apart
  // 4) switch directions (digital write, pin 3 --> 1), go fast (1300)
  // 5) wait for 3 seconds while its raising up (so we don't start the exhaust fan till its a ways away and push air back into the syringe)
  // 6) start the exhaust fan (digital write enable pin 5, turn on analog_read pin 23)
  // 7) go faster (13000) until pin 16 reads low (0) to stop
  // 8) now you are at the top, so stop (analog_write pin 4, set 13000 --> 0)
  // 9) mesure co2 5 times, 6 seconds apart
  // 10) turn off exhaust fan (turn off analog-read pin 22)

  const down_measure_up_fan = [{
    environmental_array: [
      ['digital_write', 3, 0],
      ['digital_write', 8, 0], // enable motor driver
      ['analog_write', 4, 30000, 16, 13000, 15, 1] // go down fast, stop when limit switch (pin 15) is hit
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['digital_write', 3, 0],
      ['analog_write', 4, 30000, 16, 750, -1, -1] // then keep going, but slowly
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['co2']
    ],
    pulses: [9],
    pulse_distance: [6000000], // measure co2 10 times, 6 seconds apart
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['digital_write', 3, 1],
      ['analog_write', 4, 30000, 16, 13000, -1, -1] // go back up quickly
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }, {
    //   pulses: [3],
    //   pulse_distance: [1000000], // wait 3 seconds
    //   pulsed_lights: [
    //     [1]
    //   ]
    // }, {
    environmental_array: [
      ['digital_write', 5, 1],
      ['analog_write', 22, 4096, 12, 13000, -1, -1] // then turn on fan
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['digital_write', 3, 1],
      ['analog_write', 4, 30000, 16, 13000, 16, 1] // and switch to going till you hit the limit switch (pin 16)
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['analog_write', 4, 30000, 16, 0, -1, -1], // now stop moving
      ['digital_write', 8, 1] // disable motor driver
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['co2']
    ],
    pulses: [5],
    pulse_distance: [6000000], // measure co2 5 times
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['digital_write', 5, 0],
      ['analog_write', 22, 0, 12, 10000, -1, -1] // now turn off fan
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }]

  // Same as down_measure_up_fan, but less measurements
  // because the initial starting measurement is very close to ambient, so less measurements are needed
  // so run 5 for CO2, then 4 to purge
  const down_measure_up_fan_initial = JSON.parse(JSON.stringify(down_measure_up_fan))
  down_measure_up_fan_initial[2].pulses[0] = 6
  down_measure_up_fan_initial[8].pulses[0] = 4

  // Take 10 background samples.

  const background_measure = [{

    environmental_array: [
      ['co2']
    ],
    pulses: [10],
    pulse_distance: [6000000],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['digital_write', 5, 1],
      ['analog_write', 22, 4096, 12, 10000, -1, -1]
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['co2']
    ],
    pulses: [10],
    pulse_distance: [6000000],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['digital_write', 5, 0],
      ['analog_write', 22, 0, 12, 10000, -1, -1]
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }]

  // turn on fan, measure 10 co2, turn off fan
  const fan_measure_stop = [{
    environmental_array: [
      ['digital_write', 5, 1],
      ['analog_write', 22, 3600, 12, 10000, -1, -1]
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['co2']
    ],
    pulses: [10],
    pulse_distance: [6000000],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['digital_write', 5, 0],
      ['analog_write', 22, 0, 12, 10000, -1, -1]
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }]

  // CO2 10 measurements with pauses
  const measure_pause_measure_pause = [{
    environmental_array: [
      ['digital_write', 3, 0],
      ['analog_write', 4, 30000, 16, 8000, 15, 0]
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['digital_write', 3, 0],
      ['analog_write', 4, 30000, 16, 500, -1, -1]
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['co2']
    ],
    pulses: [10],
    pulse_distance: [12000000],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['digital_write', 3, 1],
      ['analog_write', 4, 30000, 16, 8000, -1, -1]
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['digital_write', 5, 1],
      ['analog_write', 22, 4096, 12, 10000, -1, -1]
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['digital_write', 3, 1],
      ['analog_write', 4, 30000, 16, 8000, 16, 0]
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['analog_write', 4, 30000, 16, 0, -1, -1]
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['co2']
    ],
    pulses: [10],
    pulse_distance: [12000000],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['digital_write', 5, 0],
      ['analog_write', 22, 0, 12, 10000, -1, -1]
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }]

  // start in top position, move down quickly to syringe limit
  // begin co2 measurement and move down slowly
  // on completion of co2, quickly move up until hit top limit switch
  // using the 250ml syringe, it goes 30ml in 30s and 5 measurements, and 60ml in 60s and 10 measurements
  const down_measure_up = [{
    environmental_array: [
      ['digital_write', 3, 0],
      ['digital_write', 8, 0], // enable motor driver
      ['analog_write', 4, 30000, 16, 13000, 15, 1] // go down fast, stop when limit switch (pin 15) is hit
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['digital_write', 3, 0],
      ['analog_write', 4, 30000, 16, 750, -1, -1] // then keep going, but slowly
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [ // measure co2 10 times, 6 seconds apart
      ['co2']
    ],
    pulses: [10],
    pulse_distance: [6000000],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['digital_write', 3, 1],
      ['analog_write', 4, 30000, 16, 13000, 16, 1] // go back up, until other limit switch (pin 16) is hit
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['analog_write', 4, 30000, 16, 0, -1, -1], // then stop
      ['digital_write', 8, 1] // disable motor driver
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }]

  // move up quickly until you hit contact with top limit switch
  const up = [{
    environmental_array: [
      ['digital_write', 3, 1],
      ['digital_write', 8, 0], // enable motor driver
      ['analog_write', 4, 30000, 16, 13000, 15, 1]
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['analog_write', 4, 30000, 16, 0, -1, -1],
      ['digital_write', 8, 1] // disable motor driver
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }]

  // move down quickly until you hit contact with syringe limit switch
  const down = [{
    environmental_array: [
      ['digital_write', 3, 0],
      ['digital_write', 8, 0], // enable motor driver
      ['analog_write', 4, 30000, 16, 13000, 15, 1]
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }, {
    environmental_array: [
      ['analog_write', 4, 30000, 16, 0, -1, -1],
      ['digital_write', 8, 1] // disable motor driver
    ],
    pulses: [1],
    pulse_distance: [1500],
    pulsed_lights: [
      [1]
    ]
  }]

  // used for old sensor - meaure 12 co2 measurements, 5 seconds apart.
  const co2_12 = [{
    environmental_array: [
      ['co2']
    ],
    pulses: [12],
    pulse_distance: [6000000],
    pulsed_lights: [
      [1]
    ]
  }]

  // used for old sensor - meaure 12 co2 measurements, 5 seconds apart.
  const co2_2 = [{
    environmental_array: [
      ['co2']
    ],
    pulses: [2],
    pulse_distance: [6000000],
    pulsed_lights: [
      [1]
    ]
  }]

  return {
    calibrateTeflon,
    calibrateTeflonCheck,
    calibrateTeflonNoHeat,
    calibrateOpen,
    calibrateBlank,
    standardStress,
    standardWarmup,
    // calibrateTeflonMaster,
    // calibrateOpenMaster,
    testIntensity,
    // testIntensityIr,
    standard,
    standardSoil,
    standard5mlcuvette,
    standard1mlcuvette,
    standardCustom,
    standardSample,
    standardBlank,
    standardRaw,
    standardRawNoHeat,
    standardRawHeatCal,
    standardRawHeatCalShort,
    standardHeatTest,
    standardApplyCalibration,
    calibrateBlankLocal,
    standardCheck,
    environmental_only,
    resetAll,
    resetAllSoil,
    resetColorCal,
    resetNormalized,
    resetRange,
    resetRangeManufacturer,
    resetOtherLeds,
    configure_bluetooth,
    configure_bluetooth_115200,
    set_device_info,
    print_memory,
    weight_raw,
    weight,
    save_weights_example,
    pump_until,
    pump_timed,
    down_measure_up,
    down_measure_up_fan,
    down_measure_up_fan_initial,
    background_measure,
    measure_pause_measure_pause,
    fan_measure_stop,
    fan_on,
    up,
    down,
    co2_12,
    co2_2
  }
}
