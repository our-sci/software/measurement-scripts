/*

"id": "calibrateTeflon",
"description": "Calibrate a device to the device's teflon standard."

"id": "calibrateOpen",
"description": "Calibrate a device to an open blank by point open window to empty space."

"id": "calibrateBlank",
"description": "Create and save a blank value, to be applied as an offset when using the standard_blank measurement on samples."

// this needs to get updated... we don't have this variable any more
"id": "calibrateBlankLocal",
"description": "Standard reflectomter blank offset which saves result to the local Environment variable. standard measurement uses if found, then resets."

// this also works if params is empty
"id": "standard",
"description": "Standard reflectometer measurement for field and lab use"

"id": "standardBlank",
"description": "Standard reflectomter measurement with blank offset, used on samples in cuvettes or similar situations in which blanks are useful.  Use the calibrateAllBlank measurement on the blank itself to save the blank values to the device"

"id": "standardRaw",
"description": "outputs raw data only, use standard reflectometer measurement for normal use"

"id": "standardRawNoHeat",
"description": "outputs raw data only, use standard reflectometer measurement for normal use.  Heat calibration not applied."

"id": "standardRawHeatCal",
"description": "outputs raw data only without heat (voltage) adjustement, repeats 12 times over 24 minutes.  Use standard reflectometer measurement for normal use.  Heat calibration not applied."

"id": "standardHeatTest",
"description": "Used to test the heat calibration StandardRawHeatCal."

"id": "testIntensity",
"description": "Test a set of intensities near the normal standard intensity set, for device manufacture only"

"id": "testIntensityIr",
"description": "Test a set of intensities near the normal standard intensity set for IR lights only, for device manufacture only"

"id": "environmental_only",
"description": "Measure temperature, pressure, humidity, and VOCs only"

"id": "resetAll",
"description": "Sets all values back to pre-calibration factory settings (0 or 100)"

"id": "configure_bluetooth",
"description": "Sets to 115200 baud (originally 9600), device name to device ID as hex"

"id": "configure_bluetooth_115200",
"description": "Sets to 115200 baud (originally 115200), device name to device ID as hex.  Used only on devices which have already been set correctly once."

"id": "set_device_info",
"description": "Sets device ID"

*/
