/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */
/* eslint-disable linebreak-style */
/* global processor */

import { sprintf } from 'sprintf-js';
import _ from 'lodash';

import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  const striga_visual = result['weeding_group/striga_visual'];

  if (striga_visual === 'yes') {
    ui.info(
      'Striga',
      'It is absolutely essential to apply manure and fertilizer at planting (early), to suppress striga and help the maize outgrow the parasite. Also, make sure to uproot and destroy any striga plants before flowering, this will lower the striga seed bank in the soil. If possible, the farmer should rotate to a legume crop to further suppress striga.',
    );
  } else if (striga_visual === 'no') {
    ui.info(
      'Striga',
      'No action required, but monitor your fields closely and uproot any striga that appears quickly to prevent a future problem.',
    );
  }

  app.save();
})();
