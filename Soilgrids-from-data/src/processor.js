/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

import { sprintf } from 'sprintf-js';
import _ from 'lodash';

import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';
// import { checkServerIdentity } from 'tls';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  if (result.error) {
    ui.error('Something was not right', result.error);
    return;
  }

  ui.info('Percent Clay', result.soilGrid.properties.CLYPPT.M.sl1);
  app.csvExport('Percent Clay', result.soilGrid.properties.CLYPPT.M.sl1);

  ui.info('Percent Sand', result.soilGrid.properties.SNDPPT.M.sl1);
  app.csvExport('Percent Sand', result.soilGrid.properties.SNDPPT.M.sl1);

  ui.info('Percent Silt', result.soilGrid.properties.SLTPPT.M.sl1);
  app.csvExport('Percent Silt', result.soilGrid.properties.SLTPPT.M.sl1);

  const labels = ['Percent Clay', 'Percent Sand', 'Percent Silt'];
  const series = [
    result.soilGrid.properties.CLYPPT.M.sl1,
    result.soilGrid.properties.SNDPPT.M.sl1,
    result.soilGrid.properties.SLTPPT.M.sl1,
  ];
  ui.barchart(
    {
      series: [series],
    },
    'Particle Size Distribution',
    {
      axisX: {
        labelInterpolationFnc(value, index) {
          return `${labels[index]}`;
        },
      },
    },
  );

  ui.info('pH x 10', result.soilGrid.properties.PHIHOX.M.sl1);
  app.csvExport('pH x 10', result.soilGrid.properties.PHIHOX.M.sl1);

  ui.info('CEC cmol/kg', result.soilGrid.properties.CECSOL.M.sl1);
  app.csvExport('CEC cmol/kg', result.soilGrid.properties.CECSOL.M.sl1);

  ui.info('Bulk Density kg m-3', result.soilGrid.properties.BLDFIE.M.sl1);
  app.csvExport('Bulk Density kg m-3', result.soilGrid.properties.BLDFIE.M.sl1);

  ui.info('Coarse fragments %', result.soilGrid.properties.CRFVOL.M.sl1);
  app.csvExport('Coarse fragments %', result.soilGrid.properties.CRFVOL.M.sl1);

  ui.info('Soil Type USDA', result.soilGrid.properties.TAXGOUSDAMajor);
  app.csvExport('Soil Type USDA', result.soilGrid.properties.TAXGOUSDAMajor);

  ui.info('Soil Type WRB', result.soilGrid.properties.TAXNWRBMajor);
  app.csvExport('Soil Type WRB', result.soilGrid.properties.TAXNWRBMajor);

  ui.info('Available soil water until wilting point VWC%', result.soilGrid.properties.WWP.M.sl1);
  app.csvExport(
    'Available soil water until wilting point VWC%',
    result.soilGrid.properties.WWP.M.sl1,
  );

  app.save();
})();
