/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

import { sprintf } from 'sprintf-js';
import _ from 'lodash';

import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';
// import { checkServerIdentity } from 'tls';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  if (result.error) {
    ui.error('Something was not right', result.error);
    return;
  }

  if (result.Depth === 5) {
    ui.info('Percent Clay', result.soilGrid.properties.CLYPPT.M.sl2);
    app.csvExport('Percent Clay', result.soilGrid.properties.CLYPPT.M.sl2);

    ui.info('Percent Sand', result.soilGrid.properties.SNDPPT.M.sl2);
    app.csvExport('Percent Sand', result.soilGrid.properties.SNDPPT.M.sl2);

    ui.info('Percent Silt', result.soilGrid.properties.SLTPPT.M.sl2);
    app.csvExport('Percent Silt', result.soilGrid.properties.SLTPPT.M.sl2);

    ui.info('pH x 10', result.soilGrid.properties.PHIHOX.M.sl2);
    app.csvExport('pH x 10', result.soilGrid.properties.PHIHOX.M.sl2);

    ui.info('CEC cmol/kg', result.soilGrid.properties.CECSOL.M.sl2);
    app.csvExport('CEC cmol/kg', result.soilGrid.properties.CECSOL.M.sl2);

    ui.info('Bulk Density kg m-3', result.soilGrid.properties.BLDFIE.M.sl2);
    app.csvExport('Bulk Density kg m-3', result.soilGrid.properties.BLDFIE.M.sl2);
  }
  if (result.Depth === 15) {
    ui.info('Percent Clay', result.soilGrid.properties.CLYPPT.M.sl3);
    app.csvExport('Percent Clay', result.soilGrid.properties.CLYPPT.M.sl3);

    ui.info('Percent Sand', result.soilGrid.properties.SNDPPT.M.sl3);
    app.csvExport('Percent Sand', result.soilGrid.properties.SNDPPT.M.sl3);

    ui.info('Percent Silt', result.soilGrid.properties.SLTPPT.M.sl3);
    app.csvExport('Percent Silt', result.soilGrid.properties.SLTPPT.M.sl3);

    ui.info('pH x 10', result.soilGrid.properties.PHIHOX.M.sl3);
    app.csvExport('pH x 10', result.soilGrid.properties.PHIHOX.M.sl3);

    ui.info('CEC cmol/kg', result.soilGrid.properties.CECSOL.M.sl3);
    app.csvExport('CEC cmol/kg', result.soilGrid.properties.CECSOL.M.sl3);

    ui.info('Bulk Density kg m-3', result.soilGrid.properties.BLDFIE.M.sl3);
    app.csvExport('Bulk Density kg m-3', result.soilGrid.properties.BLDFIE.M.sl3);
  }
  if (result.Depth === 30) {
    ui.info('Percent Clay', result.soilGrid.properties.CLYPPT.M.sl4);
    app.csvExport('Percent Clay', result.soilGrid.properties.CLYPPT.M.sl4);

    ui.info('Percent Sand', result.soilGrid.properties.SNDPPT.M.sl4);
    app.csvExport('Percent Sand', result.soilGrid.properties.SNDPPT.M.sl4);

    ui.info('Percent Silt', result.soilGrid.properties.SLTPPT.M.sl4);
    app.csvExport('Percent Silt', result.soilGrid.properties.SLTPPT.M.sl4);

    ui.info('pH x 10', result.soilGrid.properties.PHIHOX.M.sl4);
    app.csvExport('pH x 10', result.soilGrid.properties.PHIHOX.M.sl4);

    ui.info('CEC cmol/kg', result.soilGrid.properties.CECSOL.M.sl4);
    app.csvExport('CEC cmol/kg', result.soilGrid.properties.CECSOL.M.sl4);

    ui.info('Bulk Density kg m-3', result.soilGrid.properties.BLDFIE.M.sl4);
    app.csvExport('Bulk Density kg m-3', result.soilGrid.properties.BLDFIE.M.sl4);
  }
  if (result.Depth === 60) {
    ui.info('Percent Clay', result.soilGrid.properties.CLYPPT.M.sl5);
    app.csvExport('Percent Clay', result.soilGrid.properties.CLYPPT.M.sl5);

    ui.info('Percent Sand', result.soilGrid.properties.SNDPPT.M.sl5);
    app.csvExport('Percent Sand', result.soilGrid.properties.SNDPPT.M.sl5);

    ui.info('Percent Silt', result.soilGrid.properties.SLTPPT.M.sl5);
    app.csvExport('Percent Silt', result.soilGrid.properties.SLTPPT.M.sl5);

    ui.info('pH x 10', result.soilGrid.properties.PHIHOX.M.sl5);
    app.csvExport('pH x 10', result.soilGrid.properties.PHIHOX.M.sl5);

    ui.info('CEC cmol/kg', result.soilGrid.properties.CECSOL.M.sl5);
    app.csvExport('CEC cmol/kg', result.soilGrid.properties.CECSOL.M.sl5);

    ui.info('Bulk Density kg m-3', result.soilGrid.properties.BLDFIE.M.sl5);
    app.csvExport('Bulk Density kg m-3', result.soilGrid.properties.BLDFIE.M.sl5);
  }
  if (result.Depth === 80) {
    ui.info('Percent Clay', result.soilGrid.properties.CLYPPT.M.sl6);
    app.csvExport('Percent Clay', result.soilGrid.properties.CLYPPT.M.sl6);

    ui.info('Percent Sand', result.soilGrid.properties.SNDPPT.M.sl6);
    app.csvExport('Percent Sand', result.soilGrid.properties.SNDPPT.M.sl6);

    ui.info('Percent Silt', result.soilGrid.properties.SLTPPT.M.sl6);
    app.csvExport('Percent Silt', result.soilGrid.properties.SLTPPT.M.sl6);

    ui.info('pH x 10', result.soilGrid.properties.PHIHOX.M.sl6);
    app.csvExport('pH x 10', result.soilGrid.properties.PHIHOX.M.sl6);

    ui.info('CEC cmol/kg', result.soilGrid.properties.CECSOL.M.sl6);
    app.csvExport('CEC cmol/kg', result.soilGrid.properties.CECSOL.M.sl6);

    ui.info('Bulk Density kg m-3', result.soilGrid.properties.BLDFIE.M.sl6);
    app.csvExport('Bulk Density kg m-3', result.soilGrid.properties.BLDFIE.M.sl6);
  }
  ui.info('Soil Type USDA', result.soilGrid.properties.TAXGOUSDAMajor);
  app.csvExport('Soil Type USDA', result.soilGrid.properties.TAXGOUSDAMajor);

  ui.info('Soil Type WRB', result.soilGrid.properties.TAXNWRBMajor);
  app.csvExport('Soil Type WRB', result.soilGrid.properties.TAXNWRBMajor);

  app.save();
})();
