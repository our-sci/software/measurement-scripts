/* eslint-disable linebreak-style */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

/*
import {
  sprintf
} from 'sprintf-js';
import _ from 'lodash';
*/


// app.email(to, subject, text, cc)

import {
  app,
} from '@oursci/scripts';
import * as ui from '@oursci/measurements-ui';
import '@oursci/measurements-ui/dist/styles.min.css';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  const subject = 'RFC Lab: samples received but missing survey(s)!';
  const body1 = `Hello - we received produce samples from you - thanks!  However, sample IDs ${result.sample_check.data.missing} are missing.  Usually this means that you filled out the survey, but have not yet submitted it yet.  If so, please submit them!`;
  const body2 = 'Need help submitting your surveys?  Rirst, make sure youre online (you can test by trying to open a website).  Then, in the RFC Collect app, go to main menu and click Surveys.  In the ACTIVE tab, you should find the surveys for the sample IDs.  Press and hold one to select it, and then select all surveys you need to submit.  Then hit the button that looks like a cloud with an arrow in it.  You should get some feedback that it was successful, and those surveys should now be in the SENT tab.';
  const body3 = 'If you have questions or concerns, you can always email us at lab@bionutrient.org.  Thanks!';

  if (result.sample_check !== 'undefined' && result.sample_check.data.missing !== '') {
    ui.info('Email initiated', `Your phones email client should automagically pop up and request that you email the data collector regarding the missing survey IDs.  
    <br><br>If it didn't, you may still send it manually - just copy and paste the following:
    <br><br>to: ${result.shipper_id}
    <br><br>subject:
    <br><br>${subject}
    <br><br>body:
    <br><br>${body1}
    <br><br>${body2}
    <br><br>${body3}`);
    app.email(result.shipper_id,
      'RFC Lab: samples received but missing survey(s)!',
      `Hello - we received produce samples from you - thanks!  However, sample IDs ${result.sample_check.data.missing} are missing.  Usually this means that you filled out the survey, but have not yet submitted it yet.  If so, please submit them!  Need help submitting your surveys?  Rirst, make sure you're online (you can test by trying to open a website).  Then, in the RFC Collect app, go to main menu and click 'Surveys'.  In the 'ACTIVE' tab, you should find the surveys for the sample IDs.  Press and hold one to select it, and then select all surveys you need to submit.  Then hit the button that looks like a cloud with an arrow in it.  You should get some feedback that it was successful, and those surveys should now be in the 'SENT' tab.  If you have questions or concerns, you can always email us at lab@bionutrient.org.  Thanks!`,
      '');
  } else {
    ui.error('Missing Sample Check', 'This script requires the sample_check script to be run in the same survey.  It has not been run, or this survey does not have the sample_check script in it!');
  }
  app.save();
})();