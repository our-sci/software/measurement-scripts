/* eslint-disable linebreak-style */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-unreachable */

/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

/** Greg's notes
 * added //DEBUG to quiet console log statements here and in libraries app, process-wavelengths, and serial.
 */

// import moment from 'moment';
// import _ from 'lodash';
// import mathjs from 'mathjs';
// import serial from './lib/serial';
// import app from './lib/app';
// import sendDevice from './lib/sendDevice';
// import * as MathMore from './lib/math';
// import * as utils from './lib/utils'; // use: await sleep(1000);
// import soilgrid from './lib/soilgrid';
// import weather from './lib/weather';
// import oursci from './lib/oursci';

import {
  app,
  math,
  oursci,
} from '@oursci/scripts'; // expose this to android for calling onDataAvailable


// export default serial; // expose this to android for calling onDataAvailable

const result = {};

function errorExit(error) {
  app.result({
    error,
  });
}

const requiredMeasurements = [
  'sample_check',
];
const requiredAnswersText = [
  'shipper_id',
];

(async () => {

  // pull in text and number answers for all our questions
  requiredAnswersText.forEach((a) => {
    let v;
    if (typeof app.getAnswer(a) !== 'undefined') {
      v = app.getAnswer(a);
      if (app.getAnswer(a) === '') {
        result.error[a] = 'missing';
      }
    } else {
      v = 'undefined';
      result.error[a] = 'undefined';
    }
    console.log(v);
    result[a] = v;
  });

  requiredMeasurements.forEach((a) => {
    let v = 'undefined';
    if (typeof app.getAnswer(a) !== 'undefined') {
      // does it exist?
      if (app.getAnswer(a) && app.getAnswer(a) !== '{}') {
        // is it just an empty JSON or other falsey ('', "", 0, etc.) thing?
        v = JSON.parse(app.getAnswer(a));
      }
    }
    console.log(v);
    result[a] = v;
  });

  console.log(result);
  app.result(result);
})();