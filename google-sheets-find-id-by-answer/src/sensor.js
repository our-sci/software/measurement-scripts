/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-unreachable */

import moment from 'moment';

import app from './lib/app';
import serial from './lib/serial';
import sheets from './lib/sheets';

export default serial; // expose this to android for calling onDataAvailable

const result = {};

(async () => {
  try {
    if (app.hasQuestion('sampleID')) {
      console.log('question sampleID exists!');
      console.log(`answer is ${app.getAnswer('sampleID')}`);
    }

    if (!app.hasQuestion('asfasfasfasf')) {
      console.log('question asfasfasfasf does not exist');
    }

    if (app.isAnswered('sampleID')) {
      console.log('sampleID is answered');
    } else {
      console.log('sampleID is not answered');
    }

    const gsheet = await sheets('1zAAVAzJDB9wcJNOV2RIUFaX4klgasLZxlDcRCp9jawg');
    console.log('success');
    result.sampleID = app.getAnswer('sampleID');

    result.detail = {};
    gsheet.forEach((item) => {
      if (item.sampleID === result.sampleID) {
        console.log(item);
        Object.keys(item).forEach((k) => {
          result.detail[k] = item[k];
        });
      }
    });
  } catch (e) {
    result.error = e;
    console.log(e);
  }

  app.result(result);
})();