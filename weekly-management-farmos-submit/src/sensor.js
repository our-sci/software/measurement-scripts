/* eslint-disable linebreak-style */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-unreachable */

/**
 * !!!IMPORTANT!!! for testing
 * Add crentials to mock/credentials.json (don't commit to repo)
 * format in mock/credentials.demo.json
 */
// https://test.farmos.net/admin/structure/taxonomy/farm_log_categories

import moment from 'moment';

import { app, formparser, serial } from '@oursci/scripts';
import farmos from '@oursci/farmos';
import { req, sel, survey as surveyInit } from '../.oursci-surveys/survey';
import errorCheck from './errorcheck';

export { req, sel };

export default serial; // expose this to android for calling onDataAvailable

// if in dev environment, take this survey result as example
const uuidSnippet = 'uuid:223fb37a-ae60-4a9a-9e36-084b42ddb397';
const form = 'build_Weekly-Management-Form-RFC-Farm-Partners_1556628486';

if (app.initSubmittedMock && uuidSnippet && form) {
  app.initSubmittedMock(form, uuidSnippet);
  console.log(`fetched survey result for uuid: ${uuidSnippet}`);
}

const result = {
  error: [],
  log: [],
};
result.log = [];
export function err(msg) {
  result.error.push(msg);
}

function appendResult(title, content) {
  result.log.push({
    title,
    content,
  });
}

function errorExit(error) {
  app.error();
  throw Error(error);
}

export const survey = surveyInit((id) => {
  err(`Missing answer for: ${id}`);
});

export const regex = {
  anyOptional: /^.*$/,
  any: /^.+$/,
  number: /^(\d+\.\d+)|\d+$/,
  numberOptional: /^(\d+\.\d+)|\d+|()$/,
  date: /^\d{4}-\d{2}-\d{2}$/,
  optionalDate: /^(\d{4}-\d{2}-\d{2})|()$/,
  month_year: /^\d{4}-\d{2}$/,
  field_id: /^.*\(server: (\d+), tid: (\d+)\)$/,
  planting_id: /^.*\(server: (\d+), id: (\d+)\)$/,
};

(async () => {
  try {
    try {
      errorCheck();
      if (result.error.length > 0) {
        app.result(result);
        app.error();
        return;
      }
    } catch (error) {
      errorExit(`error parsing, ${error.message}`);
    }

    const weeksAgo = app.getAnswer('which_week');

    const weeksMap = {
      this_week: 0,
      last_week: 1,
      '2_weeks_ago': 2,
      '3_weeks_ago': 3,
      '1_month': 4,
    };

    const weeks = weeksMap[weeksAgo];
    if (weeks === undefined) {
      errorExit(
        `unable to map amount of weeks: ${weeksAgo}, choices are (${Object.keys(weeksMap).join(
          ',',
        )})`,
      );
    }
    const meta = app.getMeta();
    const started = moment.unix(Number.parseFloat(meta.started) / 1000);
    if (weeks > 0) {
      started.subtract(weeks, 'week');
    }

    appendResult('Estimated Date', started.format('YYYY-MM-DD'));

    app.progress(10);
    const planting = app.getAnswer('planting');
    const [, serverId, plantingId] = regex.planting_id.exec(planting);
    console.log(`serverId: ${serverId}`);
    console.log(`plantingId: ${plantingId}`);

    const { url, username, password, farmosCookie, farmosToken } = app.getCredentials(serverId);

    // appendResult('Authenticating', `Authenticating on farmos with url ${url}`);
    app.progress(20);

    let farm;
    try {
      farm = await farmos(url, username, password, farmosToken, {
        configInstanceId: meta.instanceId,
        configPlantingId: plantingId,
        configClearEntrieswithInstanceId: true,
      });
    } catch (error) {
      throw Error(
        `unable to identify planting, check credentials / server information: ${error.message}`,
      );
    }

    app.progress(40);
    appendResult('Authentication', 'success');

    const managementPractices = app.getAnswer('management_practices');
    let management = [];
    if (managementPractices) {
      management = managementPractices.split(' ');
      if (management.includes('irrigation')) {
        const rate = app.getAnswer('irrigation/rate');
        const type = app.getAnswer('irrigation/type');
        const irrigation = await farm.submitIrrigation(started.format('YYYY-MM-DD'), rate, type);
        appendResult('Irrigation', `submitted irigation to farmos with id: ${irrigation.id}`);
      }
    }

    /*
    const inputs = app.getAnswer('amendments/amendments_type');
    let amendments;
    console.log(inputs);
    if (inputs) {
      amendments = inputs.split(' ');
      for (let i = 0; i < amendments.length; i++) {
        console.log(`submitting ${amendments[i]}`);
        const a = await farm.submitSimpleAmendment(amendments[i], null, started.unix());
        appendResult(
          `Amendment ${i + 1}`,
          `submitted amendment (${amendments[i]}) to farmos,  with id ${a.id}`,
        );
      }
    }
    */

    if (management.includes('amendment')) {
      for (let i = 1; i < 3; i++) {
        if (i > 1 && !sel(`amendments/amendment_${i}`)) {
          break;
        }

        const type = survey[`amendments/amendment_type_${i}`];
        const typeString = formparser.getItemLabels(`amendments/amendment_type_${i}`)[0];

        console.log(`type is: ${type}`);
        if (type === 'mulch') {
          let mulchType = survey[`amendments/mulch_type_${i}`];
          if (mulchType === 'other') {
            mulchType = survey[`amendments/mulch_other_${i}`];
          } else {
            mulchType = formparser.getItemLabels(`amendments/mulch_type_${i}`)[0];
          }
          const depth = survey[`amendments/mulch_depth_${i}`];
          const m = await farm.submitMulch(mulchType, started, depth, '', '', plantingId);
          appendResult('Mulch', `submitted mulch (${mulchType}) to farmos,  with id ${m.id}`);
          continue;
        }

        const a = {
          name: survey[`amendments/name_${i}`],
          method: survey[`amendments/method_${i}`],
          nutrients: survey[`amendments/nutrients_${i}`],
          n: survey[`amendments/n_${i}`] || '',
          p: survey[`amendments/p_${i}`] || '',
          k: survey[`amendments/k_${i}`] || '',
          trace: survey[`amendments/nutrients_trace_${1}`],
        };

        const amendment = await farm.submitAmendment(
          a.name,
          null,
          started.unix(),
          a.method,
          typeString,
          a.nutrients,
          a.n,
          a.p,
          a.k,
          a.trace,
        );
        appendResult(
          `Amendment ${i}`,
          `submitted amendment (${a.name}) to farmos,  with id ${amendment.id}`,
        );
      }
    }

    const weedControlType = app.getAnswer('weed_control/weed_control_type');
    if (weedControlType) {
      const activities = weedControlType.split(' ');
      let hasHerbicide = false;
      for (let i = 0; i < activities.length; i++) {
        const a = activities[i];
        if (a === 'herbicide') {
          hasHerbicide = true;
          continue;
        }

        const activity = await farm.submitSimpleActivity(a, null, started.unix());
        appendResult(
          `Weed Control ${i + 1}`,
          `submitted weed control (${a}) to farmos, with id ${activity.id}`,
        );
      }

      if (hasHerbicide) {
        const rate = app.getAnswer('weed_control/herbicide_rate');
        const type = app.getAnswer('weed_control/herbicide_type');
        const h = await farm.submitHerbicide(started.unix(), null, type, rate);
        appendResult(
          'Application of Herbicide',
          `submitted herbicide (${type}) to farmos, with id ${h.id}`,
        );
      }
    }

    const pressure = app.getAnswer('pest_disease_scouting/pest_disease_pressure');
    if (pressure) {
      const p = await farm.submitDiseasePressure(started.unix(), null, pressure);
    }

    const reason = app.getAnswer('pest_disease_control/pest_disease_reason');
    if (reason) {
      const typeAnswer = app.getAnswer('pest_disease_control/pest_disease_type');
      if (typeAnswer) {
        let types = [];
        try {
          types = formparser
            .getItemLabels('pest_disease_control/pest_disease_type')
            .filter(i => i.toLowerCase() !== 'hand-picking/pruning');
        } catch (error) {
          appendResult('Pest Control Types', 'unable to extract pest control types, ignoring.');
        }

        const rate = app.getAnswer('pest_disease_control/pest_disease_rate');
        const name = `Pest / Disease Control due to ${reason}`;

        const d = {
          methods: types,
          pest_disease_controlled_1: app.getAnswer(
            'pest_disease_control/pest_disease_controlled_1',
          ),
          pest_disease_controlled_2: app.getAnswer(
            'pest_disease_control/pest_disease_controlled_2',
          ),
          pest_disease_controlled_3: app.getAnswer(
            'pest_disease_control/pest_disease_controlled_3',
          ),
        };

        const pruning = typeAnswer.includes('hand_removal');

        const h = await farm.submitPestDiseaseControl(
          started.unix(),
          name,
          types,
          pruning,
          rate,
          null,
          d,
        );
        appendResult(
          'Pest / Disease Control',
          `submitted control (${name}) to farmos, with id ${h.id}`,
        );
      }
    }

    if (management && management.includes('harvest')) {
      const harvest = {
        weightUnit: app.getAnswer('harvest_group/harvest_weight_units'),
        weight: Number.parseFloat(app.getAnswer('harvest_group/harvest_weight')),
        areaUnit: app.getAnswer('harvest_group/harvest_area_units'),
        area: Number.parseFloat(app.getAnswer('harvest_group/harvest_area')),
        complete: sel('harvest_group/harvest_complete'),
      };

      let plantDensity;
      const densityAnswer = app.getAnswer('harvest_group/plant_density');
      const rowSpacingUnit = app.getAnswer('harvest_group/row_spacing_unit');

      if (densityAnswer) {
        plantDensity = {
          name: 'Plant Density',
          type: 'count',
          value: Number.parseFloat(densityAnswer),
          unit: `plants per ${harvest.areaUnit}`,
        };
      } else if (rowSpacingUnit) {
        const rowSpacing = app.getAnswer('harvest_group/row_spacing');
        plantDensity = {
          name: 'Row Spacing',
          type: 'length',
          value: Number.parseFloat(rowSpacing),
          unit: rowSpacingUnit,
        };
      }

      if (harvest.weightUnit && harvest.weight && harvest.area && harvest.areaUnit) {
        const density = harvest.weight / harvest.area;
        appendResult('Harvest Density', `${density} ${harvest.weightUnit} per ${harvest.areaUnit}`);
        const quantities = {
          area: {
            value: harvest.area,
            unit: harvest.areaUnit,
          },
          weight: {
            value: harvest.weight,
            unit: harvest.weightUnit,
          },
          density: {
            value: density,
            unit: `${harvest.weightUnit} per ${harvest.areaUnit}`,
          },
        };

        if (plantDensity) {
          Object.assign(quantities, { plantDensity });
        }

        console.log('plant density: ');
        console.log(quantities);

        const h = await farm.submitHarvest(started.unix(), quantities, null, harvest.complete);
        appendResult('Harvest', `submitted to farmos, with id ${h.id}`);
      } else {
        console.error(`missing harvest parameter${JSON.stringify(harvest)}`);
      }
    }

    const pruning = app.getAnswer('thin_prune_group/pruning_activity');
    if (pruning) {
      const prune = pruning
        .split(' ')
        .map((p) => {
          const pruningControl = app
            .getMeta()
            .controls.find(c => c.name === '/data/thin_prune_group/pruning_activity:label');
          const option = pruningControl.options.find(o => o.value === p);
          return option.text || '';
        })
        .join(', ');

      const p = await farm.submitPruning(started.unix(), prune, pruning.split(' '));
      appendResult('Pruning', `submitted to farmos, with id ${p.id}`);
    }
    result.url = url;
    app.result(result);
  } catch (error) {
    console.error(error);
    result.error = [error.message];
    app.result(result);
  }
})();
