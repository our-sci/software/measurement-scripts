/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

import _ from 'lodash';
//import moment from 'moment';
import app from './lib/app';
//import serial from './lib/serial';
//import sendDevice from './lib/sendDevice';
import * as MathMore from './lib/math';
import * as utils from './lib/utils'; // use: await sleep(1000);
import oursci from './lib/oursci';
//import mathjs from 'mathjs';

//export default serial; // expose this to android for calling onDataAvailable


// ///////////////////////////////////////////////////////
// OPTIONAL instead of reading from the serial port mock some data
// serial.feed('./mock/mock_sensor_output.json');
//  serial.write("print_memory+");

const questions = [
  'measurement1',
];

const measurements = [
  'measurement1',
  'measurement2',
  'measurement3',
  'measurement4',
  'measurement5',
  'measurement6',
];

const compareFields = [
  'date',
  'device_battery',
  'device_id',
  'median_365',
  'median_385',
  'median_450',
  'median_500',
  'median_530',
  'median_587',
  'median_632',
  'median_850',
  'median_880',
  'median_940',
];

const res = {};

(async () => {
  const errors = {};
  res.errors = {};

  let data;
  const averages = {};
  const variances = {};
  const current = {};

  /*
  res.answers = {};
  questions.forEach((q) => {
    res.answers[q] = app.getAnswer(q);
  });
    */

  res.measurements = {};
  res.data = {};
  measurements.forEach((q) => {
    res.measurements[q] = JSON.parse(app.getAnswer(q));
  });
  //  console.log(res.measurements);

  // the current device ID
  const device_id = res.measurements.measurement1.data.device_id;
  console.log(`This device is: ${device_id}.  Fetching server data on that ID.`);

  try {
    data = await oursci('User-Calibration-2');
    //    console.log(`I am here ${JSON.stringify(data)}`)
  } catch (error) {
    errors['oursci'] = `Unable to fetch oursci survey results: ${error}`;
    res.errors = errors;
    app.result(res);
  }

  //  console.log(data);

  // Trim the data to the median values + other relevant info
  const trimmedData = {};
  _.forEach(_.keysIn(data), (colName) => {
    _.forEach(compareFields, (item) => {
      if (colName.includes(item)) {
        trimmedData[colName] = data[colName];
      }
    });
  });
  console.log('All relevant columns from server');
  console.log(trimmedData);

  // Now get rid of other device ID's, just show current device ID, so filtered what we see from the web by the device we just measured
  // go backwards because we're using splice
  for (let i = trimmedData['measurement1.data.device_id'].length - 1; i >= 0; i--) {
    console.log(trimmedData['measurement1.data.device_id'][i]);
    if (trimmedData['measurement1.data.device_id'][i] !== device_id) {
      _.forEach(_.keysIn(trimmedData), (colName) => {
        trimmedData[colName].splice(i, 1);
      });
    }
  }
  console.log(`All relevant columns from server for device ${device_id}`);
  console.log(trimmedData);
  res.data = trimmedData;

  app.result(res);
})();

/*
{
  "id": "spad-cal-model-3",
  "name": "Model to calibrate green SPAD cards 2 - 11",
  "description": "emtpy now, tbd later",
  "version": "3.3.1",
  "action": "Run Measurement",
  "requireDevice": "false"
}
*/