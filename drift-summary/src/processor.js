/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/* global processor */

import {
  sprintf,
} from 'sprintf-js';
import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';
import flatten from 'flat';
import _ from 'lodash';

const Chartist = require('chartist');

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();
  if (result.error) {
    ui.error('Error', result.error);
    app.save();
    return;
  }

  const measurements = [
    'measurement1',
    'measurement2',
    'measurement3',
    'measurement4',
    'measurement5',
    'measurement6',
  ];

  const compareFields = [
    'date',
    'device_battery',
    'device_id',
    'median_365',
    'median_385',
    'median_450',
    'median_500',
    'median_530',
    'median_587',
    'median_632',
    'median_850',
    'median_880',
    'median_940',
  ];

  const graphFields = [
    'median_365',
    'median_385',
    'median_450',
    'median_500',
    'median_530',
    'median_587',
    'median_632',
    'median_850',
    'median_880',
    'median_940',
  ]

  const excludeFields = [
    'date',
    'device_id',
  ];

  ui.info('About', 'This shows bar graphs (x = time, y = reflectance) at the 10 wavelengths for each drift check run on this device.');
  ui.info('Device', `device ID: ${result.measurements.measurement1.data.device_id}<br>device version: ${result.measurements.measurement1.data.device_version}<br>device battery: ${result.measurements.measurement1.data.device_battery}<br>device firmware: ${result.measurements.measurement1.data.device_firmware}`);

  // first, combine the data from the web together with the data we just collected so it's in the same format
  const flatData = flatten(result.measurements);
  //  console.log('flatData');
  //  console.log(flatData);
  console.log(result.data);

  // now let's add what we just measured to what was on the web
  _.forEach(_.keysIn(result.data), (colName) => {
    result.data[colName].push(flatData[colName]);
  });

  // now we need to average the white repeats (1-3) and black repeats (4-6) together
  const graphData = {
    black: {},
    white: {},
  };
  _.forEach(compareFields, (colName) => {
    //    console.log(`this is the colName ${colName}`);
    let thisArray = result.data[`measurement1.data.median_450`]; // set as arbitrary array so that the date and device_id can have appropraite number of items in them
    if (colName !== 'device_id' && colName !== 'date') {
      thisArray = result.data[`measurement1.data.${colName}`];
    }
    graphData.white[`${colName}`] = [];
    graphData.black[`${colName}`] = [];
    for (let i = 0; i < thisArray.length; i++) {
      if (colName === 'device_id') {
        graphData.black[`${colName}`].push(result.data['measurement1.data.device_id'][i]);
        graphData.white[`${colName}`].push(result.data['measurement1.data.device_id'][i]);
      } else if (colName === 'date') {
        graphData.black[`${colName}`].push(result.data['measurement1.meta.date'][i]);
        graphData.white[`${colName}`].push(result.data['measurement1.meta.date'][i]);
      } else {
        const white1 = MathMore.MathROUND(Number(result.data[`measurement4.data.${colName}`][i]));
        const white2 = MathMore.MathROUND(Number(result.data[`measurement5.data.${colName}`][i]));
        const white3 = MathMore.MathROUND(Number(result.data[`measurement6.data.${colName}`][i]));
        const black1 = MathMore.MathROUND(Number(result.data[`measurement1.data.${colName}`][i]));
        const black2 = MathMore.MathROUND(Number(result.data[`measurement2.data.${colName}`][i]));
        const black3 = MathMore.MathROUND(Number(result.data[`measurement3.data.${colName}`][i]));
        console.log(`${white1}, ${black1}`);
        graphData.white[`${colName}`].push(MathMore.MathMEDIAN([black1, black2, black3]));
        graphData.black[`${colName}`].push(MathMore.MathMEDIAN([white1, white2, white3]));
      }
    }
  });

  // Now we can graph the results easily and keep track of the values as text to print at the end for csv copy paste (replace with a table!!!)
  console.log(graphData);

  // Graph and print black --> 
  // prep the text for printing later
  let logText = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
  _.forEach(graphData.black.date, (item) => {
    logText += `${item.slice(0, 10)}, `;
  });
  logText += '<br>';

  _.forEach(graphFields, (wavelength) => {
    logText += `${wavelength}, &nbsp;&nbsp;&nbsp;`;
    _.forEach(graphData.black[wavelength], (item) => {
      logText += `${item}, &nbsp;&nbsp;&nbsp;`;
    });
    logText += '<br>';

    // ok now chart it
    ui.barchart({
      series: [graphData.black[wavelength]],
    }, `${wavelength.slice(-3)}nm reflectance over time`, {
      //      min: Math.min(...graphData.black[wavelength]),
      //      max: Math.max(...graphData.black[wavelength]),
      seriesBarDistance: 7,
      axisX: {
        labelInterpolationFnc(value, index) {
          return `${graphData.black.date[index].slice(0, 10)}`;
        },
      },
    });
  });
  ui.info('Black drift over time', logText);


  // graph and print white --> 
  // prep the text for printing later
  logText = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
  _.forEach(graphData.white.date, (item) => {
    logText += `${item.slice(0, 10)}, `;
  });
  logText += '<br>';

  _.forEach(graphFields, (wavelength) => {
    logText += `${wavelength}, &nbsp;&nbsp;&nbsp;`;
    _.forEach(graphData.white[wavelength], (item) => {
      logText += `${item}, &nbsp;&nbsp;&nbsp;`;
    });
    logText += '<br>';

    // ok now chart it
    ui.barchart({
      series: [graphData.white[wavelength]],
    }, `${wavelength}nm by Time`, {
      //      min: Math.min(...graphData.white[wavelength]),
      //      max: Math.max(...graphData.white[wavelength]),
      seriesBarDistance: 7,
      axisX: {
        labelInterpolationFnc(value, index) {
          return `${graphData.white.date[index].slice(0, 10)}`;
        },
      },
    });
  });
  ui.info('White drift over time', logText);


})();
// make sure this is at the very end!
app.save();