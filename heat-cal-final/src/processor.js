/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/* global processor */

import {
  sprintf,
} from 'sprintf-js';
import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';

const Chartist = require('chartist');

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();
  if (result.error) {
    ui.error('Error', result.error);
    app.save();
    return;
  }

  // Loop through all the lights and all the calibration values (5 calibration values, 10 lights) and make sure there are no nans or non-numbers... inform the user if there are.
  let errors = 0;
  for (let j = 1; j < 11; j++) {
    if (isNaN(Number(result.heatcal1[j]))) errors += 1;
    if (isNaN(Number(result.heatcal2[j]))) errors += 1;
    //    if (isNaN(Number(result.heatcal3[j]))) errors += 1;
    //    if (isNaN(Number(result.heatcal4[j]))) errors += 1;
    //    if (isNaN(Number(result.heatcal5[j]))) errors += 1;
  }
  if (errors > 0) {
    ui.warning('Something is Wrong :\\', errors + ' saved values in heatcal1/2 are not numbers (NaN) but should be... something either went wrong with the calibration script or the device did not respond.  Make sure the device is still on and working, and recheck the heat calibration measurement values to ensure that all LEDs produced valid results and working model fits');
  } else {
    ui.warning('Success!', 'Heat Calibration information has been saved.  The calibration information from the device is now:');
    ui.info('Saved To Device', 'heatcal1: ' + JSON.stringify(result.heatcal1) + '<br>heatcal2: ' + JSON.stringify(result.heatcal2)); // + '<br>heatcal3: ' + JSON.stringify(result.heatcal3) + '<br>heatcal4: ' + JSON.stringify(result.heatcal4) + '<br>heatcal5: ' + JSON.stringify(result.heatcal5));
  }
  ui.info('All stored info on device', JSON.stringify(result));
})();