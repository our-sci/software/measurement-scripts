/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

/* global processor */

import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';
import { unique } from './lib/utils';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();
  const thisResult = result.data;
  // Now, we need to look at the mineral names and ensure there are no duplicates... this indicates this ID has been used twice
  let toDisplay = '';
  const theseMinerals = [];
  const thesePhotons = [];
  const theseConc = [];
  thisResult.forEach((item) => {
    const mineral = item[0].slice(0, 2).replace(' ', ''); // gt rid of any tailing spaces
    const photons = MathMore.MathROUND(Number(item[1].replace(',', '')), 3); // ugly, but need to get rid of any , so it recognizes it as a number.
    const conc = MathMore.MathROUND(Number(item[2].replace(',', '')), 2);
    toDisplay += `${mineral}: ${conc} ppm -- ${photons} photons<br>`;
    theseMinerals.push(mineral);
    thesePhotons.push(photons);
    theseConc.push(conc);
  });

  if (!theseMinerals[0]) {
    // if it can't find any, then throw an error
    ui.error(
      'Unable to find sample ID',
      'Check to ensure this samples has been run and saved to the google doc',
    );
  } else if (unique(theseMinerals)[0] !== undefined) {
    // if there are duplicates, let the user know and let them know which ones.
    ui.warning(
      'Duplicate values found',
      `Each sample ID should be unique, but duplicate values were found for this ID.  The duplicated values are<br>${unique(
        theseMinerals,
      )}`,
    );
  }
  //  else { // if you find exactly one set, then show the user and save them.
  ui.info('Results', toDisplay);
  ui.barchart(
    {
      series: [theseConc.slice(0, 10)],
    },
    'Lighter Minerals',
    {
      min: Math.min(...theseConc.slice(0, 10)),
      max: Math.max(...theseConc.slice(0, 10)),
      seriesBarDistance: 7,
      axisX: {
        labelInterpolationFnc(value, index) {
          return `${theseMinerals[index]}`;
        },
      },
    },
  );
  ui.barchart(
    {
      series: [theseConc.slice(10)],
    },
    'Heavier Minerals',
    {
      min: Math.min(...theseConc.slice(10)),
      max: Math.max(...theseConc.slice(10)),
      seriesBarDistance: 7,
      axisX: {
        labelInterpolationFnc(value, index) {
          return `${theseMinerals[index + 10]}`;
        },
      },
    },
  );
  app.csvExport('elements', theseMinerals.join());
  app.csvExport('elements_ppm', theseConc.join());
  app.csvExport('elements_photons', thesePhotons.join());
  /*
  theseMinerals.forEach((item, index) => {
    app.csvExport(item, theseConc[index]);
    app.csvExport(`${item}.photons`, thesePhotons[index]);
  });
  */
  app.save();
})();
