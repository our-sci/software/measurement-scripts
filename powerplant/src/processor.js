/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/* global processor */

import { sprintf } from 'sprintf-js';
import app from './lib/app';
import * as ui from './lib/ui';

const Chartist = require('chartist');

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  if (result.error) {
    ui.error('Error', result.error);
    app.save();
  }

  ui.info(result.type, 'Result type');

  if (result.type === 'status') {
    ui.info(result.from_device.moist_min, 'Potentiometer Moisture Minimum Callibration');
    ui.info(result.from_device.moist_max, 'Potentiometer Moisture Maxiumum Callibration');
    ui.info(result.from_device.time_min, 'Potentiometer Time Minimum Callibration');
    ui.info(result.from_device.time_max, 'Potentiometer Time Maxiumum Callibration');
    ui.info(result.from_device.moisture, 'Current Moisture Raw out, 4095 = dry, 0 = wet');
    ui.info(result.from_device.pot_moist_pct, 'Current Potentiometer Moisture Position in %');
    ui.info(
      (result.from_device.pot_time_ms / 1000).toFixed(0),
      'Current Potentiometer Time Position in [s]',
    );
    ui.info(result.from_device.pot_moist_pct, 'Current Potentiometer Moisture Position in %');
    ui.info((result.from_device.millis_pumped / 1000).toFixed(0), 'Seconds Pumped Today');
    ui.info(JSON.stringify(result.from_device, null, 2).replace('\n', '<br>'), 'device answer');
  }
  app.save();
})();
