/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

/** Greg's notes
 * added //DEBUG to quiet console log statements here and in libraries app, process-wavelengths, and serial.
 */

import app from './lib/app';
// import serial from './lib/serial';
// import wavelengthProcessor from './lib/process-wavelengths';
import sendDevice from './lib/sendDevice';
import * as MathMore from './lib/math';
// import {
//   sleep
// } from './lib/utils'; // use: await sleep(1000);

// export default serial; // expose this to android for calling onDataAvailable

// all data is passed in result, in the form of a JSON
// this separates answers (which can be anything except undefined) from measurements (which must be populated, valid JSONs)
// anything which is not valid will emerge in the result.json as a text object 'undefined'.

const result = {};

// answers which come from normal survey answers
const requiredAnswers = ['test'];

// answers which come from measurement scripts only
const requiredMeasurements = [
  'standard0',
  'standard1',
  'standard2',
  'standard3',
  'standard4',
  'standard5',
  'standard6',
  'standard7',
];

(async () => {
  requiredMeasurements.forEach((a) => {
    let v = 'undefined';
    if (typeof app.getAnswer(a) !== 'undefined') {
      // does it exist?
      if (app.getAnswer(a) && app.getAnswer(a) !== '{}') {
        // is it just an empty JSON or other falsey ('', "", 0, etc.) thing?
        v = JSON.parse(app.getAnswer(a));
      }
    }
    console.log(v);
    result[a] = v;
  });
})();

(async () => {
  requiredAnswers.forEach((a) => {
    let v = 'undefined';
    if (typeof app.getAnswer(a) !== 'undefined') {
      // does it exist?
      v = app.getAnswer(a);
    }
    console.log(v);
    result[a] = v;
  });
})();

app.result(result);
console.log(result);

/*
  "standard5": "{}",
  "standard4": "{}",
"standard4": "{\"meta\":{\"filename\":\"measurement_2018-08-09T13:56:17.854-04:00.json\",\"scriptID\":\"standard-2-4\",\"date\":\"2018-08-09T13:56:17.857-04:00\"},\"data\":{\"device_battery\":\"80\",\"device_id\":\"00:00:00:15\",\"device_version\":\"1B\",\"device_firmware\":\"1.29\",\"protocols\":\"2\",\"calibration\":\"0\",\"median_365\":\"-2.60679\",\"median_385\":\"-4.57239\",\"median_450\":\"-9\",\"median_500\":\"-10.4002\",\"median_530\":\"-12.7512\",\"median_587\":\"-28.0033\",\"median_632\":\"-29.5739\",\"median_850\":\"3.717\",\"median_880\":\"0.701097\",\"median_940\":\"1\",\"three_stdev_365\":\"0.0609445\",\"three_stdev_385\":\"0.0272774\",\"three_stdev_450\":\"0\",\"three_stdev_500\":\"0.0375749\",\"three_stdev_530\":\"0.0312256\",\"three_stdev_587\":\"0.0492519\",\"three_stdev_632\":\"0.0367405\",\"three_stdev_850\":\"0.134276\",\"three_stdev_880\":\"0.0969197\",\"three_stdev_940\":\"0\",\"spad_365\":\"111.47\",\"spad_385\":\"77.2992\",\"spad_450\":\"13.9762\",\"spad_500\":\"68.7568\",\"spad_530\":\"81.332\",\"spad_587\":\"140.02\",\"spad_632\":\"126.787\",\"spad_850\":\"50.1174\",\"spad_880\":\"84.4813\",\"spad_940\":\"0\",\"polyphenols_estimate\":\"0\",\"antioxidants_estimate\":\"0\",\"protein_estimate\":\"0\",\"temperature\":\"24.5\",\"humidity\":\"61.481\",\"pressure\":\"981.17\",\"voc\":\"97.109\"}}",
"standard5": "{\"meta\":{\"filename\":\"measurement_2018-08-09T13:57:12.380-04:00.json\",\"scriptID\":\"standard-2-4\",\"date\":\"2018-08-09T13:57:12.383-04:00\"},\"data\":{\"device_battery\":\"80\",\"device_id\":\"00:00:00:15\",\"device_version\":\"1B\",\"device_firmware\":\"1.29\",\"protocols\":\"2\",\"calibration\":\"0\",\"median_365\":\"-3.90545\",\"median_385\":\"-6.76932\",\"median_450\":\"-18\",\"median_500\":\"-12.5787\",\"median_530\":\"-12.8772\",\"median_587\":\"-23.5519\",\"median_632\":\"-26.2867\",\"median_850\":\"3.49335\",\"median_880\":\"0.524035\",\"median_940\":\"0\",\"three_stdev_365\":\"0.0434987\",\"three_stdev_385\":\"0.0241914\",\"three_stdev_450\":\"0\",\"three_stdev_500\":\"0.0426487\",\"three_stdev_530\":\"0.0375235\",\"three_stdev_587\":\"0.0357586\",\"three_stdev_632\":\"0.0273972\",\"three_stdev_850\":\"0.100137\",\"three_stdev_880\":\"0.126908\",\"three_stdev_940\":\"0\",\"spad_365\":\"110.414\",\"spad_385\":\"78.0077\",\"spad_450\":\"19.1055\",\"spad_500\":\"72.1082\",\"spad_530\":\"79.6068\",\"spad_587\":\"116.001\",\"spad_632\":\"109.379\",\"spad_850\":\"49.1942\",\"spad_880\":\"82.2438\",\"spad_940\":\"0\",\"polyphenols_estimate\":\"0\",\"antioxidants_estimate\":\"0\",\"protein_estimate\":\"0\",\"temperature\":\"24.57\",\"humidity\":\"61.309\",\"pressure\":\"981.19\",\"voc\":\"99.135\"}}",
*/