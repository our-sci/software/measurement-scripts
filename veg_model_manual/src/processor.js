/* eslint-disable linebreak-style */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

import { sprintf } from 'sprintf-js';
import _ from 'lodash';

import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  Object.keys(result.error).forEach((a) => {
    ui.error(`Answer '${a}' is ${result.error[a]}`, 'Return to the answer and fix it.');
  });

  // Define parameters to calculate polyphenol, antioxidant and protein;
  const car_norm = 0.8829; // Fresh weight for carrot, based on USDA nutritional database;
  const poly_conv = 0.1; // needed to convert ug ml-1 to mg 100 g FW for polyphenol;
  const pro_conv = 0.001; // needed to convert protein into mg protein per 100 g FW;

  const CF = car_norm / result.moisture_content;
  ui.info('Correction Factor', MathMore.MathROUND(CF, 2));
  app.csvExport('correctionFactor', MathMore.MathROUND(CF, 2));

  // Calculate final Polyphenol, Antioxidant and Protein Content;

  if (result.test_type === 'Antioxidants') {
    const antioxidants =
      ((result.cuvette_filled.data.antioxidants_estimate * result.extractant) /
        result.sample_weight) *
      CF;
    ui.info('Total Antioxidants FRAP value', MathMore.MathROUND(antioxidants, 2));
    app.csvExport('antioxidentsFrap', MathMore.MathROUND(antioxidants, 2));
  } else if (result.test_type === 'Polyphenols') {
    const polyphenols =
      ((result.cuvette_filled.data.polyphenols_estimate * result.extractant) /
        result.sample_weight) *
      poly_conv *
      CF;
    ui.info('Total Polyphenols mg GAE 100g FW', MathMore.MathROUND(polyphenols, 2));
    app.csvExport('polyphenolsMgGae100gFw', MathMore.MathROUND(polyphenols, 2));
  } else if (result.test_type === 'Protein') {
    const protein =
      ((result.cuvette_filled.data.protein_estimate * result.protein_dissolution) /
        result.sample_weight) *
      pro_conv *
      CF;
    ui.info('Total Protein mg per 100g FW', MathMore.MathROUND(protein, 2));
    app.csvExport('proteinMgPer100g', MathMore.MathROUND(protein, 2));
  }
  app.save();
})();
