/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

/** Greg's notes
 * added //DEBUG to quiet console log statements here and in libraries app, process-wavelengths, and serial.
 */

import app from './lib/app';
import serial from './lib/serial';
import wavelengthProcessor from './lib/process-wavelengths';
import sendDevice from './lib/sendDevice';
import * as MathMore from './lib/math';
import {
  sleep,
} from './lib/utils'; // use: await sleep(1000);

export default serial; // expose this to android for calling onDataAvailable

(async () => {
  await serial.init('/dev/ttyACM0', {
    //  await serial.init('/dev/rfcomm0', {
    baudRate: 115200,
    dataBits: 8,
    stopBits: 1,
    parity: 'none',
  });

  // ///////////////////////////////////////////////////////
  // OPTIONAL instead of reading from the serial port mock some data
  // serial.feed('./mock/mock_sensor_output.json');
  //  serial.write("print_memory+");

  const result = {};
  result.measurement_1 = JSON.parse(app.getAnswer('measurement_1'));
  result.measurement_2 = JSON.parse(app.getAnswer('measurement_2'));
  result.measurement_3 = JSON.parse(app.getAnswer('measurement_3'));
  result.measurement_4 = JSON.parse(app.getAnswer('measurement_4'));
  result.measurement_5 = JSON.parse(app.getAnswer('measurement_5'));
  result.measurement_6 = JSON.parse(app.getAnswer('measurement_6'));
  result.measurement_7 = JSON.parse(app.getAnswer('measurement_7'));
  result.measurement_8 = JSON.parse(app.getAnswer('measurement_8'));
  result.measurement_9 = JSON.parse(app.getAnswer('measurement_9'));
  result.measurement_10 = JSON.parse(app.getAnswer('measurement_10'));
  result.measurement_11 = JSON.parse(app.getAnswer('measurement_11'));
  result.measurement_12 = JSON.parse(app.getAnswer('measurement_12'));

  console.log(result);
  app.result(result);
})();