/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */

export default () => {
  const calibrateAllWhite = [{
      calibration: 1,
      averages: 3,
      //      pulses: [10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10],
      pulses: [60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60],
      data_type: [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
      pulse_length: [
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
      ],
      pulse_distance: [
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
        1500,
      ],
      pulsed_lights: [
        [1],
        [2],
        [3],
        [4],
        [5],
        [6],
        [7],
        [8],
        [9],
        [10],
        [1],
        [2],
        [3],
        [4],
        [5],
        [6],
        [7],
        [8],
        [9],
        [10],
      ],
      pulsed_lights_brightness: [
        [35],
        [50],
        [550],
        [1000],
        [500],
        [400],
        [900],
        [950],
        [250],
        [100],
        [35],
        [50],
        [550],
        [1000],
        [500],
        [400],
        [900],
        [950],
        [250],
        [100],
      ],
      detectors: [
        [0],
        [0],
        [0],
        [0],
        [0],
        [0],
        [0],
        [0],
        [0],
        [0],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
      ],
    },
    {
      environmental: [
        ['temperature_humidity_pressure_voc'],
      ],
    },
  ];

  // When making a copy of another protocol, JSON.parse(JSON.stringify()) is needed to ensure that you are creating an entirely separate copy (not referenced to the original)
  const calibrateAllBlack = JSON.parse(JSON.stringify(calibrateAllWhite));
  calibrateAllBlack[0]['calibration'] = 2;

  const calibrateAllBlank = JSON.parse(JSON.stringify(calibrateAllWhite));
  calibrateAllBlank[0]['calibration'] = 3;
  calibrateAllBlank[0]['data_type'] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  // calibrateAllBlank[0]['data_type'] = [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2];

  const calibrateAllWhiteMaster = JSON.parse(JSON.stringify(calibrateAllWhite));
  calibrateAllWhiteMaster[0]['calibration'] = 4;
  calibrateAllWhiteMaster[0]['data_type'] = [
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
  ];

  const calibrateAllBlackMaster = JSON.parse(JSON.stringify(calibrateAllWhite));
  calibrateAllBlackMaster[0]['calibration'] = 5;
  calibrateAllBlackMaster[0]['data_type'] = [
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
  ];

  const standard = [{
      averages: 3,
      pulses: [60, 60, 60, 60, 60, 60, 60, 60, 60, 60],
      pulse_length: [
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
        [7],
      ],
      pulse_distance: [1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500],
      pulsed_lights: [
        [3],
        [4],
        [5],
        [9],
        [6],
        [7],
        [8],
        [1],
        [2],
        [10],
      ],
      pulsed_lights_brightness: [
        [550],
        [1000],
        [500],
        [250],
        [400],
        [900],
        [950],
        [35],
        [50],
        [100],
      ],
      detectors: [
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [0],
        [0],
        [0],
      ],
    },
    {
      environmental: [
        ['temperature_humidity_pressure_voc'],
      ],
    },
  ];

  // When making a copy of another protocol, JSON.parse(JSON.stringify()) is needed to ensure that you are creating an entirely separate copy (not referenced to the original)

  const standardBlank = JSON.parse(JSON.stringify(standard));
  standardBlank[0]['data_type'] = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];

  const calibrateBlankLocal = JSON.parse(JSON.stringify(standard));
  calibrateBlankLocal[0]['calibration'] = 6;

  const standardApplyCalibration = JSON.parse(JSON.stringify(standard));
  standardApplyCalibration[0]['recall'] = [];
  for (let i = 0; i < 31; i++) {
    standardApplyCalibration[0]['recall'].push('userdef[' + i + ']');
  }

  const standardRaw = JSON.parse(JSON.stringify(standard));
  standardRaw[0]['data_type'] = [2, 2, 2, 2, 2, 2, 2, 2, 2, 2];
  standardRaw[0]['show_voltage'] = 1;

  const standardRawNoHeat = JSON.parse(JSON.stringify(standard));
  standardRawNoHeat[0]['data_type'] = [3, 3, 3, 3, 3, 3, 3, 3, 3, 3];
  standardRawNoHeat[0]['show_voltage'] = 1; // make sure that voltage is outputted

  const standardHeatTest = JSON.parse(JSON.stringify(standardRaw));
  standardHeatTest[0]['show_voltage'] = 1; // make sure that voltage is outputted
  standardHeatTest[0]['protocols'] = 12;
  standardHeatTest[0]['protocols_delay'] = 240000;
  standardHeatTest[0]['environmental'] = [ // take temp measurements every time (not just at the end)
    ['temperature_humidity_pressure_voc'],
  ];
  standardHeatTest.splice(1, 1); // get rid of temp measurement at the end since now we're taking it every time.

  const standardRawHeatCal = JSON.parse(JSON.stringify(standardRawNoHeat));
  standardRawHeatCal[0]['protocols'] = 12;
  standardRawHeatCal[0]['protocols_delay'] = 240000;
  standardRawHeatCal[0]['environmental'] = [ // take temp measurements every time (not just at the end)
    ['temperature_humidity_pressure_voc'],
  ];
  standardRawHeatCal.splice(1, 1); // get rid of temp measurement at the end since now we're taking it every time.

  const environmental_only = [{
    environmental: [
      ['temperature_humidity_pressure_voc'],
    ],
  }];

  const oldReflectometer2_flat = [{
    environmental: [
      ['temperature_humidity_pressure_voc'],
    ],
    pulses: [60, 60, 60, 60, 60, 60, 60, 60, 60, 60],
    pulse_length: [
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
      [7],
    ],
    pulse_distance: [1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500],
    pulsed_lights: [
      [3],
      [4],
      [5],
      [9],
      [6],
      [7],
      [8],
      [1],
      [2],
      [10],
    ],
    pulsed_lights_brightness: [
      [550],
      [1200],
      [700],
      [250],
      [450],
      [1200],
      [1250],
      [40],
      [80],
      [100],
    ],
    detectors: [
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [1],
      [0],
      [0],
      [0],
    ],
    dac_lights: 1,
    averages: 5,
    object_type: 'object',
    calibration: 'no',
  }];

  return {
    calibrateAllWhite,
    calibrateAllBlack,
    calibrateAllBlank,
    calibrateAllWhiteMaster,
    calibrateAllBlackMaster,
    standard,
    standardBlank,
    standardRaw,
    standardRawNoHeat,
    standardRawHeatCal,
    standardHeatTest,
    standardApplyCalibration,
    calibrateBlankLocal,
    environmental_only,
    oldReflectometer2_flat,
  };
};