/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */
/* eslint-disable linebreak-style */
/* global processor */

import { sprintf } from 'sprintf-js';
import _ from 'lodash';

import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';

const fertilizerValueMap = {
  fertilizer: 0.1,
  sell: 0,
  dont_collect: 0,
};
const soilValueMap = {
  sandy: 1,
  silty: 3,
  clay: 2,
};
const drainageValueMap = {
  rapid: 1,
  mod_quick: 3,
  mod_slow: 3,
  slow: 1,
};
const slopeValueMap = {
  level: 3,
  gentle: 3,
  mod_slope: 2,
  strong_slope: 2,
  mod_steep: 1,
  steep: 1,
  very_steep: 0,
};
const previousValueMap = {
  maize: 0,
  nonlegume: 0,
  annual_legume: 0.05,
  perennial_legume: 0.1,
};
const yieldValueMap = {
  good: 3,
  average: 2,
  poor: 1,
};
const tillageValueMap = {
  ridge: 1,
  flat: 2,
  notill: 3,
};
const residueValueMap = {
  remove: 1,
  bury: 3,
  surface: 3,
  graze: 2,
};

function accumulateValues(answer, map) {
  const items = answer.split(',');
  let acc = 0;
  items.forEach((i) => {
    if (map[i]) {
      acc += map[i];
    }
  });
  return acc;
}

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  if (result.error) {
    ui.error('Something was not right', result.error);
    return;
  }

  const errors = {};

  const field1 = {};
  const field2 = {};
  const field3 = {};

  const weightedValued = 0;

  let soilType = result['field1_soil'];
  const soil1_value = soilValueMap[soilType];

  soilType = result['field2_soil'];
  const soil2_value = soilValueMap[soilType];

  soilType = result['field3_soil'];
  const soil3_value = soilValueMap[soilType];

  let soilDrainage = result['field1_drainage'];
  const drainage1_value = drainageValueMap[soilDrainage];

  soilDrainage = result['field2_drainage'];
  const drainage2_value = drainageValueMap[soilDrainage];

  soilDrainage = result['field3_drainage'];
  const drainage3_value = drainageValueMap[soilDrainage];

  let soilSlope = result['field1_slope'];
  const slope1_value = slopeValueMap[soilSlope];

  soilSlope = result['field2_slope'];
  const slope2_value = slopeValueMap[soilSlope];

  soilSlope = result['field3_slope'];
  const slope3_value = slopeValueMap[soilSlope];

  let soilPrevious = result['field1_previous_crop'];
  const previous1_value = accumulateValues(soilPrevious, previousValueMap);

  soilPrevious = result['field2_previous_crop'];
  const previous2_value = accumulateValues(soilPrevious, previousValueMap);

  soilPrevious = result['field3_previous_crop'];
  const previous3_value = accumulateValues(soilPrevious, previousValueMap);

  let soilYield = result['field1_yield'];
  const yield1_value = yieldValueMap[soilYield];

  soilYield = result['field2_yield'];
  const yield2_value = yieldValueMap[soilYield];

  soilYield = result['field3_yield'];
  const yield3_value = yieldValueMap[soilYield];

  let soilTillage = result['field1_tillage'];
  const tillage1_value = tillageValueMap[soilTillage];

  soilTillage = result['field2_tillage'];
  const tillage2_value = tillageValueMap[soilTillage];

  soilTillage = result['field3_tillage'];
  const tillage3_value = tillageValueMap[soilTillage];

  let soilResidue = result['field1_residue'];
  const residue1_value = residueValueMap[soilResidue];

  soilResidue = result['field2_residue'];
  const residue2_value = residueValueMap[soilResidue];

  soilResidue = result['field3_residue'];
  const residue3_value = residueValueMap[soilResidue];

  const Field1_score =
    soil1_value +
    drainage1_value +
    slope1_value +
    previous1_value +
    yield1_value +
    tillage1_value +
    residue1_value;
    
  const Field2_score =
    soil2_value +
    drainage2_value +
    slope2_value +
    previous2_value +
    yield2_value +
    tillage2_value +
    residue2_value;

  const Field3_score =
    soil3_value +
    drainage3_value +
    slope3_value +
    previous3_value +
    yield3_value +
    tillage3_value +
    residue3_value;

  /*
Here is where I need to add a ranking function.
Field with greatest sum should get fertilizer first.
*/

  /* Code to check if an answer is in a list
  if(answer.split(",").contains("clay")){ */

  const fertilizerType = result['cattle_manure'];
  const cattle_manure_value = fertilizerValueMap[fertilizerType];

  const fertilizerType_goat = result['goat_manure'];
  const goat_manure_value = fertilizerValueMap[fertilizerType_goat];

  const fertilizerType_pig = result['pig_manure'];
  const pig_manure_value = fertilizerValueMap[fertilizerType_pig];

  const fertilizerType_chicken = result['chickens_manure'];
  const chicken_manure_value = fertilizerValueMap[fertilizerType_chicken];

  const total_maure_credit =
    result['cattle_number'] * cattle_manure_value +
    goat_manure_value * (result['goats_number'] / 2) +
    pig_manure_value * (result['pigs_number'] / 2) +
    chicken_manure_value;

  let legume_credit_Field1 = 0;
  console.log(result['field1_previous_crop'].split(','));
  if (
    result['field1_previous_crop'].split(',').includes('annual_legume') ||
    result['field1_previous_crop'].split(',').includes('perennial_legume')
  ) {
    legume_credit_Field1 = previous1_value * yield1_value;
  }

  let legume_credit_Field2 = 0;
  console.log(result['field2_previous_crop'].split(','));
  if (
    result['field2_previous_crop'].split(',').includes('annual_legume') ||
    result['field2_previous_crop'].split(',').includes('perennial_legume')
  ) {
    legume_credit_Field2 = previous2_value * yield2_value;
  }

  let legume_credit_Field3 = 0;
  console.log(result['field3_previous_crop'].split(','));
  if (
    result['field3_previous_crop'].split(',').includes('annual_legume') ||
    result['field3_previous_crop'].split(',').includes('perennial_legume')
  ) {
    legume_credit_Field3 = previous3_value * yield3_value;
  }
  const labels = ['Field 1', 'Field 2', 'Field 3'];
  const series = [Field1_score, Field2_score, Field3_score];

  ui.barchart(
    {
      series: [series],
    },
    'Field Values',
    {
      axisX: {
        labelInterpolationFnc(value, index) {
          return `${labels[index]}`;
        },
      },
    },
  );

  ui.info('Field 1 Value', Field1_score);
  app.csvExport('Field 1 Value', Field1_score);
  ui.info('Field 2 Value', Field2_score);
  app.csvExport('Field 2 Value', Field2_score);
  ui.info('Field 3 Value', Field3_score);
  app.csvExport('Field 3 Value', Field3_score);
  ui.info('Manure credit in bags per hectare', total_maure_credit);
  app.csvExport('Manure credit in bags per hectare', total_maure_credit);
  ui.info('Legume credit in bags per hectare for Field 1', legume_credit_Field1);
  app.csvExport('Legume credit in bags per hectare for Field 1', legume_credit_Field1);
  ui.info('Legume credit in bags per hectare for Field 2', legume_credit_Field2);
  app.csvExport('Legume credit in bags per hectare for Field 2', legume_credit_Field2);
  ui.info('Legume credit in bags per hectare for Field 3', legume_credit_Field3);
  app.csvExport('Legume credit in bags per hectare for Field 3', legume_credit_Field3);
  /* ui.info('Recommendation for field1', result.recommendations.fieldOne);
*/
  app.save();
})();
