/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

import { sprintf } from 'sprintf-js';
import _ from 'lodash';

import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';
// import { checkServerIdentity } from 'tls';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  if (result.error) {
    ui.error('Something was not right', result.error);
    return;
  }

  ui.info('SOC 5 cm g kg', result.soilGrid.properties.ORCDRC.M.sl1);
  app.csvExport('SOC 5 cm g kg', result.soilGrid.properties.ORCDRC.M.sl1);

  ui.info('SOC 5-15 cm g kg', result.soilGrid.properties.ORCDRC.M.sl2);
  app.csvExport('SOC 5-15 cm g kg', result.soilGrid.properties.ORCDRC.M.sl2);

  ui.info('SOC 15-30 cm g kg', result.soilGrid.properties.ORCDRC.M.sl3);
  app.csvExport('SOC 15-30 cm g kg', result.soilGrid.properties.ORCDRC.M.sl3);

  ui.info('SOC 30-60 cm g kg', result.soilGrid.properties.ORCDRC.M.sl4);
  app.csvExport('SOC 30-60 cm g kg', result.soilGrid.properties.ORCDRC.M.sl4);

  ui.info('SOC 60-100 cm g kg', result.soilGrid.properties.ORCDRC.M.sl5);
  app.csvExport('SOC 60-100 cm g kg', result.soilGrid.properties.ORCDRC.M.sl5);

  ui.info('SOC 100-200 cm g kg', result.soilGrid.properties.ORCDRC.M.sl6);
  app.csvExport('SOC 100-200 cm g kg', result.soilGrid.properties.ORCDRC.M.sl6);

  ui.info('C stocks 5 cm kg ha', result.soilGrid.properties.OCSTHA.M.sd1);
  app.csvExport('C stocks 5 cm kg ha', result.soilGrid.properties.OCSTHA.M.sd1);

  ui.info('C stocks 5-15 cm kg ha', result.soilGrid.properties.OCSTHA.M.sd2);
  app.csvExport('C stocks 5-15 cm kg ha', result.soilGrid.properties.OCSTHA.M.sd2);

  ui.info('C stocks 15-30 cm kg ha', result.soilGrid.properties.OCSTHA.M.sd3);
  app.csvExport('C stocks 15-30 cm kg ha', result.soilGrid.properties.OCSTHA.M.sd3);

  ui.info('C stocks 30-60 cm kg ha', result.soilGrid.properties.OCSTHA.M.sd4);
  app.csvExport('C stocks 30-60 cm kg ha', result.soilGrid.properties.OCSTHA.M.sd4);

  ui.info('C stocks 60-100 cm kg ha', result.soilGrid.properties.OCSTHA.M.sd5);
  app.csvExport('C stocks 60-100 cm kg ha', result.soilGrid.properties.OCSTHA.M.sd5);

  ui.info('C stocks 100-200 cm kg ha', result.soilGrid.properties.OCSTHA.M.sd6);
  app.csvExport('C stocks 100-200 cm kg ha', result.soilGrid.properties.OCSTHA.M.sd6);

  ui.info('BD 5 cm kg m3', result.soilGrid.properties.BLDFIE.M.sl1);
  app.csvExport('BD 5 kg m3', result.soilGrid.properties.BLDFIE.M.sl1);

  ui.info('BD 5-15 kg m3', result.soilGrid.properties.BLDFIE.M.sl2);
  app.csvExport('BD 5-15 kg m3', result.soilGrid.properties.BLDFIE.M.sl2);

  ui.info('BD 15-30 kg m3', result.soilGrid.properties.BLDFIE.M.sl3);
  app.csvExport('BD 15-30 kg m3', result.soilGrid.properties.BLDFIE.M.sl3);

  ui.info('BD 30-60 kg m3', result.soilGrid.properties.BLDFIE.M.sl4);
  app.csvExport('BD 30-60 kg m3', result.soilGrid.properties.BLDFIE.M.sl4);

  ui.info('BD 60-100 kg m3', result.soilGrid.properties.BLDFIE.M.sl5);
  app.csvExport('BD 60-100 kg m3', result.soilGrid.properties.BLDFIE.M.sl5);

  ui.info('BD 100-200 kg m3', result.soilGrid.properties.BLDFIE.M.sl6);
  app.csvExport('BD 100-200 kg m3', result.soilGrid.properties.BLDFIE.M.sl6);

  app.save();
})();
