
import { app } from "@oursci/scripts";

let err = null;

/**
 * calls previously passed onAnswerMissing function if question has not been answered
 * @param {('metaDateCreated'|'metaDateModified'|'metaInstanceID'|'metaUserID'|'sample_id'|'sample_quality'|'sample_type'|'sample_type_other'|'sample_color'|'clean_directions'|'carrot_raw_directions'|'grape_scan_directions'|'tomato_scan_directions'|'spinach_scan_directions'|'lettuce_scan_directions'|'kale_scan_directions'|'rawscan1'|'rawscan2'|'mir_directions'|'balance_connection'|'carrot_cut'|'grape_processing'|'cherry_tomato_processing'|'sample_diameter'|'carrot_shredding'|'dry_weight_instructions'|'cup_wt_ohaus'|'cup_wt_manual'|'pre_weight_ohaus'|'pre_weight_manual'|'explaindry'|'cherrytomato_grinding'|'grape_grinding'|'spinach_juicing'|'kale_juicing'|'lettuce_juicing'|'carrot_sample'|'grape_sample'|'tomato_sample'|'spinach_sample'|'kale_sample'|'lettuce_sample'|'sample_weight_ohaus'|'sample_weight_manual'|'freezing_directions'|'extractant'|'other_extractant'|'juice_carrots'|'juice_grapes'|'juice_tomato'|'juice_spinach'|'juice_kale'|'juice_lettuce'|'juice_sample'|'mir_juice_directions'|'juice_blank'|'juice_scan'|'brix'|'post_weight_ohaus'|'post_weight_manual'|'next_steps'|'get_chemisty'|'final_results'|'review')} answer
 */
export function req(question) {
  if (!app.getAnswer(question)) {
    err(question);
    return false;
  }
  return true;
}


/**
 * returns false if questions is not answered with eq
 * @param {('metaDateCreated'|'metaDateModified'|'metaInstanceID'|'metaUserID'|'sample_id'|'sample_quality'|'sample_type'|'sample_type_other'|'sample_color'|'clean_directions'|'carrot_raw_directions'|'grape_scan_directions'|'tomato_scan_directions'|'spinach_scan_directions'|'lettuce_scan_directions'|'kale_scan_directions'|'rawscan1'|'rawscan2'|'mir_directions'|'balance_connection'|'carrot_cut'|'grape_processing'|'cherry_tomato_processing'|'sample_diameter'|'carrot_shredding'|'dry_weight_instructions'|'cup_wt_ohaus'|'cup_wt_manual'|'pre_weight_ohaus'|'pre_weight_manual'|'explaindry'|'cherrytomato_grinding'|'grape_grinding'|'spinach_juicing'|'kale_juicing'|'lettuce_juicing'|'carrot_sample'|'grape_sample'|'tomato_sample'|'spinach_sample'|'kale_sample'|'lettuce_sample'|'sample_weight_ohaus'|'sample_weight_manual'|'freezing_directions'|'extractant'|'other_extractant'|'juice_carrots'|'juice_grapes'|'juice_tomato'|'juice_spinach'|'juice_kale'|'juice_lettuce'|'juice_sample'|'mir_juice_directions'|'juice_blank'|'juice_scan'|'brix'|'post_weight_ohaus'|'post_weight_manual'|'next_steps'|'get_chemisty'|'final_results'|'review')} question
 * @param {string} eq the case insensitive string to compare to
 */
export function sel(question, eq = "yes") {
  const answer = app.getAnswer(question);
  if (answer && answer.toLowerCase() === eq.toLowerCase()) {
    return true;
  }
  return false;
}

/**
 *
 * @param {function} onAnswerMissing This is called when an answer is missing, as argument the
 * question ID is given
 */
export const survey = (onAnswerMissing) => {
  err = onAnswerMissing;
  return {
'metaDateCreated' : app.getAnswer('metaDateCreated'),
'metaDateModified' : app.getAnswer('metaDateModified'),
'metaInstanceID' : app.getAnswer('metaInstanceID'),
'metaUserID' : app.getAnswer('metaUserID'),
'sample_id' : app.getAnswer('sample_id'),
'sample_quality' : app.getAnswer('sample_quality'),
'sample_type' : app.getAnswer('sample_type'),
'sample_type_other' : app.getAnswer('sample_type_other'),
'sample_color' : app.getAnswer('sample_color'),
'clean_directions' : app.getAnswer('clean_directions'),
'carrot_raw_directions' : app.getAnswer('carrot_raw_directions'),
'grape_scan_directions' : app.getAnswer('grape_scan_directions'),
'tomato_scan_directions' : app.getAnswer('tomato_scan_directions'),
'spinach_scan_directions' : app.getAnswer('spinach_scan_directions'),
'lettuce_scan_directions' : app.getAnswer('lettuce_scan_directions'),
'kale_scan_directions' : app.getAnswer('kale_scan_directions'),
'rawscan1' : app.getAnswer('rawscan1'),
'rawscan2' : app.getAnswer('rawscan2'),
'mir_directions' : app.getAnswer('mir_directions'),
'balance_connection' : app.getAnswer('balance_connection'),
'carrot_cut' : app.getAnswer('carrot_cut'),
'grape_processing' : app.getAnswer('grape_processing'),
'cherry_tomato_processing' : app.getAnswer('cherry_tomato_processing'),
'sample_diameter' : app.getAnswer('sample_diameter'),
'carrot_shredding' : app.getAnswer('carrot_shredding'),
'dry_weight_instructions' : app.getAnswer('dry_weight_instructions'),
'cup_wt_ohaus' : app.getAnswer('cup_wt_ohaus'),
'cup_wt_manual' : app.getAnswer('cup_wt_manual'),
'pre_weight_ohaus' : app.getAnswer('pre_weight_ohaus'),
'pre_weight_manual' : app.getAnswer('pre_weight_manual'),
'explaindry' : app.getAnswer('explaindry'),
'cherrytomato_grinding' : app.getAnswer('cherrytomato_grinding'),
'grape_grinding' : app.getAnswer('grape_grinding'),
'spinach_juicing' : app.getAnswer('spinach_juicing'),
'kale_juicing' : app.getAnswer('kale_juicing'),
'lettuce_juicing' : app.getAnswer('lettuce_juicing'),
'carrot_sample' : app.getAnswer('carrot_sample'),
'grape_sample' : app.getAnswer('grape_sample'),
'tomato_sample' : app.getAnswer('tomato_sample'),
'spinach_sample' : app.getAnswer('spinach_sample'),
'kale_sample' : app.getAnswer('kale_sample'),
'lettuce_sample' : app.getAnswer('lettuce_sample'),
'sample_weight_ohaus' : app.getAnswer('sample_weight_ohaus'),
'sample_weight_manual' : app.getAnswer('sample_weight_manual'),
'freezing_directions' : app.getAnswer('freezing_directions'),
'extractant' : app.getAnswer('extractant'),
'other_extractant' : app.getAnswer('other_extractant'),
'juice_carrots' : app.getAnswer('juice_carrots'),
'juice_grapes' : app.getAnswer('juice_grapes'),
'juice_tomato' : app.getAnswer('juice_tomato'),
'juice_spinach' : app.getAnswer('juice_spinach'),
'juice_kale' : app.getAnswer('juice_kale'),
'juice_lettuce' : app.getAnswer('juice_lettuce'),
'juice_sample' : app.getAnswer('juice_sample'),
'mir_juice_directions' : app.getAnswer('mir_juice_directions'),
'juice_blank' : app.getAnswer('juice_blank'),
'juice_scan' : app.getAnswer('juice_scan'),
'brix' : app.getAnswer('brix'),
'post_weight_ohaus' : app.getAnswer('post_weight_ohaus'),
'post_weight_manual' : app.getAnswer('post_weight_manual'),
'next_steps' : app.getAnswer('next_steps'),
'get_chemisty' : app.getAnswer('get_chemisty'),
'final_results' : app.getAnswer('final_results'),
'review' : app.getAnswer('review')}
};
