/* eslint-disable linebreak-style */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

/*
import {
  sprintf
} from 'sprintf-js';
import _ from 'lodash';
*/

import {
  app,
  math,
} from '@oursci/scripts';
import * as ui from '@oursci/measurements-ui';
import '@oursci/measurements-ui/dist/styles.min.css';
import {
  survey
} from '../.oursci-surveys/survey.js';

function isEmpty(obj) {
  for (var key in obj) {
    if (obj.hasOwnProperty(key))
      return false;
  }
  return true;
}

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  /*
  Object.keys(result.error).forEach((a) => {
    ui.error(`Answer '${a}' is ${result.error[a]}`, 'Return to the answer and fix it.');
  });
*/
  // Define parameters to calculate polyphenol and antioxidant;

  // norm_weight is the moisture content for each produce type based on USDA nutritional database;

  const poly_conv = 0.1; // needed to convert ug ml-1 to mg 100 g FW for polyphenol;

  if (result.sample_type === '') {
    app.error();
    ui.error('missing sample type', 'sample type should be carrot, spinach, etc. etc..  calculations applied but norm_weight set to 0.95');
  }

  console.log(JSON.stringify(result));
  // Calculate final Polyphenol, Antioxidant and Protein Content;

  let cup_wt = 0;
  if (
    !isEmpty(result.cup_wt_ohaus) &&
    typeof result.cup_wt_ohaus.data !== 'undefined' &&
    typeof result.cup_wt_ohaus.data.weight_grams !== 'undefined' &&
    result.cup_wt_ohaus.data.weight_grams !== 'undefined'
  ) {
    cup_wt = result.cup_wt_ohaus.data.weight_grams;
    ui.info('Cup Weight (ohaus)', math.MathROUND(cup_wt, 2));
  } else if (Number(result.cup_wt_manual)) {
    cup_wt = result.cup_wt_manual;
    ui.info('Cup Weight (manual)', math.MathROUND(cup_wt, 2));
  } else {
    app.error();
    ui.error(
      'Cup Weight Missing',
      'The cup weight is missing, so moisture content cannot be calculated.  Go back and fill in the weight.',
    );
  }

  let pre_wt = 0;
  if (
    !isEmpty(result.pre_weight_ohaus) &&
    typeof result.pre_weight_ohaus.data !== 'undefined' &&
    typeof result.pre_weight_ohaus.data.weight_grams !== 'undefined' &&
    result.pre_weight_ohaus.data.weight_grams !== 'undefined'
  ) {
    pre_wt = result.pre_weight_ohaus.data.weight_grams;
    ui.info('Pre Weight (ohaus)', math.MathROUND(pre_wt, 2));
  } else if (Number(result.pre_weight_manual)) {
    pre_wt = result.pre_weight_manual;
    ui.info('Pre Weight (manual)', math.MathROUND(pre_wt, 2));
  } else {
    app.error();
    ui.error(
      'Pre Weight Missing',
      'The pre weight is missing, so moisture content cannot be calculated.  Go back and fill in the weight.',
    );
  }

  let post_wt = 0;
  if (
    !isEmpty(result.post_weight_ohaus) &&
    typeof result.post_weight_ohaus.data !== 'undefined' &&
    typeof result.post_weight_ohaus.data.weight_grams !== 'undefined' &&
    result.post_weight_ohaus.data.weight_grams !== 'undefined'
  ) {
    post_wt = result.post_weight_ohaus.data.weight_grams;
    ui.info('Post Weight (ohaus)', math.MathROUND(post_wt, 2));
  } else if (Number(result.post_weight_manual)) {
    post_wt = result.post_weight_manual;
    ui.info('Post Weight (manual)', math.MathROUND(post_wt, 2));
  } else {
    app.error();
    ui.error(
      'Post Weight Missing',
      'The post weight is missing, so moisture content cannot be calculated.  Go back and fill in the weight.',
    );
  }

  let sample_wt = 0;
  if (
    !isEmpty(result.sample_weight_ohaus) &&
    typeof result.sample_weight_ohaus.data !== 'undefined' &&
    typeof result.sample_weight_ohaus.data.weight_grams !== 'undefined' &&
    result.sample_weight_ohaus.data.weight_grams !== 'undefined'
  ) {
    sample_wt = result.sample_weight_ohaus.data.weight_grams;
    ui.info('Sample Weight (ohaus)', math.MathROUND(sample_wt, 2));
  } else if (Number(result.sample_weight_manual)) {
    sample_wt = result.sample_weight_manual;
    ui.info('Sample Weight (manual)', math.MathROUND(sample_wt, 2));
  } else {
    app.error();
    ui.error(
      'Sample Weight Missing',
      'The sample weight is missing, so moisture content cannot be calculated.  Go back and fill in the weight.',
    );
  }

  // Inform if any of the following are missing
  // --> for testing edge cases
  //  delete result.finalResults.polyphenols.dilution;
  // result.extractant = '20';
  //  sample_wt = undefined;

  if ((typeof result.finalResults.polyphenols.dilution === 'undefined' || !isFinite(result.finalResults.polyphenols.dilution)) || (Number(result.finalResults.polyphenols.dilution) <= 0 || Number(result.finalResults.polyphenols.dilution) > 50)) {
    app.error();
    ui.error(
      `The dilution (${result.finalResults.polyphenols.dilution}) is outside of range`,
      'Go back to the Wet Chemistry survey and fix it!',
    );
  }
  if ((typeof result.extractant === 'undefined' || !isFinite(result.extractant) || (Number(result.extractant)) <= 0 || Number(result.extractant) > 30)) {
    app.error();
    ui.error(
      `The ml extractant (${result.extractant}) is outside of range`,
      'Go back to the Wet Chemistry survey and fix it!',
    );
  }
  if ((typeof sample_wt === 'undefined' || !isFinite(sample_wt)) || (Number(sample_wt) <= 0 || Number(sample_wt) > 20)) {
    app.error();
    ui.error(
      `The sample weight (${sample_wt}) is outside of range`,
      'Go back to the Wet Chemistry survey and fix it!',
    );
  }

  // Calculates moisture content;
  let moisture_content = 0;
  if (
    isNaN(pre_wt) ||
    isNaN(post_wt) ||
    isNaN(cup_wt) ||
    cup_wt === 0 ||
    pre_wt === 0 ||
    post_wt === 0
  ) {
    ui.warning(
      `Weight is missing, default of ${result.finalResults.norm_weight} is applied as moisture content`,
      'A weight is missing, so moisture cannot be calculated.  Go back and fill in all sample weights.  If weights are not present, an estimated moisture of will be applied.',
    );
    moisture_content = math.MathROUND(result.finalResults.norm_weight, 3);
  } else {
    moisture_content = (pre_wt - post_wt) / (pre_wt - cup_wt);
    if (typeof moisture_content === 'undefined' || !isFinite(moisture_content)) {
      app.error();
      ui.error(
        `The moisture was NaN or infinity, default of ${result.finalResults.norm_weight} is applied as moisture content`,
        'Please check that the entered weights are correct, otherwise we will set the correction factor to 1',
      );
      moisture_content = math.MathROUND(result.finalResults.norm_weight, 3);
    }
    if (moisture_content < 0 || moisture_content > 1) {
      app.error();
      ui.error(
        `The moisture content out of range, default of ${result.finalResults.norm_weight} is applied as moisture content`,
        'Please check that the entered weights are correct, otherwise we will set the correction factor to 1',
      );
      moisture_content = math.MathROUND(result.finalResults.norm_weight, 3);
    }
    if (moisture_content < 0.6 && moisture_content > 0) {
      ui.warning(
        'The moisture content was very low',
        'If the vegetable was very dry this may be fine, otherwise please check that the entered weights are correct',
      );
    }
    ui.info('Moisture Content', math.MathROUND(moisture_content, 3));
    app.csvExport('moistureContent', math.MathROUND(moisture_content, 3));
    app.csvExport('dryMatterContent', math.MathROUND(1 - moisture_content, 3));
    app.csvExport('dryMatterContentNorm', math.MathROUND(1 - result.finalResults.norm_weight, 3));
  }

  // let CF = 0;
  // if (moisture_content  0 || moisture_content < 0 || moisture_content > 1) {
  //   CF = 1;
  //   ui.info('Correction Factor', math.MathROUND(CF, 3));
  //   app.csvExport('correctionFactor', math.MathROUND(CF, 3));
  // } else {
  //   CF = moisture_content / result.finalResults.norm_weight;
  //   ui.info('Correction Factor', math.MathROUND(CF, 3));
  //   app.csvExport('correctionFactor', math.MathROUND(CF, 3));
  // }

  /*
  CF = moisture_content / result.finalResults.norm_weight;
  raw = ((result.finalResults.polyphenols.value * result.extractant) / sample_wt) * poly_conv
  raw * CF * dilution

  // Dan
  raw_sample_poly = 
  // long
  total = ((orig_poly * extractant) / sample_wt) * poly_conv * (moisture / norm_moisture) * dilution
  // short
  total = raw_sample_poly * (moisture / norm_moisture) * dilution

  // greg
  // long
  total =  dilution * (1 - norm_moisture) * (((orig_poly * extractant) / sample_wt) * poly_conv) / (1 - moisture)
  // short
  total = dilution * raw_sample_poly * (1 - norm_moisture) / (1 - moisture)

  // comparison of short
  total = raw_sample_poly * dilution * (moisture / norm_moisture);
  total = raw_sample_poly * dilution * (1 - norm_moisture) / (1 - moisture);

  antioxdants
  moisture = .9
  <-- let's say value in sample is 1g total, so .1g dry mass
  norm moisture = .95
  moisture / norm_moisture = .9473
  (1-norm_moisture) / (1-moisture) = .5

  // di
  dry weight /
 */

  // make sure to ouptut all factors needed to recalculate poly and anti as needed...
  app.csvExport('sample_weight_final', math.MathROUND(sample_wt, 3));
  if (result.finalResults.polyphenols.value !== '' && result.finalResults.polyphenols.dilution !== '') {
    const dry_weight = 1 - moisture_content;
    const orig_sample_poly = result.finalResults.polyphenols.value * result.finalResults.polyphenols.dilution;
    const raw_sample_poly = ((orig_sample_poly * result.extractant) / sample_wt) * poly_conv; // ug / 100 g raw fresh weight sample weight... (extractant is ml 80% methanol used.)
    const dry_sample_poly = raw_sample_poly / dry_weight; // ug / 100g dry weight
    const polyphenols = dry_sample_poly * (1 - result.finalResults.norm_weight); // ug / 100g normalized weight (based on USDA averages see sensor script for list)
    //    CF = ${math.MathROUND(CF, 3)}<br>
    ui.info('Total Polyphenols',
      `dry weight = ${math.MathROUND(dry_weight, 3)}<br>
      dry weight (norm) = ${math.MathROUND((1 - result.finalResults.norm_weight), 3)}<br>
    Poly raw (in meth) = ${math.MathROUND(orig_sample_poly, 2)}<br>
    Poly / 100g raw = ${math.MathROUND(raw_sample_poly, 2)}<br>
    Poly / 100g dry = ${math.MathROUND(dry_sample_poly, 2)}<br>
    <b>Poly / 100g norm = ${math.MathROUND(polyphenols, 2)}</b><br>`);
    //    ui.info('Total Polyphenols mg GAE 100g FW', `old : new  ${result.finalResults.polyphenols.value} : ${math.MathROUND(polyphenols, 2)}`);
    app.csvExport('polyphenolsMgGae100gFw', math.MathROUND(polyphenols, 2));
    app.csvExport('polyphenolsMgGae100gFw_dry', math.MathROUND(dry_sample_poly, 2));
    app.csvExport('polyphenolsMgGae100gFw_dilution', math.MathROUND(result.finalResults.polyphenols.dilution, 2));
  } else {
    app.error();
    ui.error(
      'Polyphenols missing or undefined',
      'Polyphenols not found and/or dilution not found.',
    );
  }

  if (result.finalResults.antioxidants.value !== '' && result.finalResults.antioxidants.dilution !== '') {
    const dry_weight = 1 - moisture_content;
    const orig_sample_anti = result.finalResults.antioxidants.value * result.finalResults.antioxidants.dilution;
    const raw_sample_anti = ((orig_sample_anti * result.extractant) / sample_wt); // ug / 100 g raw fresh weight sample weight... (extractant is ml 80% methanol used.)
    const dry_sample_anti = raw_sample_anti / dry_weight; // ug / 100g dry weight
    const antioxidants = dry_sample_anti * (1 - result.finalResults.norm_weight); // ug / 100g normalized weight (based on USDA averages see sensor script for list)
    //    CF = ${math.MathROUND(CF, 3)}<br>
    ui.info('Total Antioxidants',
      `dry weight = ${math.MathROUND(dry_weight, 3)}<br>
      dry weight (norm) = ${math.MathROUND((1 - result.finalResults.norm_weight), 3)}<br>
    Anti raw (in meth) = ${math.MathROUND(orig_sample_anti, 2)}<br>
    Anti / 100g raw = ${math.MathROUND(raw_sample_anti, 2)}<br>
    Anti / 100g dry = ${math.MathROUND(dry_sample_anti, 2)}<br>
    <b>Anti / 100g norm = ${math.MathROUND(antioxidants, 2)}</b><br>`);
    //    ui.info('Total Antioxidants FRAP value', `old : new ${result.finalResults.antioxidants.value} : ${math.MathROUND(antioxidants, 2)}`);
    app.csvExport('antioxidentsFrap', math.MathROUND(antioxidants, 2));
    app.csvExport('antioxidentsFrap_dry', math.MathROUND(dry_sample_anti, 2));
    app.csvExport('antioxidentsFrap_dilution', math.MathROUND(result.finalResults.antioxidants.dilution, 2));
  } else {
    app.error();
    ui.error(
      'Antioxidants missing or undefined',
      'Antioxidants not found and/or dilution not found.',
    );
  }

  // if (result.finalResults.proteins.value !== '' && result.finalResults.proteins.dilution !== '') {
  //   const proteins =
  //     ((result.finalResults.proteins.value * result.extractant) / sample_wt) *
  //     CF *
  //     result.finalResults.protein.dilution;
  //   ui.info('Total Proteins', math.MathROUND(proteins, 2));
  //   app.csvExport('proteins', math.MathROUND(proteins, 2));
  // } else {
  //   ui.error(
  //     'Proteins missing or undefined',
  //     'Proteins not found and/or dilution not found.',
  //   );
  // }
  app.save();
})();