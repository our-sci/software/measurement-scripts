/* eslint-disable linebreak-style */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

import { sprintf } from 'sprintf-js';
// import _ from 'lodash';

import { app } from '@oursci/scripts';
import * as ui from '@oursci/measurements-ui';
import '@oursci/measurements-ui/dist/styles.min.css';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  // Process your results from sensor.js here
  // ...

  if (result.log) {
    // Display each entry from the results log array with an info card
    result.log.forEach((log) => {
      ui.info(log.title, log.content);
    });
  }

  // ui.info('DEBUG', JSON.stringify(result, null, 2));

  // Results of Logistic Regression Models

  if (result.prob_avg <= 0.3) {
    ui.info('Above Average Milk Quality', 'greater than 3.5%');
    app.csvExport('butterfat_average', 'above_average');
    app.csvExport('prob_avg', result.prob_avg);
  } else if (result.prob_avg >= 0.3) {
    ui.info('Below Average Milk Quality', 'less than 3.5%)');
    app.csvExport('butterfat_average', 'below_average');
    app.csvExport('prob_avg', result.prob_avg);
  }

  if (result.prob_avg >= 0.3) {
    ui.info('No Premium for High Quality', 'less than 4.2%');
    app.csvExport('butterfat_high', 'no');
    app.csvExport('prob_high', result.prob_high);
  } else if (result.prob_high <= 0.8) {
    ui.info('No Premium for High Quality', 'less than 4.2%');
    app.csvExport('butterfat_high', 'no');
    app.csvExport('prob_high', result.prob_high);
  } else if (result.prob_high >= 0.8) {
    ui.info('Premium for High Quality', 'greater than 4.2%');
    app.csvExport('butterfat_high', 'yes');
    app.csvExport('prob_high', result.prob_high);
  }

  let box = '';
  if (result.butterfat_content >= 5.0) {
    ui.info('Milk Butterfat content is greater than 5%');
  } else if (result.butterfat_content <= 5.0) {
    box += `From: ${result.min_butterfat.toFixed(3)}<br>`;
    box += `To: ${result.max_butterfat.toFixed(3)}<br>`;
    box +=
      'If this range is not consistent with the "Milk Quality" or "Premium for High Quality" boxes, make decisions based on those boxes';
    ui.info('Estimated Butterfat Range [%]', box);
  }
  box = '';

  app.csvExport('butterfat_content', result.butterfat_content);
  app.csvExport('min_butterfat', result.min_butterfat);
  app.csvExport('max_butterfat', result.max_butterfat);

  if (result.error && result.error.length > 0) {
    // <------ this is new, check if length is 0
    // Display each entry from the results error array with an error card
    app.error(); // this is new, indicating that there is an error
    result.error.forEach((msg) => {
      ui.error('Error', msg);
    });
    app.save();
    return;
  }

  app.save();
})();
