/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

/* global processor */

import { sprintf } from 'sprintf-js';
import _ from 'lodash';

import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';

function erf(x) {
  // save the sign of x
  const sign = x >= 0 ? 1 : -1;
  x = Math.abs(x);

  // constants
  const a1 = 0.254829592;
  const a2 = -0.284496736;
  const a3 = 1.421413741;
  const a4 = -1.453152027;
  const a5 = 1.061405429;
  const p = 0.3275911;

  // A&S formula 7.1.26
  const t = 1.0 / (1.0 + p * x);
  const y = 1.0 - ((((a5 * t + a4) * t + a3) * t + a2) * t + a1) * t * Math.exp(-x * x);
  return sign * y; // erf(-x) = -erf(x);
}

function cdf(x, mean, variance) {
  return 0.5 * (1 + erf((x - mean) / Math.sqrt(2 * variance)));
}

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  console.log(result);
  if (result.errors) {
    if (Object.keys(result.errors).length > 0) {
      Object.keys(result.errors).forEach((k) => {
        console.log(k);
        ui.error(k, result.errors[k]);
      });
      app.save();
      return;
    }
  }

  const species = result.answers.species;
  if (!species) {
    ui.error('Species missing', 'Species not defined, please answer species question first');
  } else {
    const avgs = result.averages[species];
    const vars = result.variances[species];
    const keys = _.keys(avgs);

    _.forEach(keys, (k) => {
      const args = k.split('.');

      const avg = avgs[k];
      const variance = vars[k];

      let ans = '';
      if (args.length === 3) {
        ans = JSON.parse(result.answers[`chlorophyll/${args[0]}`]).data[args[2]];
      } else {
        ans = result.answers[k];
      }

      if (ans && variance && avg) {
        const percentile = cdf(ans, avg, variance);
        ui.donut(
          k,
          sprintf('%.01f %%', percentile * 100),
          percentile,
          `Value is ${ans}, average of other measurements for ${species}: ${sprintf('%.1f', avg)}`,
        );
      } else {
        ui.error('param missing', `for ${k}`);
      }
    });
  }

  ui.info('Soil Type', result.soilGrid.properties.TAXNWRBMajor);
  const taxnwrb = result.soilGrid.properties.TAXNWRB;
  if (taxnwrb) {
    ui.info(
      'Other Soil Types',
      Object.keys(taxnwrb)
        .filter(k => taxnwrb[k] > 7)
        .sort((a, b) => taxnwrb[b] - taxnwrb[a])
        .map(k => `${k}: ${taxnwrb[k]}`)
        .join('<br>'),
    );
  }

  if (result.weather) {
    const totalRain = [];
    console.log(result.weather.data.weather);
    const rainfall = result.weather.data.weather.forEach((w, day) => {
      totalRain[day] = 0;
      w.hourly.forEach((h) => {
        totalRain[day] += Number.parseFloat(h.precipMM);
      });
    });

    ui.info('total rain', totalRain.map(p => sprintf('%.02f mm', p)).join('<br>'));
    ui.barchart(
      {
        series: [totalRain],
      },
      'Rain over past 11 days in mm',
    );
  } else {
    ui.error('Weather data missing', 'unable to fetch weather data, please rerun script');
  }

  app.save();
})();
