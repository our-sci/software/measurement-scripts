export const soils = [
  'Sand',
  'Loamy Sand',
  'Sandy Loam',
  'Sandy Clay Loam',
  'Sandy Clay',
  'Loam',
  'Silt Loam',
  'Silt',
  'Clay Loam',
  'Silty Clay Loam',
  'Clay',
  'Silty Clay',
];

export const slopes = ['flat', 'gentle', 'hilly', 'moderate', 'rolling'];
