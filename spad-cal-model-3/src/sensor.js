/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

/** Greg's notes
 * added //DEBUG to quiet console log statements here and in libraries app, process-wavelengths, and serial.
 */

import app from './lib/app';
import serial from './lib/serial';
import wavelengthProcessor from './lib/process-wavelengths';
import sendDevice from './lib/sendDevice';
import * as MathMore from './lib/math';
import {
  sleep,
} from './lib/utils'; // use: await sleep(1000);

export default serial; // expose this to android for calling onDataAvailable

(async () => {
  await serial.init('/dev/ttyACM0', {
    //  await serial.init('/dev/rfcomm0', {
    baudRate: 115200,
    dataBits: 8,
    stopBits: 1,
    parity: 'none',
  });

  // ///////////////////////////////////////////////////////
  // OPTIONAL instead of reading from the serial port mock some data
  // serial.feed('./mock/mock_sensor_output.json');
  //  serial.write("print_memory+");

  const result = {};
  result.card2 = JSON.parse(app.getAnswer('spad/card-2'));
  result.card3 = JSON.parse(app.getAnswer('spad/card-3'));
  result.card4 = JSON.parse(app.getAnswer('spad/card-4'));
  result.card5 = JSON.parse(app.getAnswer('spad/card-5'));
  result.card6 = JSON.parse(app.getAnswer('spad/card-6'));
  result.card7 = JSON.parse(app.getAnswer('spad/card-7'));
  result.card8 = JSON.parse(app.getAnswer('spad/card-8'));
  result.card9 = JSON.parse(app.getAnswer('spad/card-9'));
  result.card10 = JSON.parse(app.getAnswer('spad/card-10'));

  // result.measurement = JSON.parse(app.getAnswer('info_calibration_check/measurement_check')); // uncomment for -test version
  console.log(result);
  app.result(result);
})();

/*
{
  "id": "spad-cal-model-3",
  "name": "Model to calibrate green SPAD cards 2 - 11",
  "description": "emtpy now, tbd later",
  "version": "3.3.1",
  "action": "Run Measurement",
  "requireDevice": "false"
}
*/