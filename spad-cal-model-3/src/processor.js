/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/* global processor */

import {
  sprintf,
} from 'sprintf-js';
import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';

const Chartist = require('chartist');

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();
  if (result.error) {
    ui.error('Error', result.error);
    app.save();
    return;
  }

  // ///////////////////////////////////////////////////////
  // create fit data for the temperature and median response, display the results, save them to CSV, and prepare them to be saved back to the device
  /*
  ui.info(
    'Heat Calibration Information',
    'In order to account for the effects of heat on the light intensity of the device, a correction factor is applied.  This measurement calculates that correction factor for each light, and saves the correction factor to the device.  The correction factors are based on measuring the voltage supplied to the LED, which varies inversely with the temperature because the circuit is current controlled (so intensity ~ voltage * current... we know that intensity varies with temperature, and we know that current is constant... so we can estimate the temperature change by measuring voltage.  This is intended to address short term temperature effects.  Impacts of intensity like LED degradation, scratches in the light guide, etc should be addressed by recalibrating the device using a calibration card.',
  );
  */

  const spadActual = [
    [8, 15.8, 32.1],
    [23.8, 37.6, 42.2],
    [21.8, 40.1, 51]
  ];
  const cards = [1, 2, 3];
  const ndvi = [];
  const cvi = [];
  for (let i = 2; i < 5; i++) {
    ndvi.push([]);
    cvi.push([]);
    ndvi[0].push(sprintf('%.2f', result[`card${i}`].data.ndvi));
    cvi[0].push(sprintf('%.2f', result[`card${i}`].data.cvi));
  }
  for (let i = 5; i < 8; i++) {
    ndvi[1].push(sprintf('%.2f', result[`card${i}`].data.ndvi));
    cvi[1].push(sprintf('%.2f', result[`card${i}`].data.cvi));
  }
  for (let i = 8; i < 11; i++) {
    ndvi[2].push(sprintf('%.2f', result[`card${i}`].data.ndvi));
    cvi[2].push(sprintf('%.2f', result[`card${i}`].data.cvi));
  }

  let textPrint = 'SPAD,&nbsp;&nbsp; NDVI,&nbsp;&nbsp; CVI<br>';
  for (let i = 1; i < 4; i++) {
    textPrint += `&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;card #${i}<br>`;
    for (let j = 0; j < 3; j++) {
      textPrint += `${spadActual[i - 1][j]},&nbsp;&nbsp;&nbsp; ${ndvi[i - 1][j]},&nbsp;&nbsp; ${cvi[i - 1][j]},&nbsp;<br>`;
    }
  }

  ui.info('Greenness values', textPrint);

  console.log(ndvi);
  console.log(cvi);
  console.log(spadActual);

  ui.multixy(
    ndvi,
    spadActual,
    'SPAD v. NDVI',
    cards.map(v => 'Fit, Card ' + v),
  );

  ui.multixy(
    cvi,
    spadActual,
    'CVI v. NDVI',
    cards.map(v => 'Fit, Card ' + v),
  );


  //  ui.plotxy(ndvi, spadActual, 'SPAD v NDVI');
  //  ui.plotxy(cvi, spadActual, 'SPAD v CVI');

  /*
    ui.plot({
        series: [data],
      },
      'Raw Spectral Results', {
        //    min: 0,
        //    max: 66000,
      },
    );

    ui.plot({
        series: [data],
      },
      'NDVI for cards', {
        //    min: 0,
        //    max: 66000,
      },
    );
  */

})();
// make sure this is at the very end!
app.save();