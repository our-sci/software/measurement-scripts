import axios from 'axios';

const key = 'f8308119b5844e5faa7225518180706';

export default (lat, lon, from, to) =>
  new Promise((resolve, reject) => {
    axios
      .get(`https://api.worldweatheronline.com/premium/v1/past-weather.ashx?key=${key}&q=${lat},${lon}&format=json&date=${from}&enddate=${to}&tp=1`)
      .then((response) => {
        console.log(response.data);
        resolve(response.data);
      })
      .catch((error) => {
        console.log(error);
        reject(error);
      });
  });
