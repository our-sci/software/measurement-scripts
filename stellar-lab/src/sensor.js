/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

/* eslint-disable prefer-destructuring */

import app from './lib/app';
import serial from './lib/serial';
import { sleep } from './lib/utils'; // use: await sleep(1000);

export default serial; // expose this to android for calling onDataAvailable

function processInput(data) {
  const json = {};

  const lines = data.split('\n');
  const args = lines[1].split(' ');
  console.log(args);

  json.time = args[9].split(':')[1];

  json.values = {};

  lines.slice(2).forEach((element) => {
    if (element.trim()) {
      const vals = element.trim().split(' ');
      json.values[vals[0]] = vals[2];
    }
  });
  return json;
}

(async () => {
  serial.webInputEmitter.on('event', () => {
    const json = processInput(app.getWebInput());
    app.result(json);
  });

  if (!app.isAndroid) {
    const fs = require('fs');
    const mock = fs.readFileSync('./mock/mock_sensor_output.json', 'utf-8').toString();
    const json = processInput(mock);

    console.log('saving json');
    console.log(json);
    app.result(json);
  }
})();
