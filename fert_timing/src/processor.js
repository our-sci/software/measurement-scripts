/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */
/* eslint-disable linebreak-style */
/* global processor */

import { sprintf } from 'sprintf-js';
import _ from 'lodash';

import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  const NPK_rec = {};
  console.log(result['fertilizers_use'].split(','));
  if (
    result['fertilizers_use'].split(',').includes('NPK') &&
    result['fertilizers_use'].split(',').includes('Urea') &&
    result['fert_timing'].split(',').includes('planting')
  ) {
    ui.info('Great job!');
    ui.info('It is best to add NPK fertilizer at planting and apply urea 4-6 weeks after planting');
    ui.info(
      'Applying NPK fertilizer at planting helps maize develop a healthy root system early, which can help maize withstand early dry spells and reduce the negative effects of witch weed. If, for some reason, you cannot apply NPK fertilizer at planting, please try to apply 2 within 2 weeks of planting, or one week after germination',
    );
  } else if (
    result['fertilizers_use'].split(',').includes('NPK') &&
    result['fert_timing'].split(',').includes('planting')
  ) {
    ui.info('Great job!');
    ui.info(
      'Applying NPK fertilizer at planting helps maize develop a healthy root system early, which can help maize withstand early dry spells and reduce the negative effects of witch weed. If, for some reason, you cannot apply NPK fertilizer at planting, please try to apply 2 within 2 weeks of planting, or one week after germination',
    );
  } else if (
    result['fertilizers_use'].split(',').includes('NPK') &&
    !result['fertilizers_use'].split(',').includes('Urea') &&
    !result['fert_timing'].split(',').includes('planting')
  ) {
    ui.info('Apply NPK fertilizer at the time of planting maize');
    ui.info(
      'Put the fertilizer and seed side by side. You must not worry about losing the applied NPK fertilizer due to poor germination - as long as you have used good seed and have planted after soils are wetted with good rains, you will get good germination. The benefits of applying NPK fertilizer at planting far outweigh any losses of fertilizer in spots with no germination. In any case, part of the fertilizer (P) will remain in the soil for future use or neighboring crops can use that fertilizer due to better rooting system with early fertilization',
    );
  } else if (
    result['fertilizers_use'].split(',').includes('NPK') &&
    result['fertilizers_use'].split(',').includes('Urea') &&
    !result['fert_timing'].split(',').includes('planting')
  ) {
    ui.info(
      'It is best to add NPK fertilizer at planting and then apply urea 4-6 weeks after planting',
    );
    ui.info(
      'Applying NPK fertilizer at planting helps maize develop a healthy root system early, which can help maize withstand early dry spells and reduce the negative effects of witch weed. If, for some reason, you cannot apply NPK fertilizer at planting, please try to apply 2 within 2 weeks of planting, or one week after germination',
    );
  } else if (
    result['fertilizers_use'].split(',').includes('Urea') &&
    !result['fertilizers_use'].split(',').includes('NPK')
  ) {
    ui.info('If you only have urea fertilizer available, apply it 2-4 weeks after planting');
    ui.info(
      'This earlier application helps plants to avoid too much UREA deficiency stress. If possible, you should apply manure at planting to provide your maize with P to help develop a good root system',
    );
  } else if (
    !result['fertilizers_use'].split(',').includes('NPK') &&
    !result['fertilizers_use'].split(',').includes('Urea')
  ) {
    ui.info('If possible, you should apply manure at planting');
  }

  app.save();
})();
