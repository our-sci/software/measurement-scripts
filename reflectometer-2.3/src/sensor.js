/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

/** Greg's notes
 * added //DEBUG to quiet console log statements here and in libraries app, process-wavelengths, and serial.
 */

import app from './lib/app';
import serial from './lib/serial';
import wavelengthProcessor from './lib/process-wavelengths';
import sendDevice from './lib/sendDevice';
import * as MathMore from './lib/math';
import sleep from './lib/utils'; // use: await sleep(1000);
/*
import {
  xhr
} from 'winjs';
*/

export default serial; // expose this to android for calling onDataAvailable

(async () => {
  await serial.init('/dev/ttyACM0', {
    //  await serial.init('/dev/rfcomm0', {
    baudRate: 115200,
    dataBits: 8,
    stopBits: 1,
    parity: 'none',
  });

  // ///////////////////////////////////////////////////////
  // OPTIONAL instead of reading from the serial port mock some data
  // serial.feed('./mock/mock_sensor_output.json');

  // ///////////////////////////////////////////////////////
  // Multiple measurements are saved from this single measurement script
  // Select a serial.write command below based on what you are updating to the Our Sci database.
  // Then copy paste the associated manifest (listed at the very bottom) and save it to the manifest.json file in src folder
  // Finally hit ctrl-b "compile and upload" and check the website app.our-sci.net to confirm.

  /*
  // Calibration measurements (device calibration to card, blank to EEPROM, blank to local environment variable)
  // the lights are in numerical order, rather than wavelength order...
  calibrateAllWhite,
  calibrateAllBlack,
  calibrateAllBlank,
  calibrateBlankLocal,
  calibrateAllWhiteMaster,
  calibrateAllBlackMaster,

  // standard measurement for user device on sample showing relative values (0 - 100)
  standard,
  standardBlank,
  standardRaw,
  standardRawHeatCal,
  standardApplyCalibration,

  //  other legacy protocols
  oldReflectometer2_flat,
  */

  let protocol = sendDevice().standard; // <-- copy paste the protocol you want to run in here

  // if you want to adjust on the fly based on local environmental variables, add if statements here!
  if (
    app.getEnv('rfclab') === '1' &&
    JSON.stringify(protocol) === JSON.stringify(sendDevice().standard)
  ) {
    // if (JSON.stringify(protocol) === JSON.stringify(sendDevice().standard)) { // DEBUG
    protocol = sendDevice().standardApplyCalibration;
  }
  // more if statements as needed

  // Then send the protocol to the device
  serial.write(JSON.stringify(protocol));
  console.log(protocol);

  // set of scripts to produce (send info to serial, manifest, a few flags that I can use to adjust visualization...) -->
  // see and get environment vars and questions from survey (using VS intellisense, typescript)
  // mock for environment vars.
  // eliminate mock for surveys (not having to \" all the time) access surveys via phone or access the web.

  // Mark the progress of pulling in the data
  let count = 0;
  try {
    app.progress(0.1);
    const result = await serial.readStreamingJson('data_raw[*]', () => {
      count += 1;
      app.progress(0.1 + (count / 617.0) * 100.0 * 0.9);
    });
    app.progress(100);

    // ///////////////////////////////////////////////////////
    // if this is just a single measurement, then define meas as the measurement.  If there are multiple, note that they also need to be pulled out here.
    for (let j = 0; j < result.sample.length; j++) {
      const meas = result.sample[j];
      meas.passed = {};
      // default values to zero, save over if they exist (otherwise we're dealing with typeof 'undefined' all the time which is annoying)
      meas.passed.is_chlorophyll = '';
      meas.passed.test = '';
      meas.passed.cardQr = '';
      meas.passed.blank = '';
      // Go get any information from previous questions and pass it along for the processor script to use.
      // May also save things using environment variable (env.get, env.set)
      if (typeof meas.calibration === 'undefined') {
        // default calibration to zero.
        meas.calibration = 0;
      } // if you can't find it, set it to zero
      if (typeof app.getAnswer('is_chlorophyll') !== 'undefined') {
        meas.passed.is_chlorophyll = app.getAnswer('is_chlorophyll');
        console.log('has chlorophyll' + meas.passed.is_chlorophyll);
      }
      if (typeof app.getAnswer('test') !== 'undefined') {
        meas.passed.test = app.getAnswer('test');
        console.log('this is lab test ' + meas.passed.test);
      }
      if (typeof app.getAnswer('cardQr') !== 'undefined') {
        meas.passed.cardQr = app.getAnswer('cardQr');
        console.log('this is a calibration, passed QR code to processor... it is ... ');
        console.log(meas.passed.cardQr);
      }
      if (typeof app.getEnv('blank') !== 'undefined') {
        // it needs to be defined and cal = 0
        if (app.getEnv('blank') !== '' && app.getEnv('blank') !== null) {
          // but also must be not empty
          if (meas.calibration === 0) {
            meas.passed.blank = JSON.parse(app.getEnv('blank'));
            console.log(
              'this measurement will use a blank saved in the local environment variables.  The saved blank is... ',
            );
            console.log(JSON.stringify(meas.passed.blank));
            console.log(
              'saved blank has been passed to processor.js and local environment blank has been reset {}.  Confirm reset: ',
            );
            app.setEnv(
              'blank',
              '',
              "Blank, and is applied to the next 'standard' measurement.  Reset after each measurement when it is applied.",
            );
            console.log(app.getEnv('blank'));
          } else {
            console.log(
              'A blank was found, but is not supposed to be applied in this measurement.  It was not used, and was removed as a local variable.  Confirm reset: ',
            );
            app.setEnv(
              'blank',
              '',
              "Blank, and is applied to the next 'standard' measurement.  Reset after each measurement when it is applied.",
            );
            console.log(app.getEnv('blank'));
          }
        }
      }
    }
    /* for testing above
    "toDevice": "chickenRunIs+A+Great-movie+",
    "is_chlorophyll": "1",
    "test": "1",
    */
    app.result(result);
  } catch (err) {
    console.error(`error reading json: ${err}`);
  }
})();

// list of all scripts saved to app.our-sci.net based on this script (only changing the protocol sent to device in sensor.js)

/*
"id": "standard-2-3",
"name": "Reflectometer standard 2-3",
"description": "Standard reflectometer measurement for field and lab use",
"version": "2.3",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "standardBlank-2-3",
"name": "Reflectometer blank 2-3",
"description": "Standard reflectomter measurement with blank offset, used on samples in cuvettes or similar situations in which blanks are useful.  Use the calibrateAllBlank measurement on the blank itself to save the blank values to the device",
"version": "2.3",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateBlankLocal-2-3",
"name": "Reflectometer blank saved as local variable",
"description": "Standard reflectomter blank offset which saves result to the local Environment variable. standard measurement uses if found, then resets.",
"version": "2.3",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "standardRaw-2-3",
"name": "Reflectometer raw 2-3",
"description": "outputs raw data only, use standard reflectometer measurement for normal use",
"version": "2.3",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "standardRawNoHeat-2-3",
"name": "Reflectometer raw no heat calibration 2-3",
"description": "outputs raw data only, use standard reflectometer measurement for normal use.  Heat calibration not applied.",
"version": "2.3",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllWhite-2-3",
"name": "Reflectometer calibration white 2-3",
"description": "Calibrate a device to user calibration card, using the white square.",
"version": "2.3",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllBlack-2-3",
"name": "Reflectometer calibration black 2-3",
"description": "Calibrate a device to user calibration card, using the black square.",
"version": "2.3",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllBlank-2-3",
"name": "Reflectometer calibration blank 2-3",
"description": "Create and save a blank value, to be applied as an offset when using the standard_blank measurement on samples.",
"version": "2.3",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllWhiteMaster-2-3",
"name": "Reflectometer calibration white master 2-3",
"description": "Calibrate the master device to master calibration card, using the white square.",
"version": "2.3",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllBlackMaster-2-3",
"name": "Reflectometer calibration black master 2-3",
"description": "Calibrate the master device to master calibration card, using the black square.",
"version": "2.3",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllBlackMasterMaster-2-3",
"name": "Reflectometer Master device - Maste card calibration 2-3",
"description": "ONLY used to calibrate the master card.  Force saves 0 / 100 values to the device.",
"version": "2.3",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "standard-firmware-1-18",
"name": "Reflectometer measurement firmware 1.18",
"description": "only for beta devices on old firmware 1.18",
"version": "2.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "heat-cal",
"name": "Heat Calibration",
"description": "Calibrate devices for different temperatures (3 hour test)",
"version": "1.0",
"action": "Run Calibration",
"requireDevice": "true"
*/

/*
"id": "standard-chlorophyll-2-3",
"name": "Chlorophyll standard 2-3",
"description": "Chlorophyll content using reflectometer for field and lab use",
"version": "2.3",
"action": "Run Measurement",
"requireDevice": "true"
  */