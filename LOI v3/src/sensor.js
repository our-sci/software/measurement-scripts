/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-unreachable */

/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

/** Greg's notes
 * added //DEBUG to quiet console log statements here and in libraries app, process-wavelengths, and serial.
 */

// import moment from 'moment';
// import _ from 'lodash';
// import mathjs from 'mathjs';

// import serial from './lib/serial';
import app from './lib/app';
// import sendDevice from './lib/sendDevice';
// import * as MathMore from './lib/math';
// import * as utils from './lib/utils'; // use: await sleep(1000);
// import soilgrid from './lib/soilgrid';
// import weather from './lib/weather';
// import oursci from './lib/oursci';

// export default serial; // expose this to android for calling onDataAvailable

const result = {};

function errorExit(error) {
  app.result({
    error,
  });
}

result.error = {}; // put error information here.
const requiredMeasurements = ['loi_crucible_ohaus', 'loi_pre_ohaus', 'loi_post_ohaus'];
const requiredAnswersNumber = ['loi_crucible_manual', 'loi_pre_manual', 'loi_post_manual'];

(async () => {
  requiredMeasurements.forEach((a) => {
    let v;
    if (typeof app.getAnswer(a) !== 'undefined') {
      v = JSON.parse(app.getAnswer(a));
    } else {
      v = 'undefined';
      result.error[a] = 'undefined';
    }
    console.log(v);
    result[a] = v;
  });

  requiredAnswersNumber.forEach((a) => {
    let v;
    if (typeof app.getAnswer(a) !== 'undefined') {
      v = app.getAnswer(a);
      if (app.getAnswer(a) == '') {
        result.error[a] = 'missing';
      } else if (isNaN(app.getAnswer(a))) {
        result.error[a] = 'not a number';
      }
    } else {
      v = 'undefined';
      result.error[a] = 'undefined';
    }
    console.log(v);
    result[a] = v;
  });

  console.log(result);
  app.result(result);
})();
