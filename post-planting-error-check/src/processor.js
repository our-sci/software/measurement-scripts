/* eslint-disable linebreak-style */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

import { app } from '@oursci/scripts';
import * as ui from '@oursci/measurements-ui';
import '@oursci/measurements-ui/dist/styles.min.css';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  console.log(result);
  if (result.errors && result.errors.length > 0) {
    result.errors.forEach((a) => {
      ui.error('Error', a);
    });
  } else {
    ui.info('Success', 'Survey looks all good.');
  }

  app.save();
})();
