/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-unreachable */

/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

/** Greg's notes
 * added //DEBUG to quiet console log statements here and in libraries app, process-wavelengths, and serial.
 */

// import moment from 'moment';
// import _ from 'lodash';
// import mathjs from 'mathjs';

// import serial from './lib/serial';
import app from "./lib/app";
// import sendDevice from './lib/sendDevice';
// import * as MathMore from './lib/math';
// import * as utils from './lib/utils'; // use: await sleep(1000);
// import soilgrid from './lib/soilgrid';
// import weather from './lib/weather';
import oursci from "./lib/oursci";

// export default serial; // expose this to android for calling onDataAvailable

const result = {};

function errorExit(error) {
  app.result({
    error,
  });
}

/*

in survey -- 2020 RFC LOI Final (where this survey is run)
- pull these in here in sensor.js...
- tray_id - ID of the tray (all ids are in this tray)
- lab_id_1/2/3... - id of this sample
- loi_post_ohaus_1 - crucible + soil after
- loi_post_manual_1 - crucible + soil after

in survey -- 2020 RFC LOI pre-weights
- pull these in in processor.js from the surveystack db...
- tray_id - ID of the tray (all ids are in this tray)
- lab_id_1/2/3... - id of this sample
- loi_crucible_ohaus_1 - crucible before
- loi_crucible_manual_1 - crucible before
- loi_pre_ohaus_1 - crucible + soil before
- loi_pre_manual_1 - crucible + soil before

Output...

  Any warnings... plus...
    ui.info('Pre Weight', MathMore.MathROUND(moist_wt - crucible_wt, 3));
    app.csvExport('pre_weight', MathMore.MathROUND(moist_wt - crucible_wt, 3));
    ui.info('Post Weight', MathMore.MathROUND(burned_wt - crucible_wt, 3));
    app.csvExport('post_weight', MathMore.MathROUND(burned_wt - crucible_wt, 3));
    ui.info('Organic Matter %', MathMore.MathROUND(Organic_Matter, 2));
    app.csvExport('Organic_Matter', MathMore.MathROUND(Organic_Matter, 2));
    ui.info('Total Organic C %', MathMore.MathROUND(Total_C, 2));
    app.csvExport('Total Organic C %', MathMore.MathROUND(Total_C, 2));

*/

result.error = {}; // put error information here.
result.error.id = [];
// we're going to pull in each answer, but also put them into an array to pass to processor via results.json
result.lab_id = [];
result.loi_post_ohaus = [];
result.loi_post_manual = [];
result.loi_crucible_ohaus = [];
result.loi_pre_ohaus = [];
result.loi_crucible_manual = [];
result.loi_pre_manual = [];

const items = 37; // total samples per tray + 1
const split = 18; // half-way point through the tray

const requiredMeasurements = [];
const requiredAnswersNumber = [];
const requiredAnswersText = [];
for (let i = 0; i < items; i++) {
  let rack2 = ''; // first 18, there's no prefix
  if (i > split) rack2 = 'rack_2/'; // add prefix for last 18
  requiredMeasurements.push(`${rack2}loi_post_ohaus_${i}`);
  requiredAnswersNumber.push(`${rack2}loi_post_manual_${i}`);
  if (i === 8 && typeof app.getAnswer('lab_id_') !== 'undefined') {
    requiredAnswersText.push('lab_id_'); // mistake in survey, need to include to fix old surveys.  Since is fixed.
  } else {
    requiredAnswersText.push(`${rack2}lab_id_${i}`);
  }
}
requiredAnswersText.push('tray_id');

(async () => {
  requiredMeasurements.forEach((a) => {
    let v;
    if (typeof app.getAnswer(a) !== 'undefined' && app.getAnswer(a) !== '') {
      v = JSON.parse(app.getAnswer(a));
    } else {
      v = 'undefined';
      //      result.error[a] = 'undefined';
    }
    if (a.includes('loi_post_ohaus')) {
      result.loi_post_ohaus.push(v);
    } else {
      result[a] = v;
    }
  });

  requiredAnswersNumber.forEach((a) => {
    let v;
    if (typeof app.getAnswer(a) !== 'undefined') {
      v = app.getAnswer(a);
      if (app.getAnswer(a) === '') {
        //        result.error[a] = 'missing';
      } else if (isNaN(app.getAnswer(a))) {
        //        result.error[a] = 'not a number';
      }
    } else {
      v = 'undefined';
      //      result.error[a] = 'undefined';
    }
    if (a.includes('loi_post_manual')) {
      result.loi_post_manual.push(v);
    } else {
      result[a] = v;
    }
  });

  requiredAnswersText.forEach((a) => {
    let v;
    if (typeof app.getAnswer(a) !== 'undefined') {
      v = app.getAnswer(a);
      if (app.getAnswer(a) === '') {
        //        result.error[a] = 'missing';
      }
    } else {
      v = 'undefined';
      //      result.error[a] = 'undefined';
    }
    if (a.includes('lab_id')) {
      result.lab_id.push(v);
    } else {
      result[a] = v;
    }
  });

  // console.log(requiredAnswersText);
  // console.log(requiredAnswersNumber);
  // console.log(requiredMeasurements);

  // Now we need to go get the data from the survey '2020 RFC LOI pre-weights' and put them into arrays also...

  let data;
  try {
    data = await oursci('2020-rfc-loi-pre-weights');
  } catch (error) {
    result.error.oursciFetch = `Unable to fetch oursci survey results: ${error}`;
    console.log(result.error.oursciFetch);
  }
  //  console.log(data);

  // filter the data
  const trayData = [];
  data.forEach((row) => {
    if (row.tray_id === result.tray_id) trayData.push(row);
  });
  if (trayData.length > 1) {
    console.log(`multiple tray_ids found for ${result.tray_id}`);
    result.error.multiple_tray_ids = true;
  } else if (trayData.length === 0) {
    console.log(`no tray_id ${result.tray_id} found`);
    result.error.no_tray_id = true;
  }
  //  console.log(trayData);

  // now... we make an array for the pre and crucible measures based on the trayData...
  // pass undefined and identify those which do not have ID's
  // note we search the entire tray for the ID, not just the same position that the current ID is in.
  // console.log(trayData[0]);

  result.lab_id.forEach((id) => {
    // loop thru IDs in current surveys
    if (id === 'undefined') {
      result.loi_crucible_ohaus.push('undefined');
      result.loi_pre_ohaus.push('undefined');
      result.loi_crucible_manual.push('undefined');
      result.loi_pre_manual.push('undefined');
      result.error.id.push('undefined');
    } else {
      // if not undefined...
      let found = 0;
      const thisTray = trayData[0];
      //      console.log(thisTray);
      Object.keys(thisTray).forEach((columnName) => {
        if (columnName.includes('lab_id_') && thisTray[columnName] === id) {
          // if the column name is a lab_id_XXX column and it's the id then...
          let position = columnName.split('_');
          if (position[position.length - 1] === '') {
            //            console.log('found position 8');
            position[position.length - 1] = '8'; // fixing error in old form where lab_id_8 was mismarked as lab_id_... this is since corrected
          }
          position = position[position.length - 1];
          let rack2 = '';
          if (Number(position) > split) rack2 = 'rack_2.';
          // console.log(`looking: ${id} in ${columnName}, found: ${thisTray[columnName]}`);
          //          console.log(`position: ${position}, id: ${thisTray[columnName]}`);
          result.loi_pre_ohaus.push(thisTray[`${rack2}loi_pre_ohaus_${position}`]);
          result.loi_crucible_ohaus.push(thisTray[`${rack2}loi_crucible_ohaus_${position}`]);
          result.loi_pre_manual.push(thisTray[`${rack2}loi_pre_manual_${position}`]);
          result.loi_crucible_manual.push(thisTray[`${rack2}loi_crucible_manual_${position}`]);
          result.error.id.push('');
          found++; // loi_post_ohaus_1
        }
      });
      if (found === 0) {
        result.loi_crucible_ohaus.push('id not found');
        result.loi_pre_ohaus.push('id not found');
        result.loi_crucible_manual.push('id not found');
        result.loi_pre_manual.push('id not found');
        result.error.id.push('id not found');
      } else if (found > 1) {
        result.loi_crucible_ohaus.push('multiple matched ids');
        result.loi_pre_ohaus.push('multiple matched ids');
        result.loi_crucible_manual.push('multiple matched ids');
        result.loi_pre_manual.push('multiple matched ids');
        result.error.id.push('multiple matched ids');
      }
    }
  });

  //  console.log(result);
  app.result(result);
})();