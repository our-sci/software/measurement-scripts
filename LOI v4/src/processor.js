/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
// /* eslint-disable brace-style */
/* eslint-disable eol-last */
// /* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
// /* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

/* global processor */

// import { sprintf } from 'sprintf-js';
// import _ from 'lodash';

import app from "./lib/app";
import * as ui from "./lib/ui";
import * as MathMore from "./lib/math";

(() => {
  const result = (() => {
    if (typeof processor === "undefined") {
      return require("../data/result.json");
    }
    return JSON.parse(processor.getResult());
  })();

  // Find + display an errors from sensor
  Object.keys(result.error).forEach((error) => {
    if (error === "id") {
      // these are the sample-by-sample errosr
      result.error[error].forEach((sample, index) => {
        if (index !== 0 && sample !== "") {
          // if an error is marked, then display it
          ui.error(
            `id: ${result.lab_id[index]}, pos: ${index}`,
            `error: ${sample}`
          );
        }
      });
    } else {
      // all other errors
      ui.error("Something went wrong", error);
    }
  });

  // if it came from the balance, use it
  // if not, use numerical output
  // if neither, report an error
  // info and csv output

  // use these to print to user at the end visibly easy to review list
  const labId = [];
  const crucible = [];
  const moist = [];
  const final = [];
  const organic = [];
  const total = [];

  result.lab_id.forEach((id, index) => {
    if (index !== 0 && id !== "undefined" && result.error.id[index] === "") {
      // if this is a good measurement... calculate + display...
      let crucible_wt = 0;
      if (
        result.loi_crucible_ohaus[index] !== null &&
        result.loi_crucible_ohaus[index] !== "" &&
        typeof result.loi_crucible_ohaus[index].data !== "undefined" &&
        typeof result.loi_crucible_ohaus[index].data.weight_grams !==
        "undefined"
      ) {
        crucible_wt = result.loi_crucible_ohaus[index].data.weight_grams;
        // ui.info(
        //   `${id} Crucible Weight (ohaus)`,
        //   MathMore.MathROUND(crucible_wt, 3)
        // );
      } else if (Number(result.loi_crucible_manual[index])) {
        crucible_wt = result.loi_crucible_manual[index];
        // ui.info(
        //   `${id} Crucible Weight (manual)`,
        //   MathMore.MathROUND(crucible_wt, 3)
        // );
      } else {
        ui.error(
          `id "${id}" Crucible Weight Missing`,
          "The crucible weight is missing both from the scale and from manual entry, so LOI Carbon cannot be calculated.  Go back and fill in the weight."
        );
      }

      let moist_wt = 0;
      if (
        result.loi_pre_ohaus[index] !== null &&
        result.loi_pre_ohaus[index] !== "" &&
        typeof result.loi_pre_ohaus[index].data !== "undefined" &&
        typeof result.loi_pre_ohaus[index].data.weight_grams !== "undefined"
      ) {
        moist_wt = result.loi_pre_ohaus[index].data.weight_grams;
        // ui.info(`${id} Moist Weight (ohaus)`, MathMore.MathROUND(moist_wt, 3));
      } else if (Number(result.loi_pre_manual[index])) {
        moist_wt = result.loi_pre_manual[index];
        // ui.info(`${id} Moist Weight (manual)`, MathMore.MathROUND(moist_wt, 3));
      } else {
        ui.error(
          `id "${id}" Moist Weight Missing`,
          "The crucible + soil weight is missing both from the scale and from manual entry, so LOI Carbon cannot be calculated.  Go back and fill in the weight."
        );
      }

      let burned_wt = 0;
      if (
        result.loi_post_ohaus[index] !== null &&
        result.loi_post_ohaus[index] !== "" &&
        typeof result.loi_post_ohaus[index].data !== "undefined" &&
        typeof result.loi_post_ohaus[index].data.weight_grams !== "undefined"
      ) {
        burned_wt = result.loi_post_ohaus[index].data.weight_grams;
        // ui.info(`${id} Final Weight (ohaus)`, MathMore.MathROUND(burned_wt, 3));
      } else if (Number(result.loi_post_manual[index])) {
        burned_wt = result.loi_post_manual[index];
        // ui.info(
        //   `${id} Final Weight (manual)`,
        //   MathMore.MathROUND(burned_wt, 3)
        // );
      } else {
        ui.error(
          `id "${id}" Final Weight Missing`,
          "The final weight is missing, so LOI Carbon cannot be calculated.  Go back and fill in the weight."
        );
      }

      if (moist_wt !== 0 && burned_wt !== 0 && crucible_wt !== 0) {
        const Organic_Matter =
          100 * ((moist_wt - burned_wt) / (moist_wt - crucible_wt));
        // Traditional conversion factor for %OM to %TOC is OM = TOC x 1.724, may vary based on soil type and type of OM;
        const Total_C = Organic_Matter / 1.724;

        if (Total_C < 0) {
          ui.error(
            `id "${id}" Total Carbon < 0!`,
            "something is wrong, check the weights or sample IDs"
          );
        }
        if (Total_C < 0.4) {
          ui.warning(
            `id "${id}" Total Carbon < 0.5!`,
            "for very poor soils this could be correct, but is very low.  Confirm before continuing"
          );
        }
        if (Total_C > 10) {
          ui.warning(
            `id "${id}" Total Carbon > 10!`,
            "this is very unlikely for minerals soils.  Check the weights or sample IDs to ensure there wasnt an error"
          );
        }

        app.csvExport(`lab_id_${index}`, id);
        app.csvExport(`pre_weight_${index}`, MathMore.MathROUND(moist_wt - crucible_wt, 3));
        app.csvExport(`post_weight_${index}`, MathMore.MathROUND(burned_wt - crucible_wt, 3));
        //        app.csvExport(`burned_wt_${index}`, MathMore.MathROUND(burned_wt, 3));
        app.csvExport(`organic_matter_${index}`, MathMore.MathROUND(Organic_Matter, 2));
        app.csvExport(`total_organic_c_${index}`, MathMore.MathROUND(Total_C, 2));

        labId.push(id);
        crucible.push(crucible_wt);
        moist.push(moist_wt);
        final.push(burned_wt);
        organic.push(MathMore.MathROUND(Organic_Matter, 3));
        total.push(MathMore.MathROUND(Total_C, 3));
      } else {
        labId.push(id);
        crucible.push("");
        moist.push("");
        final.push("");
        organic.push("");
        total.push("");
      }
    } else if (index !== 0) {
      ui.error(
        `id: "${id}", pos: ${index}`,
        `error: ${result.error.id[index]}. There was a problem finding the ID from the previous survey.  Manually check and correct.`
      );
      labId.push(id);
      crucible.push(result.error.id[index]);
      moist.push(result.error.id[index]);
      final.push(result.error.id[index]);
      organic.push(result.error.id[index]);
      total.push(result.error.id[index]);
    } else {
      // make sure to push something otherwise we get off counting
      labId.push(id);
      crucible.push("");
      moist.push("");
      final.push("");
      organic.push("");
      total.push("");
    }
  });

  function spaces(text, len) {
    const extraSpaces = len - text.toString().trim().length;
    if (extraSpaces > 0) {
      for (let i = 0; i < extraSpaces; i++) {
        text += '&nbsp;';
      }
    }
    //    console.log(`|${text}|`);
    return text;
  }
  const len = 13; // length between rows
  let allResults = `<b>${spaces("Lab ID", len)} ${spaces("total % C", len)} ${spaces("crucible wt", len)} ${spaces("pre wt", len)} ${spaces("post wt", len)}</b><br>`;
  // let crucibleResults = "";
  // let moistResults = "";
  // let finalResults = "";
  // let organicResults = "";
  // let totalResults = "";

  for (let i = 1; i < crucible.length; i++) {
    allResults += `${spaces(labId[i], len)} ${spaces(total[i], len)} ${spaces(crucible[i], len)} ${spaces(moist[i], len)} ${spaces(final[i], len)}<br>`;
    // totalResults += `${total[i]}<br>`;
    // organicResults += `${organic[i]}<br>`;
    // crucibleResults += `${crucible[i]}<br>`;
    // moistResults += `${moist[i]}<br>`;
    // finalResults += `${final[i]}<br>`;
  }
  ui.info("All Results", allResults);
  // ui.info("total_c", totalResults);
  // ui.info("organic_matter", organicResults);
  // ui.info("crucible weights", crucibleResults);
  // ui.info("moist weights", moistResults);
  // ui.info("final weights", finalResults);
  app.save();
})();